<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWedstrijdenTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedstrijden', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('titel', 30);
            $table->date('datum');
            $table->time('tijd');
            $table->string('formatie', 10);
            $table->integer('teams_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wedstrijden', function (Blueprint $table) {
            //
        });
    }

}

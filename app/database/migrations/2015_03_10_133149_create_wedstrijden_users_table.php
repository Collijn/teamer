<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWedstrijdenUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedstrijden_users', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('wedstrijden_id');
            $table->integer('user_id');
            $table->integer('status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wedstrijden_user', function (Blueprint $table) {
            //
        });
    }

}

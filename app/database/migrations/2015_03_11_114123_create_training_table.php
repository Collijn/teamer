<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingen', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->date('datum');
            $table->time('tijd');
            $table->integer('teams_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainingen', function (Blueprint $table) {
            //
        });
    }

}

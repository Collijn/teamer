<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voornaam', 32);
            $table->string('achternaam', 32);
            $table->string('email', 320);
            $table->string('password', 64);
            $table->integer('groups_id');
            $table->string('remember_token', 100)->nullable();
            $table->integer('teams_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users');
    }

}

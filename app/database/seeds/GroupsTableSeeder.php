<?php

class GroupsTableSeeder extends Seeder
{

    public function run()
    {
        $groups = New Groups;
        $groups->naam = 'Administrator';
        $groups->save();

        $groups = New Groups;
        $groups->naam = 'Trainer';
        $groups->save();

        $groups = New Groups;
        $groups->naam = 'Speler';
        $groups->save();
    }
}
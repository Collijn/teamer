<?php

class StatusTableSeeder extends Seeder
{
    public function run()
    {
        $status = new Status;
        $status->status = 'Onbekend';
        $status->save();

        $status = new Status;
        $status->status = 'Aanwezig';
        $status->save();

        $status = new Status;
        $status->status = 'Afwezig';
        $status->save();
    }
}
<?php

class TrainingenUser extends Eloquent
{

    protected $table = "trainingen_users";

    public $timestamps = false;

    public function training()
    {
        return $this->hasMany('Trainingen', 'id', 'trainingen_id');
    }

    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function status()
    {
        return $this->hasOne('Status', 'id', 'status_id');
    }

}
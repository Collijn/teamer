<?php

class WedstrijdenUser extends Eloquent
{

    protected $table = "wedstrijden_users";

    public $timestamps = false;

    public function wedstrijd()
    {
        return $this->hasMany('Wedstrijden', 'id', 'wedstrijden_id');
    }

    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function status()
    {
        return $this->hasOne('Status', 'id', 'status_id');
    }

}
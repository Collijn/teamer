<?php

class Selectie extends Eloquent
{

    public $timestamps = false;

    public function User()
    {
        return $this->hasMany('User', 'id', 'user_id');
    }

    public function Wedstrijd()
    {
        return $this->hasOne('Wedstrijden', 'id', 'wedstrijd_id');
    }

    public function Teams()
    {
        return $this->hasOne('Teams', 'id', 'team_id');
    }

}
@extends('base.master')

@section('content')
    <div class="team-select col-md-4 col-md-offset-4">
        <h3>Vul u teamcode in</h3>

        <form method="post" action="login" class="team-form">
            <div class="form-group">
                <input type="text" name="teamcode" placeholder="Teamcode" class="input-lg form-control"><br>
            </div>
            <button type="submit" class="btn btn-success btn-login">Volg u team!</button>
        </form>
    </div>
@stop
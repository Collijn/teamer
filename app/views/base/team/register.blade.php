@extends('base.master-footer')

@section('content')
    <h3 class="head-text">Team Aanmaken</h3>

    <form method="post" action="registreren" class="form-horizontal form-register">

        <div class="col-md-6">
            <h5 class="team-steps">Stap 1: Maak een account aan</h5>

            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="voornaam" class="form-control" placeholder="Voornaam">
                </div>

                <div class="col-sm-6">
                    <input type="text" name="achternaam" class="form-control" placeholder="Achternaam">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="email" name="email" class="form-control" placeholder="E-mailadres">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="password" name="password" class="form-control" placeholder="Wachtwoord">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Wachtwoord">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h5 class="team-steps">Stap 2: Vul de teamgegevens in</h5>

            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" name="teamnaam" class="form-control" placeholder="Team Naam">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" name="teamadress" class="form-control" placeholder="Team Adress">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button style="float: right" type="submit" class="btn btn-registreer btn-default">Maak u team aan!
                    </button>
                </div>
            </div>
        </div>

    </form>
@stop
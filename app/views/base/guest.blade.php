@extends('base.master-footer')

@section('content')

    <div class="col-md-8">
        <img style="margin-top: 70px" width="700px" src="../../img/front-img.png">
    </div>

    <div class="col-md-4">
        <h3 class="head-text">Registeren</h3>

        <form method="post" action="register" class="form-horizontal form-register">
            <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="voornaam" class="form-control" value="{{Input::old('voornaam')}}"
                           placeholder="Voornaam">
                </div>

                <div class="col-sm-6">
                    <input type="text" name="achternaam" class="form-control" value="{{Input::old('achternaam')}}"
                           placeholder="Achternaam">
                </div>
            </div>
            <div class="form-group @if ($errors->has('emailRegister')) has-error @endif">
                <div class="col-sm-12">
                    <input type="email" name="email" class="form-control" value="{{Input::old('email')}}"
                           placeholder="E-mailadres">
                </div>
            </div>
            <div class="form-group @if ($errors->has('passwordRegister')) has-error @endif">
                <div class="col-sm-12">
                    <input type="password" name="password" class="form-control" placeholder="Wachtwoord">
                </div>
            </div>
            <div class="form-group @if ($errors->has('passwordRegister_confirmation')) has-error @endif">
                <div class="col-sm-12">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Wachtwoord">
                </div>
            </div>
            <div class="form-group @if ($errors->has('teamcode')) has-error @endif">
                <div class="col-sm-8">
                    <input type="text" name="teamcode" class="teamcode form-control" value="{{Input::old('teamcode')}}"
                           placeholder="Teamcode">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="team" class="teamnaam form-control" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-registreer btn-default">Registreer</button>
                </div>
                <div class="col-sm-8">
                    <a class="team-link" href="team/registreren">Of maak je eigen team aan</a>
                </div>
            </div>
        </form>
    </div>

    <script>
        $('.teamcode').on('input', function () {

            $.ajax({
                url: '<?php echo Request::root(); ?>/team/teamcode',
                type: 'post',
                data: {teamcode: $('.teamcode').val()},
                success: function (response) {

                    $('.teamnaam').val(response);

                }
            });
        })

    </script>
@stop
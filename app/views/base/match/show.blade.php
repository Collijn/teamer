@extends('base.master')

@section('content')
    @if(Auth::check() == true)
        <?php $user = Auth::user() ?>
    @endif
    <div class="row">
        <div style="text-align: center; display: none;" class="alert alert-success" role="alert">

        </div>

        <div class="col-md-4">
            <h1 class="head-text">{{ $wedstrijd->titel }}</h1>

            @foreach($wed_users as $wed_user)
                @if($wed_user->user_id == $user->id)
                    @if($wed_user->status_id == 3)
                        <button style="margin-bottom: 20px;" type="button" value='3'
                                class="btn btn-danger btn-afwezig btn-sm" disabled>Afwezig
                        </button>
                    @else
                        <button style="margin-bottom: 20px;" type="button" value='3'
                                class="btn btn-danger btn-afwezig btn-sm">Afwezig
                        </button>
                    @endif

                    @if($wed_user->status_id == 2)
                        <button style=" margin-bottom: 20px;" type="button" value='2'
                                class="btn btn-aanwezig btn-success btn-sm" disabled>Aanwezig
                        </button>
                    @else
                        <button style=" margin-bottom: 20px;" type="button" value='2'
                                class="btn btn-aanwezig btn-success btn-sm">Aanwezig
                        </button>
                    @endif
                @endif
            @endforeach

            <div id="players">
                <table class="table players-table">
                    <tbody>
                    <tr>
                        <td>
                            <h4>Aanwezig
                                ( {{ WedstrijdenUser::where('status_id', '=', 2)->where('wedstrijden_id','=',$wed_id)->count(); }}
                                )</h4>
                        </td>
                        <td>
                        </td>
                    </tr>
                    @foreach($wed_users as $wed_user)
                        @if($wed_user->status_id == 2)
                            <tr class="user-id-{{ $wed_user->user->id }}">
                                <td id="{{ $wed_user->user->id }}" class="draggable">
                                    {{ $wed_user->user->voornaam }} {{ $wed_user->user->achternaam }}
                                </td>
                                <td style="text-align: right;" class="td-status-{{ $wed_user->user_id }}">
                                    {{ $wed_user->status->status }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>

                    <tbody>
                    <tr>
                        <td>
                            <h4>Afwezig
                                ( {{ WedstrijdenUser::where('status_id', '=', 3)->where('wedstrijden_id','=',$wed_id)->count(); }}
                                )</h4>
                        </td>
                        <td>
                        </td>
                    </tr>
                    @foreach($wed_users as $wed_user)
                        @if($wed_user->status_id == 3)

                            <tr class="user-id-{{ $wed_user->user->id }}">
                                <td id="{{ $wed_user->user->id }}" class="draggable">
                                    {{ $wed_user->user->voornaam }} {{ $wed_user->user->achternaam }}
                                </td>
                                <td style="text-align: right;" class="td-status-{{ $wed_user->user_id }}">
                                    {{ $wed_user->status->status }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>

                    <tbody>
                    <tr>
                        <td>
                            <h4>Onbekend
                                ( {{ WedstrijdenUser::where('status_id', '=', 1)->where('wedstrijden_id','=',$wed_id)->count(); }}
                                )</h4>
                        </td>
                        <td>
                        </td>
                    </tr>
                    @foreach($wed_users as $wed_user)
                        @if($wed_user->status_id == 1)
                            <tr class="user-id-{{ $wed_user->user->id }}">
                                <td id="{{ $wed_user->user->id }}" class="draggable">
                                    {{ $wed_user->user->voornaam }} {{ $wed_user->user->achternaam }}
                                </td>
                                <td style="text-align: right;" class="td-status-{{ $wed_user->user_id }}">
                                    {{ $wed_user->status->status }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if($user->groups_id == 1 OR $user->groups_id == 2)
                {{ Form::open(array('url' => 'wedstrijd/'.$wed_id, 'method' => 'delete' )) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            @endif
        </div>
        <div class="col-md-5">

            <div id="matchfield"></div>

            <button class="formatie-btn btn btn-success" type="button"
                    onclick="$('#matchfield').matchcenter('setPlayingSystem', '4-4-1-1');" value="4-4-1-1"/>
            4-4-1-1</button>
            <button class="formatie-btn btn btn-success" type="button"
                    onclick="$('#matchfield').matchcenter('setPlayingSystem', '4-4-2');" value="4-4-2"/>
            4-4-2</button>
            <button class="formatie-btn btn btn-success" type="button"
                    onclick="$('#matchfield').matchcenter('setPlayingSystem', '4-3-3');" value="4-3-3"/>
            4-3-3</button>
            <button class="formatie-btn btn btn-success" type="button"
                    onclick="$('#matchfield').matchcenter('setPlayingSystem', '4-1-2-1-2');" value="4-1-2-1-2"/>
            4-1-2-1-2</button>

        </div>

        <div class="col-md-3">

            <h4 class="head-text">Wisselspelers</h4>
            <table class="table wissels">
                <tbody>

                @if($selectie->count())
                    @foreach($selectie as $sel)
                        @foreach($sel->user as $s)
                            @if(strpos($sel->positie, 'wissel-') !== FALSE)
                                @if($s)
                                    <tr>
                                        <td class="wissel-td draggable {{ $sel->positie }}">{{ $s->voornaam }} {{ $s->achternaam }}</td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    @endforeach
                @else
                    <tr class="placeholder">
                        <td>Geen wissels</td>
                    </tr>

                @endif
                </tbody>
            </table>

            @if($user->groups_id == 1 OR $user->groups_id == 2)
                <button style="width: 100%" type="button" class="btn btn-opslaan btn-success">Opslaan</button>
            @endif
        </div>

        <script>
            var matchcenter = $('#matchfield').matchcenter({system: "<?php if($wedstrijd->formatie) { echo $wedstrijd->formatie; } else { echo "4-4-2"; } ?>"});

            @if($selectie->count())
            @foreach($selectie as $sel)
            @if(strpos($sel->positie, 'player-') !== FALSE)
            $('#matchfield').matchcenter("addPlayer", "{{ $sel->positie }}".replace('player-', ''), "{{ $sel->positie }}".replace('player-', ''), "@if($sel->user->count()) {{ $sel->user[0]->voornaam }} {{ $sel->user[0]->achternaam }} @endif");
                    @endif
                @endforeach
            @else
                for (i = 1; i < 12; i++) {
                $('#matchfield').matchcenter("addPlayer", i, i, "");
            }
            @endif

                @if($user->groups_id == 1 OR $user->groups_id == 2)
            $(".btn-opslaan").click(function () {
                player = {};

                for (i = 1; i < 12; i++) {
                    player["player-" + i] = $("#player-" + i).find('.player-name').text().trim();
                }

                i = 1;

                $('.wissels tr').each(function () {
                    if ($(this).text().trim() !== "Geen wissels") {
                        player["wissel-" + i] = $(this).text().trim();

                        i++;
                    }

                });

                $.ajax({
                    url: '<?php echo Request::root(); ?>/wedstrijd/opstelling',
                    type: 'post',
                    data: {players: JSON.stringify(player), wed_id: '<?php echo $wed_id; ?>'},
                    success: function (response) {
                        console.log(response);
                        $('.alert-success').text('Opstelling opgeslagen!').fadeIn("slow").delay(5000).fadeOut('slow');
                    }
                });
            });

            $(function () {
                $("#players .draggable").draggable({
                    appendTo: "body",
                    helper: "clone",
                    revert: 'invalid'
                });

                $(".player-name").droppable({
                    activeClass: "ui-state-default",
                    hoverClass: "ui-state-hover",
                    accept: ":not(.ui-sortable-helper)",
                    drop: function (event, ui) {

                        if (ui.draggable.context.tagName == "TD") {
                            if (ui.draggable.context.offsetParent.className == 'table wissels ui-droppable ui-state-default') {
                                $(ui.draggable).parent().remove();
                            } else {
                                $(ui.draggable).draggable('disable');
                            }


                            $(this).text(ui.draggable.text());
                        }

                        if (ui.draggable.context.tagName == "DIV") {
                            if ($(this).text().length > 0) {
                                tekst = $(this).text();

                                $(this).text(ui.draggable.text());
                                $(ui.draggable).text(tekst);
                            }

                            if ($(this).text().length == 0) {
                                $(this).text(ui.draggable.text());
                                $(ui.draggable).text('');
                            }
                        }

                    }
                });

                $(".wissels tbody").droppable({
                    activeClass: "ui-state-default",
                    hoverClass: "ui-state-hover",
                    accept: ":not(.ui-sortable-helper)",
                    drop: function (event, ui) {

                        $(".placeholder").remove();

                        var row = '<tr>' +
                                '<td class="wissels-td draggable">' + ui.draggable.text() + '</td>' +
                                '</tr>';

                        $(this).append(row);

                        $(".draggable").draggable({
                            appendTo: "body",
                            helper: "clone",
                            revert: 'invalid'
                        });

                        if (ui.draggable.context.tagName == "TD") {
                            $(ui.draggable).draggable('disable');
                        }

                        if (ui.draggable.context.tagName == "DIV") {
                            $(ui.draggable).text('');
                        }


                    }
                });

                $(".draggable").draggable({
                    appendTo: "body",
                    helper: "clone",
                    revert: 'invalid'
                });

                $(".player-name").draggable({
                    appendTo: "body",
                    helper: "clone",
                    revert: 'invalid'

                });
            });
            @endif

                $('.formatie-btn').click(function () {
                        $.ajax({
                            url: '<?php echo Request::root(); ?>/wedstrijd/opstellingformatie',
                            type: 'post',
                            data: {formatie: $(this).val(), wed_id: '<?php echo $wed_id ?>'},
                            success: function (response) {
                                $('.alert-success').text('Formatie opgeslagen!').fadeIn("slow").delay(5000).fadeOut('slow');
                            }
                        });
                    });


            $('.btn-afwezig').click(function () {
                $.ajax({
                    url: '<?php echo Request::root(); ?>/wedstrijd/status',
                    type: 'get',
                    data: {status: $('.btn-afwezig').val(), id: '<?php echo $wed_id; ?>'},
                    success: function (response) {

                        $('.btn-afwezig').prop('disabled', true);
                        $('.td-status-<?php echo $user->id; ?>').text('Afwezig');

                        $('.btn-aanwezig').prop('disabled', false);
                        location.reload();
                    }
                });
            });

            $('.btn-aanwezig').click(function () {
                $.ajax({
                    url: '<?php echo Request::root(); ?>/wedstrijd/status',
                    type: 'get',
                    data: {status: $('.btn-aanwezig').val(), id: '<?php echo $wed_id; ?>'},
                    success: function (response) {

                        $('.btn-aanwezig').prop('disabled', true);
                        $('.td-status-<?php echo $user->id; ?>').text('Aanwezig');

                        $('.btn-afwezig').prop('disabled', false);
                        location.reload();
                    }
                });
            })

        </script>
@stop


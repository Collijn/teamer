@extends('base.master')

@section('content')
    <div class="row">
        <div class="col-md-4 top">
            {{ Form::open(array('url' => 'wedstrijd/'.$wedstrijd->id, 'method' => 'put' )) }}
            {{ Form::label('titel', 'Titel') }}
            {{ Form::text('titel', $wedstrijd->titel) }}
            <br>
            {{ Form::label('datum', 'Datum') }}
            {{ Form::text('datum', $wedstrijd->datum) }}
            <br>
            {{ Form::label('tijd', 'Tijd') }}
            {{ Form::text('tijd', $wedstrijd->tijd) }}
            <br>
            {{ Form::submit('Opslaan') }}
            {{ Form::close() }}
        </div>
    </div>
@stop
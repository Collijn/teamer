@extends('base.master')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h3 class="head-text">Wedstrijd aanmaken</h3>

        <form method="post" action="/wedstrijd" class="form-horizontal form-register">
            <div class="form-group @if ($errors->has('titel')) has-error @endif">
                <div class="col-sm-12">
                    <input type="text" name="titel" class="form-control" value="{{Input::old('titel')}}"
                           placeholder="Titel">
                </div>
            </div>
            <div class="form-group @if ($errors->has('datum')) has-error @endif">
                <div class="col-sm-12">
                    <input type="text" name="datum" class="form-control" value="{{Input::old('datum')}}"
                           placeholder="Datum">
                </div>
            </div>
            <div class="form-group @if ($errors->has('tijd')) has-error @endif">
                <div class="col-sm-12">
                    <input type="text" name="tijd" class="form-control" placeholder="Tijd">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-registreer btn-default">Aanmaken</button>
                </div>
            </div>
        </form>
    </div>
@stop
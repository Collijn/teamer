<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <?= javascript_include_tag() ?>
    <?= stylesheet_link_tag() ?>


    <title>Jumbotron Template for Bootstrap</title>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Teamer</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            @if(Auth::check() == true)
                <?php $user = Auth::user() ?>
                <p class="navbar-text navbar-right"><a href="#"
                                                       class="navbar-link">{{ $user->voornaam }} {{ $user->achternaam }}</a>
                    <a href="/logout" class="navbar-link">Logout</a></p>
                <p class="navbar-text navbar-right">Teamcode {{ $user->teams->teamcode }}</p>
            @else
                <form method="post" action="/login" class="navbar-form navbar-right">

                    <div class="form-group form-email @if ($errors->has('email')) has-error @endif">
                        <input type="text" name="emailLogin" placeholder="Email" value="{{Input::old('emailLogin')}}"
                               class="form-control"><br>

                        <div class="login-bot"><input type="checkbox"> Aangemeld blijven</div>
                    </div>

                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                        <input type="password" name="password" placeholder="Password" class="form-control"><br>

                        <div class="login-bot"><a class="login-bot" href="">Wachtwoord vergeten?</a></div>
                    </div>

                    <button type="submit" class="btn btn-success btn-login">Login</button>
                </form>
            @endif
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>


<div class="container">

    @yield('content')

</div>
<!-- /container -->

<div class="footer"></div>
</body>

</html>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>
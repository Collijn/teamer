@extends('base.master-footer')

@section('content')
    <div class="row">
        <div class="col-md-4 top">
            @if($user->groups_id == 1 OR $user->groups_id == 2)
                <h4 class="head-text">WEDSTRIJDEN
                    <button type="button" data-toggle="modal" data-target="#wedstrijdModal"
                            class="btn btn-modal btn-add btn-success"></button>
                </h4>

                <!-- Modal -->
                <div class="modal fade" id="wedstrijdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h3 class="head-text modal-title" id="myModalLabel">Wedstrijd Aanmaken</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="/wedstrijd"
                                                  class="form-horizontal form-register">
                                                <div class="form-group @if ($errors->has('titel')) has-error @endif">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="titel" class="form-control"
                                                               value="{{Input::old('titel')}}" placeholder="Titel">
                                                    </div>
                                                </div>
                                                <div class="form-group @if ($errors->has('datum')) has-error @endif">
                                                    <div class="col-sm-8">
                                                        <input type="text" name="datum" class="form-control"
                                                               value="{{Input::old('datum')}}" placeholder="Datum">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="tijd" class="form-control"
                                                               placeholder="Tijd">
                                                    </div>
                                                </div>
                                                <div class="form-group @if ($errors->has('mededeling')) has-error @endif">
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" rows="5" name="bijzonder"
                                                                  placeholder="Bijzonder"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-registreer btn-default">
                                                            Aanmaken
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <h4 class="head-text">WEDSTRIJDEN</h4>
            @endif
            <table class="table">
                @if($wedstrijden->count())
                    @foreach($wedstrijden as $wedstrijd)
                        <tr class='clickable-row' data-href='wedstrijd/{{ $wedstrijd->id }}'>
                            <td>
                                {{ $wedstrijd->titel }}
                            </td>
                            <td>
                                {{ date("d-m-y", strtotime($wedstrijd->datum)) }}
                            </td>
                            <td>
                                {{ date("G:i", strtotime($wedstrijd->tijd)) }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            Geen wedstrijden beschikbaar
                        </td>
                    </tr>
                @endif
            </table>
        </div>
        <div class="col-md-4 top">
            @if($user->groups_id == 1 OR $user->groups_id == 2)
                <h4 class="head-text">TRAININGEN
                    <button type="button" data-toggle="modal" data-target="#trainingModal"
                            class="btn btn-modal btn-add btn-success"></button>
                </h4>

                <!-- Modal -->
                <div class="modal fade" id="trainingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h3 class="head-text modal-title" id="myModalLabel">Training Aanmaken</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="/training"
                                                  class="form-horizontal form-register">
                                                <div class="form-group @if ($errors->has('datum')) has-error @endif">
                                                    <div class="col-sm-8">
                                                        <input type="text" name="datum" class="form-control"
                                                               value="{{Input::old('datum')}}" placeholder="Datum">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="tijd" class="form-control"
                                                               placeholder="Tijd">
                                                    </div>
                                                </div>
                                                <div class="form-group @if ($errors->has('mededeling')) has-error @endif">
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" rows="5" name="bijzonder"
                                                                  placeholder="Bijzonder"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-registreer btn-default">
                                                            Aanmaken
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <h4 class="head-text">TRAININGEN</h4>
            @endif
            <table class="table">

                @if($trainingen->count())
                    @foreach($trainingen as $training)
                        <tr class='clickable-row' data-href='training/{{$training->id}}'>
                            <td>
                                <?php setlocale(LC_TIME, 'dutch'); ?>
                                {{ strftime('%A, %d %B',strtotime($training->datum))}}
                            </td>
                            <td>
                                {{ date("G:i", strtotime($training->tijd)) }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            Geen trainingen beschikbaar
                        </td>
                    </tr>
                @endif

            </table>
        </div>
        <div class="col-md-4 top">
            @if($user->groups_id == 1 OR $user->groups_id == 2)
                <h4 class="head-text">MEDEDELINGEN
                    <button type="button" data-toggle="modal" data-target="#mededelingModal"
                            class="btn btn-modal btn-add btn-success"></button>
                </h4>

                <!-- Modal -->
                <div class="modal fade" id="mededelingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h3 class="head-text modal-title" id="myModalLabel">Mededeling Aanmaken</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="mededeling/create"
                                                  class="form-horizontal form-register">
                                                <div class="form-group @if ($errors->has('titel')) has-error @endif">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="titel" class="form-control"
                                                               value="{{Input::old('titel')}}" placeholder="Titel">
                                                    </div>
                                                </div>
                                                <div class="form-group @if ($errors->has('mededeling')) has-error @endif">
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" rows="5" name="mededeling"
                                                                  placeholder="Mededeling"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-registreer btn-default">
                                                            Opslaan
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <h4 class="head-text">MEDEDELINGEN</h4>
            @endif
            <table class="table">
                @if($mededelingen->count())
                    @foreach($mededelingen as $mededeling)
                        <tr class='link-row' data-toggle="modal"
                            data-target="#mededelingModalShow-{{ $mededeling->id }}">
                            <td>
                                {{ $mededeling->titel }}
                            </td>
                        </tr>

                        <!-- Modal -->
                        <div class="modal fade" id="mededelingModalShow-{{ $mededeling->id }}" tabindex="-1"
                             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h3 class="head-text modal-title"
                                            id="myModalLabel">{{ $mededeling->titel }}</h3>
                                    </div>
                                    <div class="modal-body">
                                        {{ $mededeling->mededeling }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <tr>
                        <td>
                            Geen meldingen beschikbaar
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>

@stop



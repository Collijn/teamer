@extends('base.master')

@section('content')
    <div class="row">
        <div class="col-md-3 top">
            <h3 style="float: left">SPELERS</h3>

            @foreach($training_users as $training_user)
                @if($training_user->user_id == $user->id)
                    @if($training_user->status_id == 3)
                        <button style="float: right; margin-top: 20px;" type="button" value='3'
                                class="btn btn-danger btn-afwezig btn-sm" disabled>Afwezig
                        </button>
                    @else
                        <button style="float: right; margin-top: 20px;" type="button" value='3'
                                class="btn btn-danger btn-afwezig btn-sm">Afwezig
                        </button>
                    @endif

                    @if($training_user->status_id == 2)
                        <button style="float: right; margin-top: 20px;" type="button" value='2'
                                class="btn btn-success btn-sm" disabled>Aanwezig
                        </button>
                    @else
                        <button style="float: right; margin-top: 20px;" type="button" value='2'
                                class="btn btn-success btn-sm">Aanwezig
                        </button>
                    @endif
                @endif
            @endforeach

            <div id="players">
                <table class="table">
                    @foreach($training_users as $training_user)
                        <tr>
                            <td>
                                {{ $training_user->user->voornaam }} {{ $training_user->user->achternaam }}
                            </td>
                            <td style="text-align: right;" class="td-status-{{ $training_user->user_id }}">
                                {{ $training_user->status->status }}
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>

        <div class="col-md-6 top">
            <h3>BIJZONDER</h3>
        </div>

        <script>
            $('.btn-afwezig').click(function () {
                $.ajax({
                    url: '<?php echo Request::root(); ?>/training/status',
                    type: 'get',
                    data: {status: $('.btn-afwezig').val(), id: '<?php echo $training_id; ?>'},
                    success: function (response) {

                        $('.btn-afwezig').prop('disabled', true);
                        $('.td-status-<?php echo $user->id; ?>').text('Afwezig');

                        $('.btn-success').prop('disabled', false);
                    }
                });
            });

            $('.btn-success').click(function () {
                $.ajax({
                    url: '<?php echo Request::root(); ?>/training/status',
                    type: 'get',
                    data: {status: $('.btn-success').val(), id: '<?php echo $training_id; ?>'},
                    success: function (response) {

                        $('.btn-success').prop('disabled', true);
                        $('.td-status-<?php echo $user->id; ?>').text('Aanwezig');

                        $('.btn-afwezig').prop('disabled', false);
                    }
                });
            })
        </script>
@stop


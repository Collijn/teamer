@extends('base.master')

@section('content')
    <div class="row">
        <div class="col-md-4 top">
            {{ Form::open(array('url' => 'training/'.$training->id, 'method' => 'put' )) }}
            {{ Form::label('datum', 'Datum') }}
            {{ Form::text('datum', $training->datum) }}
            <br>
            {{ Form::label('tijd', 'Tijd') }}
            {{ Form::text('tijd', $training->tijd) }}
            <br>
            {{ Form::submit('Opslaan') }}
            {{ Form::close() }}
        </div>
    </div>
@stop
<?php

Route::get('register', 'LoginController@getRegister');

Route::post('register', 'LoginController@postRegister');
Route::post('login', 'LoginController@postLogin');

Route::get('/', 'HomeController@index');

Route::post('team/teamcode', 'TeamController@postTeamcode');
Route::post('team/registreren', 'TeamController@postRegister');

Route::get('team/registreren', 'TeamController@getRegister');

Route::group(array('before' => 'auth|admin'), function () {
    Route::post('mededeling/destroy/{id}', 'MededelingController@store');
    Route::post('mededeling/create', 'MededelingController@store');
    Route::post('wedstrijd/opstelling', 'WedstrijdController@postOpstelling');
    Route::post('wedstrijd/opstellingformatie', 'WedstrijdController@postOpstellingFormatie');
    Route::get('wedstrijd/status', 'StatusController@getWedstrijdEdit');
    Route::get('training/status', 'StatusController@getTrainingEdit');
    Route::resource('wedstrijd', 'WedstrijdController');
    Route::resource('training', 'TrainingController');
});

Route::group(array('before' => 'auth'), function () {
    Route::get('wedstrijd/status', 'StatusController@getWedstrijdEdit');
    Route::get('training/status', 'StatusController@getTrainingEdit');
    Route::get('logout', 'LoginController@getLogout');
    Route::resource('wedstrijd', 'WedstrijdController', array('only' => array('index', 'show')));
    Route::resource('training', 'TrainingController', array('only' => array('index', 'show')));
});






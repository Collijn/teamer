<?php

class TeamController extends BaseController
{

    public function getRegister()
    {
        Return View::make('base.team.register');
    }

    public function postRegister()
    {
        //Registreer formulier valideren
        $data = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
            'voornaam' => Input::get('voornaam'),
            'achternaam' => Input::get('achternaam'),
            'teamnaam' => Input::get('teamnaam'),
            'teamadress' => Input::get('teamadress')
        );

        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'voornaam' => 'required',
            'achternaam' => 'required',
            'teamnaam' => 'required|unique:teams,naam',
            'teamadress' => 'required'
        );

        //Formulier valideren
        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {

            $code = $this->generateCode();

            //Team in database zetten
            $team = new Teams;
            $team->naam = $data['teamnaam'];
            $team->adress = $data['teamadress'];
            $team->teamcode = $code;
            $team->save();

            //Gebruiker in database zetten
            $user = New User;
            $user->voornaam = $data['voornaam'];
            $user->achternaam = $data['achternaam'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->teams_id = $team->id;
            $user->groups_id = 1;
            $user->save();

            $user1 = User::find($user->id);

            //Nieuwe gebruiker inloggen
            Auth::login($user1);

            return Redirect::to('/');
        } else {
            return Redirect::to('team/registreren')->withErrors($validator)->withInput(Input::except('password'));
        }
    }

    public function postTeamcode()
    {
        //Vraag teamnaam op
        $team = Teams::where('teamcode', '=', Input::get('teamcode'))->first();

        if ($team) {
            return $team->naam;
        } else {
            return '';
        }

    }

    public function generateCode()
    {
        //Genereer een unique random code
        $code = str_random(6);

        $team = Teams::where('teamcode', '=', $code)->first();

        if ($team != NULL) {
            $this->generateCode();
        } else {
            return $code;
        }
    }
}
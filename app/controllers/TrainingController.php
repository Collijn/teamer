<?php

class TrainingController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return View::make('base.training.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $user = Auth::user();

        $trainingen = new Trainingen;
        $trainingen->datum = Input::get('datum');
        $trainingen->tijd = Input::get('tijd');
        $trainingen->teams_id = $user->teams_id;
        $trainingen->save();

        $users = User::where('teams_id', '=', $user->teams_id)->get();

        foreach ($users as $user) {
            $training_user = new TrainingenUser;
            $training_user->user_id = $user->id;
            $training_user->trainingen_id = $trainingen->id;
            $training_user->status_id = 1;
            $training_user->save();
        }

        return Redirect::to('/training/' . $trainingen->id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();

        $training = Trainingen::where('teams_id', '=', $user->teams_id)->where('id', '=', $id)->get();

        $trainingen_users = TrainingenUser::where('trainingen_id', '=', $id)->get();

        $data = array(
            'training_users' => $trainingen_users,
            'training_id' => $id,
            'user' => $user
        );

        if ($training->count()) {
            return View::make('base.training.show', $data);
        } else {
            return Redirect::to('/');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $training = Trainingen::where('id', '=', $id)->first();

        return View::make('base.training.edit')->with('id', $id)->with('training', $training);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $training = Trainingen::where('id', '=', $id)->first();

        $training->datum = Input::get('datum');
        $training->tijd = Input::get('tijd');

        $training->save();

        return Redirect::to('/training/' . $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $training = Trainingen::where('id', '=', $id)->first();
        $training->delete();

        $trai_users = TrainingenUser::where('trainingen_id', '=', $id)->get();

        foreach ($trai_users as $trai_user) {
            $user = TrainingenUser::find($trai_user->id);
            $user->delete();
        }

        return Redirect::to('/');
    }


}

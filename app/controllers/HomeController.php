<?php

class HomeController extends BaseController
{

    public function index()
    {
        if (Auth::check()) {
            $user = Auth::user();

            $wedstrijden = Wedstrijden::where('teams_id', '=', $user->teams_id)->get();
            $trainingen = Trainingen::where('teams_id', '=', $user->teams_id)->get();
            $mededelingen = Mededelingen::where('teams_id', '=', $user->teams_id)->orderBy('created_at', 'desc')->get();

            $data = array(
                'wedstrijden' => $wedstrijden,
                'trainingen' => $trainingen,
                'mededelingen' => $mededelingen,
                'user' => $user
            );

            return View::make('base.home', $data);
        } else {
            return View::make('base.guest');
        }
    }

}

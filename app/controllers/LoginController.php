<?php

Class LoginController extends BaseController
{
    public function getLogout()
    {
        Auth::logout();

        return Redirect::to('/');
    }

    public function postLogin()
    {

        $user = array(
            'email' => Input::get('emailLogin'),
            'password' => Input::get('password')
        );

        $rules = array(
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        );

        $validator = Validator::make($user, $rules);

        if ($validator->passes()) {
            if (Auth::attempt($user)) {
                return Redirect::to('/');
            } else {
                return Redirect::back()->withInput(Input::except('password'));
            }
        } else {
            return Redirect::back()->withErrors($validator)->withInput(Input::except('password'));
        }
    }

    public function postRegister()
    {
        //Registreer formulier valideren
        $data = array(
            'emailRegister' => Input::get('email'),
            'passwordRegister' => Input::get('password'),
            'passwordRegister_confirmation' => Input::get('password_confirmation'),
            'voornaam' => Input::get('voornaam'),
            'achternaam' => Input::get('achternaam'),
            'teamcode' => Input::get('teamcode')
        );

        $rules = array(
            'emailRegister' => 'required|email|unique:users,email',
            'passwordRegister' => 'required|confirmed',
            'passwordRegister_confirmation' => 'required',
            'voornaam' => 'required',
            'achternaam' => 'required',
            'teamcode' => 'required'
        );

        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            $team = Teams::where('teamcode', '=', $data['teamcode'])->first();

            //Nieuwe gebruiker aanmaken
            $user = New User;
            $user->voornaam = $data['voornaam'];
            $user->achternaam = $data['achternaam'];
            $user->email = $data['emailRegister'];
            $user->password = Hash::make($data['passwordRegister']);
            $user->teams_id = $team->id;
            $user->save();

            $user1 = User::find($user->id);

            //Nieuwe gebruiker inloggen
            Auth::login($user1);

            return Redirect::to('/');
        } else {
            return Redirect::to('/')->withErrors($validator)->withInput(Input::except('password'));
        }
    }
}
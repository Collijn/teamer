<?php

class StatusController extends BaseController
{

    public function getWedstrijdEdit()
    {
        $user = Auth::user();

        $wed_user = WedstrijdenUser::where('user_id', '=', $user->id)->where('wedstrijden_id', '=', Input::get('id'))->first();
        $wed_user->status_id = Input::get('status');
        $wed_user->save();
    }

    public function getTrainingEdit()
    {
        $user = Auth::user();

        $training_user = TrainingenUser::where('user_id', '=', $user->id)->where('trainingen_id', '=', Input::get('id'))->first();
        $training_user->status_id = Input::get('status');
        $training_user->save();
    }
}
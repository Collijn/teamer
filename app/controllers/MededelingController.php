<?php

class MededelingController extends BaseController
{
    public function store()
    {
        $user = Auth::user();

        //de mededeling opslaan in de database
        $mededeling = new Mededelingen;
        $mededeling->titel = Input::get('titel');
        $mededeling->mededeling = Input::get('mededeling');
        $mededeling->teams_id = $user->teams_id;
        $mededeling->save();

        return Redirect::to('/');

    }

    public function destroy($id)
    {
        //mededeling verwijderen
        $mededeling = Mededelingen::find($id);
        $mededeling->delete();
    }
}
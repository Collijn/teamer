<?php

class WedstrijdController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return Redirect::to('/');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return View::make('base.match.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $user = Auth::user();

        $wedstrijden = new Wedstrijden;
        $wedstrijden->titel = Input::get('titel');
        $wedstrijden->datum = Input::get('datum');
        $wedstrijden->tijd = Input::get('tijd');
        $wedstrijden->teams_id = $user->teams_id;
        $wedstrijden->save();

        $users = User::where('teams_id', '=', $user->teams_id)->get();

        foreach ($users as $user) {
            $wed_user = new WedstrijdenUser;
            $wed_user->user_id = $user->id;
            $wed_user->wedstrijden_id = $wedstrijden->id;
            $wed_user->status_id = 1;
            $wed_user->save();
        }

        return Redirect::to('/wedstrijd/' . $wedstrijden->id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $wedstrijd = Wedstrijden::where('teams_id', '=', $user->teams_id)->where('id', '=', $id)->first();
        $selectie = Selectie::where('wedstrijd_id', '=', $id)->get();

        if ($wedstrijd) {
            $users = User::where('teams_id', '=', $user->teams_id)->get();

            foreach ($users as $user) {
                $wed_usr = WedstrijdenUser::where('user_id', '=', $user->id)->where('wedstrijden_id', '=', $id)->first();

                if ($wed_usr == null) {
                    $wed_user = new WedstrijdenUser;
                    $wed_user->user_id = $user->id;
                    $wed_user->wedstrijden_id = $wedstrijd->id;
                    $wed_user->status_id = 1;
                    $wed_user->save();
                }
            }

            $wed_users = WedstrijdenUser::where('wedstrijden_id', '=', $id)->get();
            $data = array(
                'wed_users' => $wed_users,
                'wed_id' => $id,
                'user' => $user,
                'wedstrijd' => $wedstrijd,
                'selectie' => $selectie
            );

            return View::make('base.match.show', $data);
        } else {
            return Redirect::to('/');
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $wedstrijd = Wedstrijden::where('id', '=', $id)->first();

        return View::make('base.match.edit')->with('id', $id)->with('wedstrijd', $wedstrijd);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
        $wedstrijd = Wedstrijden::find($id);
        $wedstrijd->titel = Input::get('titel');
        $wedstrijd->datum = Input::get('datum');
        $wedstrijd->tijd = Input::get('tijd');
        $wedstrijd->save();

        return Redirect::to('/wedstrijd/' . $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $wedstrijd = Wedstrijden::find($id);
        $wedstrijd->delete();

        $wed_users = WedstrijdenUser::where('wedstrijden_id', '=', $id)->get();

        foreach ($wed_users as $wed_user) {
            $user = WedstrijdenUser::find($wed_user->id);
            $user->delete();
        }

        return Redirect::to('/');
    }

    public function postOpstelling()
    {

        $players = Input::get('players');
        $wedId = Input::get('wed_id');

        $playersObject = json_decode($players, true);

        $opstelling = Selectie::where('wedstrijd_id', $wedId)->get();

        if ($opstelling->count()) {
            foreach ($playersObject as $key => $player) {

                $playerNaam = explode(" ", $player);

                if (strlen($playerNaam[0]) > 0) {
                    $users = User::where('voornaam', '=', $playerNaam[0])->where('achternaam', '=', $playerNaam[1])->first();

                    $user = $users->id;

                } else {
                    $user = '';
                }

                $opstelling = Selectie::where('wedstrijd_id', $wedId)->where('positie', $key)->first();
                $opstelling->user_id = $user;
                $opstelling->save();
            }
        } else {
            foreach ($playersObject as $key => $player) {

                $playerNaam = explode(" ", $player);

                if (strlen($playerNaam[0]) > 0) {
                    $users = User::where('voornaam', '=', $playerNaam[0])->where('achternaam', '=', $playerNaam[1])->first();
                    $user = $users->id;
                } else {
                    $user = '';
                }

                $opstelling = new Selectie;
                $opstelling->positie = $key;
                $opstelling->user_id = $user;
                $opstelling->wedstrijd_id = $wedId;
                $opstelling->save();
            }
        }

        return Redirect::to('/wedstrijd/' . $wedId);

    }

    public function postOpstellingFormatie()
    {

        $wed_id = Input::get('wed_id');
        $formatie = Input::get('formatie');

        $wedstrijd = Wedstrijden::where('id', '=', $wed_id)->first();
        $wedstrijd->formatie = $formatie;
        $wedstrijd->save();
    }

}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
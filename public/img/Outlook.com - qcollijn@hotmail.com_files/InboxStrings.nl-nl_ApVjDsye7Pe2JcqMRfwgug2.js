ol.stringMerge(ol.strings, {
    ExpandCollapse: {
        Tooltip: {
            Label: {".": "{0} weergeven/verbergen"},
            ItemName: {QuickViews: {".": "categorieën"}, Folders: {".": "Mappen"}}
        }
    },
    ContainerSelector: {Apply: {".": "Toepassen"}},
    Pinning: {
        Quickview: {
            UpsellFormat: {
                LinkText: {".": "Met vlag markeren inschakelen"},
                ".": "Belangrijke berichten kun je boven in Postvak IN laten staan. {0}"
            }
        }
    },
    FolderList: {
        Header: {Label: {".": "Mappen"}},
        Actions: {
            ManageRules: {".": "Regels beheren"},
            Refresh: {".": "Vernieuwen"},
            AddFolder: {".": "Een nieuwe map toevoegen"}
        },
        DefaultNoSelection: {".": "<Map selecteren>"},
        ContextMenu: {
            NewSubfolder: {".": "Nieuwe submap"},
            EmptyFolder: {".": "Map leegmaken"},
            ForThisFolder: {".": "Voor deze map"},
            Rename: {".": "Naam wijzigen"},
            MarkAsRead: {".": "Alles markeren als gelezen"},
            Delete: {".": "Verwijderen"}
        }
    },
    EnhancedRules: {
        BlockAllFromSender: {
            Flyout: {
                SingleSender: {Title: {".": "Alle e-mail blokkeren van {0}"}},
                Button: {BlockAll: {".": "Alles blokkeren"}},
                MultiSenders: {Title: {".": "Alle e-mail blokkeren van"}}
            }
        },
        CategorizeAll: {
            OptionDialog: {
                Action: {".": "Alles categoriseren"},
                RemoveCategoriesMessage: {".": "Verwijderen uit {0}."},
                AddCategoriesMessage: {".": "Categoriseren als {0}."},
                Checkbox: {Label: {".": "Dit ook doen voor toekomstige berichten"}}
            }
        }
    },
    InstantActions: {
        Tooltips: {
            Move: {
                Targeted: {".": "Dit bericht verplaatsen naar {0}"},
                Generic: {".": "Dit bericht verplaatsen naar een map"}
            },
            MoveAndMark: {
                Targeted: {".": "Dit bericht verplaatsen naar {0} en markeren als gelezen"},
                Generic: {".": "Dit bericht verplaatsen naar een map en markeren als gelezen"}
            },
            Flag: {
                Unflagged: {
                    PinFlaggingDisabled: {".": "Dit bericht markeren"},
                    PinFlaggingEnabled: {".": "Dit bericht boven aan het Postvak IN laten staan"}
                }, Flagged: {".": "Markering van dit bericht opheffen"}
            },
            Archive: {".": "Dit bericht archiveren"},
            Categorize: {
                Remove: {".": "Dit bericht verwijderen van de categorie {0}"},
                Add: {".": "Dit bericht toevoegen aan de categorie {0}"}
            },
            Sweep: {
                Delete: {".": "Alle berichten van deze afzender verwijderen..."},
                Move: {".": "Alle berichten van deze afzender verplaatsen..."}
            },
            Delete: {".": "Dit bericht verwijderen"},
            Junk: {
                NotJunk: {".": "Deze afzender markeren als onveilig en dit bericht verwijderen"},
                Junk: {".": "Dit bericht markeren als niet-ongewenst en verplaatsen naar Postvak IN"}
            },
            Mark: {
                Read: {".": "Dit bericht markeren als ongelezen"},
                NotRead: {".": "Dit bericht markeren als gelezen"}
            }
        }
    },
    Categories: {
        Menu: {
            ApplyCategoryToSenders: {".": "Toepassen op alles van..."},
            ManageCategories: {".": "Categorieën beheren"},
            AddNewInlineCategory: {".": "Nieuwe categorie"},
            ApplyCategory: {".": "Toepassen"}
        }
    },
    AdvancedSearch: {
        Subject: {".": "Onderwerp bevat"},
        From: {".": "Van"},
        Folders: {".": "Mappen", AllFolders: {".": "Alle mappen"}},
        Search: {".": "Zoeken"},
        Keywords: {".": "Trefwoorden"},
        Categories: {".": "Categorieën", AllCategories: {".": "Alle categorieën"}},
        To: {".": "Aan"}
    },
    Footer: {Privacy: {Text: {".": "Privacy"}}},
    SpellChecker: {
        SpellCheck: {
            CompleteWithErrors: {".": "Tijdens de spellingcontrole zijn enkele woorden gevonden die je moet controleren."},
            CompleteNoErrors: {".": "De spellingcontrole is voltooid."},
            NoSpellingSuggestions: {".": "Geen spellingsuggesties"},
            InvalidRTEMode: {".": "Probeer de RTF-modus om de spelling te controleren"},
            Title: {".": "Mogelijke spelfouten opsporen"},
            DirtySend: {".": "De spelling van het bericht controleren voor verzenden?"}
        }, Misspelling: {SuggestionTooltip: {".": "Klik op dit woord om spellingsuggesties weer te geven"}}
    },
    ErrorDhtml: {Ok: {Button: {".": "OK"}}},
    ConnectivityLossUi: {Title: {".": "Internetverbinding verbroken"}},
    ManageFolders: {AddFolder: {Title: {".": "Nieuwe map"}}},
    Search: {
        DateFilterTitle: {
            AndLater: {".": "{0} (en later)"},
            DateAndRange: {".": "{0} {1}"},
            AndEarlier: {".": "{0} (en eerder)"}
        },
        Categories: {MultipleCategories: {".": "{0} categorieën"}},
        DateRange: {
            ThreeMonths: {".": "± 3 maanden"},
            ".": "Bereik",
            OneMonth: {".": "± 1 maand"},
            AndLater: {".": "later dan datum"},
            TwoWeeks: {".": "± 2 weken"},
            AndEarlier: {".": "eerder dan datum"},
            OneYear: {".": "± 1 jaar"},
            OneDay: {".": "± 1 dag"},
            OneWeek: {".": "± 1 week"},
            ThreeDays: {".": "± 3 dagen"},
            SixMonths: {".": "± 6 maanden"},
            ExactDate: {".": "exacte datum"}
        },
        Folder: {".": "Map"},
        SearchInContainer: {".": "{0} zoeken"},
        SearchWithFilters: {".": "Zoeken met filters"},
        Attachments: {
            AnyAttachment: {".": "Alles"},
            DoesNotHaveAttachment: {".": "Zonder bijlage"},
            HasAttachment: {".": "Met bijlage"},
            ".": "Bijlagen"
        },
        Date: {".": "Datum"},
        Folders: {MultipleFolders: {".": "{0} mappen"}},
        SearchAllMail: {".": "E-mail doorzoeken"},
        Category: {".": "Categorie"}
    },
    ManageCategories: {
        QuickView: {".": "Weergeven in mappenvenster"},
        PersonalCategories: {".": "Persoonlijke categorieën"},
        NewCategory: {".": "Nieuwe categorie"},
        Remove: {".": "Verwijderen"},
        ShowCategories: {".": "De categoriekolom in de berichtenlijst weergeven"},
        Filter: {".": "Filter"},
        ShowHideCategoriesTitle: {".": "Categorieën beheren"}
    },
    Entity: {
        MeetingSuggestion: {
            Tooltip: {".": "Add to calendar/View calendar"},
            ViewAgenda: {".": "View calendar"},
            AddEvent: {".": "Add to calendar"}
        }, Address: {Tooltip: {".": "View on map"}}, Email: {Tooltip: {".": "Een nieuw bericht schrijven"}}
    },
    MoveTo: {Folder: {NewFolder: {".": "Nieuwe map"}}},
    KeyboardShortcuts: {
        Alert: {Text: {".": "Dat was een sneltoets. Klik Annuleren als dat niet de bedoeling was. Ga naar Opties om sneltoetsen uit te schakelen of de instellingen te wijzigen."}},
        Hotmail: {
            showFontNames: {Description: {".": "Ctrl+Shift+F"}, Shortcut: {".": null}},
            underline: {Description: {".": "Ctrl+U"}, Shortcut: {".": null}},
            insertLink: {Description: {".": "Ctrl+K"}, Shortcut: {".": null}},
            justifyLeft: {Description: {".": "Ctrl+L"}, Shortcut: {".": null}},
            emojiPicker: {Description: {".": "Ctrl+Shift+Y"}, Shortcut: {".": null}},
            bold: {Description: {".": "Ctrl+B"}, Shortcut: {".": null}},
            justifyRight: {Description: {".": "Ctrl+R"}, Shortcut: {".": null}},
            outdent: {Description: {".": "Alt+Shift+Left"}, Shortcut: {".": null}},
            insertNumberedList: {Description: {".": "Ctrl+Shift+M"}, Shortcut: {".": null}},
            showFontSizes: {Description: {".": "Ctrl+Shift+S"}, Shortcut: {".": null}},
            justifyCenter: {Description: {".": "Ctrl+E"}, Shortcut: {".": null}},
            italic: {Description: {".": "Ctrl+I"}, Shortcut: {".": null}},
            showHighlightColors: {Description: {".": "Ctrl+Shift+H"}, Shortcut: {".": null}},
            showFontColors: {Description: {".": "Ctrl+Shift+C"}, Shortcut: {".": null}},
            indent: {Description: {".": "Alt+Shift+Right"}, Shortcut: {".": null}},
            insertBulletedList: {Description: {".": "Ctrl+Shift+L"}, Shortcut: {".": null}}
        },
        Gmail: {
            showFontNames: {Description: {".": null}, Shortcut: {".": null}},
            underline: {Description: {".": "Ctrl+U"}, Shortcut: {".": null}},
            insertLink: {Description: {".": null}, Shortcut: {".": null}},
            justifyLeft: {Description: {".": "Ctrl+Shift+L"}, Shortcut: {".": null}},
            emojiPicker: {Description: {".": null}, Shortcut: {".": null}},
            bold: {Description: {".": "Ctrl+B"}, Shortcut: {".": null}},
            justifyRight: {Description: {".": "Ctrl+Shift+R"}, Shortcut: {".": null}},
            outdent: {Description: {".": "Ctrl+["}, Shortcut: {".": null}},
            insertNumberedList: {Description: {".": "Ctrl+Shift+7"}, Shortcut: {".": null}},
            showFontSizes: {Description: {".": null}, Shortcut: {".": null}},
            justifyCenter: {Description: {".": "Ctrl+Shift+E"}, Shortcut: {".": null}},
            italic: {Description: {".": "Ctrl+I"}, Shortcut: {".": null}},
            showHighlightColors: {Description: {".": null}, Shortcut: {".": null}},
            showFontColors: {Description: {".": null}, Shortcut: {".": null}},
            indent: {Description: {".": "Ctrl+]"}, Shortcut: {".": null}},
            insertBulletedList: {Description: {".": "Ctrl+Shift+8"}, Shortcut: {".": null}}
        },
        Tooltip: {Format: {".": "{0} ({1})"}}
    },
    CodePage: {Unknown: {".": "Onbekende ({0})"}, AutoSelect: {".": "Automatisch selecteren"}},
    MiniCalendarControl: {Loading: {".": "Laden..."}},
    Archive: {ArchiveFolder: {Name: {".": "Archief"}}},
    PinBoard: {
        CollapseWidget: {
            Show: {Title: {".": "Gemarkeerde e-mailberichten weergeven"}},
            Hide: {Title: {".": "Gemarkeerde e-mailberichten verbergen"}}
        },
        Alert: {
            Disable: {
                Title: {".": "Met vlag markeren uitschakelen"},
                Button: {Title: {".": "Uitschakelen"}},
                Message: {".": "Je staat op het punt de sectie voor markeren met vlag boven in je Postvak IN te verwijderen. Via Opties kun je deze sectie altijd weer terugzetten."}
            }
        },
        TrayMessage: {Show: {".": "Weergeven"}, Hide: {".": "Verbergen"}, Flagged: {".": "Gemarkeerd als {0}"}}
    },
    ContextMessage: {
        CreateRule: {".": "Regel maken"},
        EmptyFolder: {".": "Leegmaken"},
        Archive: {".": "Archiveren"},
        SendEmail: {".": "E-mail verzenden"},
        Forward: {".": "Doorsturen"},
        Delete: {".": "Verwijderen"},
        ViewSource: {".": "Berichtbron weergeven"},
        Reply: {".": "Beantwoorden"},
        ReportAndDelete: {".": "Ongewenst"},
        Move: {".": "Verplaatsen"},
        ReplyAll: {".": "Allen beantwoorden"},
        NotJunk: {".": "Niet ongewenst"},
        MarkRead: {".": "Markeren als gelezen"},
        MarkUnread: {".": "Markeren als ongelezen"},
        FindEmail: {".": "E-mail zoeken"},
        SaveToOneNote: {".": "Opslaan in OneNote"}
    },
    MoveAllFromSender: {Generic: {".": "Alle e-mail verplaatsen van..."}},
    Accessibility: {
        MessageBodyLabel: {".": "Romptekst bericht"},
        ListHeader: {CheckboxLabel: {SelectNone: {".": "Niets selecteren"}, ".": "Alles selecteren"}},
        MessageSubjectLabel: {".": "Berichtonderwerp"},
        MessageList: {
            Checkbox: {
                Label: {
                    HighImportance: {".": "Hoge urgentie. "},
                    MoreMessages: {".": "Nog {0} berichten in gesprek. "},
                    Unread: {".": "Ongelezen. "},
                    Template: {".": "{0}{1}{2}{3}{4}. {5}. {6}{7}"},
                    HasAttachments: {".": "Met bijlagen. "},
                    Flagged: {".": "Gemarkeerd. "},
                    MoreSenders: {".": "{0} en anderen. "},
                    LowImportance: {".": "Lage urgentie. "},
                    SingleSender: {".": "{0}. "}
                }
            }
        }
    },
    Carousel: {
        SkyDrive: {Close: {".": "Sluiten"}, GoToSkyDrive: {".": "Naar OneDrive.com"}},
        WaitForInitialization: {".": "De instelling is nog niet klaar. Probeer het nog een keer."},
        ServiceErrorRefreshPage: {".": "Er kan geen verbinding worden gemaakt met de service. Vernieuw de pagina."},
        UploadCanceled: {".": "Uploaden geannuleerd"},
        UpsellSkydriveOverQuantityFormat: {".": "Je bestanden kunnen niet worden toegevoegd omdat deze de limiet van {0} bestanden overschrijden. Gebruik OneDrive om deze bestanden te verzenden."},
        FilesWithErrorsWarning: {".": "Bestanden met fouten worden niet geüpload."},
        UploadInternalError: {".": "Er is iets misgegaan bij het bijvoegen van dit bestand."},
        UploadBusyOk: {".": "OK"},
        UploadProgressFormat: {".": "{0} van {1} bijgewerkt ({2} MB)"},
        AlreadyUploading: {".": "Je kunt maar één bestand per keer bijvoegen. Wacht totdat het bestand is bijgevoegd en voeg dan pas een ander bestand bij."},
        UploadServerError: {".": "Er is iets misgegaan bij het bijvoegen van dit bestand."},
        UpsellSkydriveOverSizeLimitFormat: {".": "Je bestanden kunnen niet worden toegevoegd omdat deze de limiet van {0} MB overschrijden. Gebruik OneDrive om deze bestanden te verzenden."},
        ErrorLoadingFile: {".": "Er is iets misgegaan bij het bijvoegen van dit bestand."},
        EmptyFile: {".": "Dit bestand is leeg. Voeg een ander bestand toe."},
        UploadError: {".": "Er is iets misgegaan bij het bijvoegen van dit bestand."},
        UploadUnrecognizedResponse: {".": "Er is iets misgegaan bij het bijvoegen van dit bestand."},
        FileExceededMaxBytes: {".": "Met dit bestand overschrijd je de limiet voor bijlagen."},
        ShowHideAttachments: {".": "Bijlagen weergeven/verbergen"}
    },
    LiveViews: {External: {Updating: {".": "Updaten..."}}},
    Compose: {DraftHeaderLabel: {".": "Concept"}, ChangeHeader: {".": "Wijzigen"}},
    NewOptions: {Title: {".": "New Options"}},
    Contact: {EditContact: {".": "Contactpersoon bewerken"}},
    QuickView: {
        NewCategory: {".": "Nieuwe categorie"},
        Header: {
            Label: {".": "Categorieën"},
            ContextMenu: {
                NewCategory: {".": "Nieuwe categorie toevoegen"},
                ManageCategories: {".": "Categorieën beheren"}
            }
        },
        ContextMenu: {
            EmptyQuickView: {".": "Weergave leegmaken"},
            ManageCategories: {".": "Categorieën beheren"},
            Rename: {".": "Naam wijzigen"},
            Delete: {".": "Verwijderen"},
            ForThisQuickView: {".": "Voor deze categorie"}
        }
    },
    InlineImage: {
        Small: {".": "Klein (25%)"},
        Medium: {".": "Gemiddeld (50%)"},
        ErrorsInRTE: {".": "Bestanden met fouten worden verwijderd uit de e-mail en worden niet verzonden."},
        Large: {".": "Groot (200%)"},
        Remove: {".": "Verwijderen"},
        ErrorsInRTEAction: {".": "Verzenden"},
        InlineIsNotAnImageError: {".": "Dit bestand is geen afbeelding en kan niet inline worden ingevoegd"},
        Original: {".": "Origineel (100%)"}
    },
    ReadMessage: {
        Alert: {UnsubscribeAndBlockNotice: {".": "Deze afzender is uit je lijst met contacten verwijderd en deze afzender heeft een verzoek ontvangen om jou te verwijderen uit de adressenlijst."}},
        HideMessageHistory: {".": "Berichtgeschiedenis verbergen"},
        SaveOneDriveProgressCancel: {".": "Annuleren"},
        SaveOneDriveIndeterminateTextMultiple: {".": "{0} bestanden uploaden naar {1}"},
        Delete: {".": "Verwijderen"},
        Newsletters: {
            UnsubscribeDialog: {
                UnsubscribeBodyMissingFormat: {".": "{0} heeft niet aangegeven hoe je je kunt afmelden, dus alles van de volgende afzender wordt geblokkeerd: {1}"},
                UnsubscribeBodyMissingFromName: {".": "Deze afzender"},
                UnsubscribeHeader: {".": "Deze nieuwsbrief niet meer ontvangen"},
                UnsubscribeBodyEmailFormat: {".": "We vragen {0} om je uit de mailinglijst te verwijderen. Alles wat je in de tussentijd toegezonden krijgt, wordt in de map Ongewenste e-mail geplaatst."},
                BlockButton: {".": "Blokkeren"},
                UnsubscribeButton: {".": "Afmelden"},
                UnsubscribeBodyLinkFormat: {
                    ".": "Ga naar de {1} van {0} om je af te melden. Alles wat je in de tussentijd toegezonden krijgt, wordt in de map Ongewenste e-mail geplaatst.",
                    LinkText: {".": "website"}
                },
                SweepOptionTextFormat: {".": "Ook alles van {0} in de map {1} verwijderen"},
                BlockHeader: {".": "Deze afzender blokkeren"}
            }
        },
        PrintMessage: {".": "Bericht afdrukken"},
        TrustedSender: {Popup: {Text: {".": "Dit bericht is van een betrouwbare afzender. We hebben gecontroleerd of het bericht veilig is om phishing te voorkomen."}}},
        SaveOneDriveLocation: {".": "E-mailbijlagen"},
        Actions: {".": "Acties"},
        FullMessageViewLink: {Text: {".": "Volledige weergave"}},
        Reply: {".": "Beantwoorden"},
        NextMessage: {".": "Volgend bericht"},
        ReplyAll: {".": "Allen beantwoorden"},
        SaveOneDriveIndeterminateTextSingular: {".": "{0} bestand uploaden naar {1}"},
        PreviousMessage: {".": "Vorig bericht"},
        TotalCountText: {".": "{0} berichten"},
        MarkAsJunk: {
            ".": "Ongewenste e-mail",
            Title: {".": "Bericht wordt verplaatst naar Ongewenste e-mail"},
            SenderInWhiteList: {
                ".": "Je meldt dit bericht van {0} als ongewenste e-mail. Je kunt ook de afzender verwijderen uit je lijst met contactpersonen en verdere berichten van deze persoon blokkeren.",
                RemoveFromSafeList: {".": "Veilige afzender blokkeren"},
                KeepInSafeList: {".": "Veilige afzender behouden"}
            },
            SenderCompromised: {
                ".": "Je meldt dit bericht van {0} als ongewenste e-mail. Je kunt ook de afzender verwijderen uit je lijst met veilige afzenders en verdere berichten van deze persoon blokkeren.",
                CheckBox: {".": "Volgens mij is deze persoon gehackt!"}
            },
            SenderInContactList: {
                ".": "Je meldt dit bericht van {0} als ongewenste e-mail. Je kunt ook de afzender verwijderen uit je lijst met contactpersonen en verdere berichten van deze persoon blokkeren. Je kunt de verwijdering en blokkering later ongedaan maken op de pagina Personen.",
                KeepInContacts: {".": "Contactpersoon behouden"},
                RemoveFromContacts: {".": "Contactpersoon verwijderen en blokkeren"}
            }
        },
        SaveOneDriveProgressAction: {".": "Doorgaan"},
        AddContact: {".": "Toevoegen aan contactpersonen"},
        Encoding: {".": "Tekenset:"},
        HideSenderDetails: {".": "Koptekstgegevens verbergen"},
        MarkAsNotJunk: {".": "Niet ongewenst"},
        Forward: {".": "Doorsturen"},
        BlockedContentDialog: {
            ".": "Onderdelen van dit bericht zijn uit veiligheidsoverwegingen geblokkeerd. Je staat op het punt om deze inhoud te deblokkeren.",
            Button: {".": "Blokkering opheffen"}
        },
        SaveOneDriveProgressMessage: {".": "De bijlagen worden opgeslagen in OneDrive. Weet u zeker dat u deze pagina wilt verlaten?"},
        DeleteAllFromSender: {".": "Alles van deze afzender verwijderen"},
        SafetyBar: {Unsubscribe: {".": "Afmelden"}},
        ShowSenderDetails: {".": "Koptekstgegevens weergeven"},
        ShowMessageHistory: {".": "Berichtgeschiedenis weergeven"},
        MaxMessagesLimitHit: {".": "Omdat deze discussie erg lang is, worden niet alle berichten weergegeven. Ga voor oudere berichten terug naar het Postvak IN en sorteer op datum (in plaats van op discussie)."},
        AllowSender: {MultipleMessagesAllowedInfo: {".": "{0} is toegevoegd aan de lijst met veilige afzenders. Nu worden berichten van deze persoon altijd toegestaan."}},
        Junk: {AllowReportingJunkQuestion: {".": "Help ons ongewenste e-mail te bestrijden door deze bij ons te melden. We kunnen deze e-mail met andere bedrijven delen, zodat ze ons kunnen helpen om ongewenste e-mail te stoppen."}},
        Translation: {Title: {".": "Vertalen:"}},
        BackToMessages: {".": "Terug naar berichten"}
    },
    Folder: {
        Chat: {".": "Berichtgeschiedenis"},
        Drafts: {".": "Concepten"},
        Inbox: {".": "Postvak IN"},
        Deleted: {".": "Verwijderd"},
        Junk: {".": "Ongewenst"},
        Sent: {".": "Verzonden"},
        SearchResults: {".": "Zoekresultaten"}
    },
    ActionQueue: {PendingWarning: {".": "Nog een ogenblik geduld. Als de pagina wordt afgesloten voordat de verbinding is hersteld, wordt de laatste taak mogelijk niet uitgevoerd."}},
    Toolbar: {CategorizeDropdownTitle: {".": "Voeg categorieën toe aan berichten."}, POP: {".": "POP"}},
    MessageCategoryFilter: {
        Other: {Label: {".": "Alle andere"}},
        MailingList: {Label: {".": "Groepen"}},
        FlaggedMessages: {Label: {".": "Gemarkeerd"}}
    },
    Undo: {
        Items: {
            RemoveCategory: {
                Single: {".": "Voor 1 item is de categorie {0} verwijderd."},
                Multiple: {".": "Voor {1} items is de categorie {0} verwijderd."}
            },
            ApplyCategory: {
                Single: {".": "Op 1 item is de categorie {0} toegepast."},
                Multiple: {".": "Op {1} items is de categorie {0} toegepast."}
            },
            MoveItems: {
                Single: {".": "1 item is verplaatst naar je map {0}."},
                Inbox: {
                    Single: {".": "1 item is verplaatst naar je Postvak IN."},
                    Multiple: {".": "{0} items zijn verplaatst naar je Postvak IN."}
                },
                Multiple: {".": "{1} items zijn verplaatst naar je map {0}."}
            },
            Marked: {
                UnRead: {
                    Single: {".": "1 item is gemarkeerd als niet-gelezen."},
                    Multiple: {".": "{0} items zijn gemarkeerd als niet-gelezen."}
                },
                NotJunk: {
                    Inbox: {
                        Single: {".": "1 items is gemarkeerd als geen ongewenste e-mail en is verplaatst naar je Postvak IN."},
                        Multiple: {".": "{0} items zijn gemarkeerd als geen ongewenste e-mail en zijn verplaatst naar je Postvak IN."}
                    },
                    Single: {".": "1 item is gemarkeerd als geen ongewenste e-mail en is verplaatst naar je map {0}."},
                    Multiple: {".": "{1} items zijn gemarkeerd als geen ongewenste e-mail en zijn verplaatst naar de map {0}."}
                },
                Read: {
                    Single: {".": "1 item is gemarkeerd als gelezen."},
                    Multiple: {".": "{0} items zijn gemarkeerd als gelezen."}
                },
                Junk: {
                    Single: {".": "1 item is gemarkeerd als ongewenste e-mail en is verplaatst naar je map Ongewenste e-mail."},
                    Multiple: {".": "{0} items zijn gemarkeerd als ongewenste e-mail en zijn verplaatst naar je map Ongewenste e-mail."}
                }
            },
            Flagged: {Single: {".": "1 item is gemarkeerd."}, Multiple: {".": "{0} items zijn gemarkeerd."}},
            Tooltip: {
                InActive: {".": "Er zijn geen handelingen die ongedaan kunnen worden gemaakt."},
                Active: {
                    Move: {".": "Verplaatsen van {0} ongedaan maken"},
                    ".": "{0} {1} ongedaan maken",
                    Flag: {".": "Markering van {0} ongedaan maken"},
                    Read: {".": "Markeren als gelezen van {0} ongedaan maken"},
                    NotJunk: {".": "Markeren van {0} als gewenst ongedaan maken"},
                    Archive: {".": "Archiveren van {0} ongedaan maken"},
                    Categorize: {".": "Categoriseren van {0} ongedaan maken"},
                    Unread: {".": "Markeren als ongelezen van {0} ongedaan maken"},
                    Unflag: {".": "Markering van {0} opheffen ongedaan maken"},
                    Delete: {".": "Verwijderen van {0} ongedaan maken"},
                    Junk: {".": "Markeren van {0} als ongewenst ongedaan maken"}
                }
            },
            UnFlagged: {
                Single: {".": "De markering voor 1 item is verwijderd."},
                Multiple: {".": "De markering voor {0} items is verwijderd."}
            }
        },
        FreBubble: {
            Button: {Text: {".": "Ik snap het"}},
            Text: {".": "Je kunt nu bewerkingen ongedaan maken, zoals verwijderen, verplaatsen, categoriseren of markeren. Klik op deze knop of gebruik Ctrl+Z."}
        },
        Errors: {Generic: {".": "Er is iets verkeerd gegaan. Ongedaan maken kan niet worden uitgevoerd."}}
    },
    Curtain: {
        AlmostDoneStage: {".": "Bijna klaar..."},
        PreparingStage: {".": "Voorbereiden..."},
        LoadingStage: {".": "Laden..."}
    },
    BulkAction: {
        NothingSelectedError: {".": "Selecteer ten minste één e-mail."},
        AllFromSender: {
            Flyout: {
                MultiSenderSender: {Title: {".": "Voor alle e-mail van"}},
                SingleSender: {Title: {".": "Voor alle e-mail van {0}"}}
            }
        },
        SenderBased: {BulkSelectedError: {".": "Het lijkt erop dat u meer dan een pagina met afzenders hebt geselecteerd. Deze actie kan alleen voor één pagina met afzenders tegelijk worden uitgevoerd."}},
        Completed: {
            MultipleSendersBlocked: {".": "{0} De afzenders zijn geblokkeerd."},
            MoveRuleCreated: {
                archive: {".": "{0} Er is een regel gemaakt om toekomstige e-mail te archiveren."},
                "delete": {".": "{0} Er is een regel gemaakt om toekomstige e-mail te verwijderen."},
                move: {".": "{0} Er is een regel gemaakt om toekomstige e-mail te verplaatsen."}
            },
            MostRecentMessage: {
                archive: {".": "Alle e-mail, behalve de meest recente, is gearchiveerd. Dit wordt ook gedaan voor alle toekomstige e-mails."},
                "delete": {".": "Alle e-mail, behalve de meest recente van alle geselecteerde gebruikers, is verwijderd. Dit wordt ook gedaan voor alle toekomstige e-mails."},
                move: {".": "Alle e-mail, behalve de meest recente, is verplaatst. Dit wordt ook gedaan voor alle toekomstige e-mails."}
            },
            "10DayOldMessages": {
                archive: {".": "E-mail ouder dan 10 dagen is gearchiveerd. Dit wordt ook gedaan voor alle toekomstige e-mails."},
                "delete": {".": "E-mail ouder dan 10 dagen is verwijderd. Dit wordt ook gedaan voor alle toekomstige e-mails."},
                move: {".": "E-mail ouder dan 10 dagen is verplaatst. Dit wordt ook gedaan voor alle toekomstige e-mails."}
            },
            CategorizeRuleCreated: {".": "{0} Er is een regel gemaakt om toekomstige e-mail te categoriseren."},
            Generic: {
                plural: {
                    unsubscribe: {".": "De afzender is gevraagd om je te verwijderen van deze adressenlijst en er zijn {0} e-mails verwijderd."},
                    "delete": {".": "{0} e-mails zijn verwijderd."},
                    move: {".": "{0} e-mails zijn verplaatst naar {1}."},
                    block: {".": "{0} afzenders geblokkeerd."},
                    archive: {".": "{0} e-mails zijn gearchiveerd."},
                    categorize: {".": "{0} e-mails zijn gecategoriseerd."}
                },
                single: {
                    unsubscribe: {".": "De afzender is gevraagd om je te verwijderen van deze adressenlijst en er is {0} e-mail verwijderd."},
                    "delete": {".": "{0} e-mail is verwijderd."},
                    move: {".": "{0} e-mail is verplaatst naar {1}."},
                    block: {".": "{0} afzender geblokkeerd."},
                    archive: {".": "{0} email is gearchiveerd."},
                    categorize: {".": "{0} e-mail is gecategoriseerd."}
                }
            },
            SingleSenderBlocked: {".": "{0} De afzender is geblokkeerd."}
        },
        Sweep: {
            Flyout: {
                Option: {
                    SweepAll: {".": "Alles {0} uit {1}"},
                    SweepAllCreateRule: {".": "Alles {0} uit {1}, toekomstige e-mail blokkeren"},
                    SweepAllExceptMostRecent: {".": "Altijd de meest recente bewaren, de rest {0}"},
                    SweepAllOlderThan10Days: {".": "E-mail die ouder is dan 10 dagen altijd {0}"}
                },
                MultiSender: {Title: {".": "Voor {0} van"}},
                Archive: {".": "Archiveren"},
                Category: {Newsletter: {".": "alle nieuwsbrieven"}, Email: {".": "alle e-mail"}},
                Button: {Sweep: {".": "Opruimen"}},
                Delete: {".": "Verwijderen"},
                SingleSender: {Title: {".": "Voor {0} van {1}"}}
            }, NoSenderError: {".": "Vegen werkt niet op e-mail zonder afzender, zoals concepten."}
        },
        Progress: {
            "delete": {".": "{0} e-mails zijn verwijderd."},
            Title: {".": "Het verzoek wordt zo snel mogelijk uitgevoerd"},
            Generic: {".": "{0} e-mails zijn verwerkt."},
            StopButton: {".": "Stoppen"},
            categorize: {".": "{0} e-mails zijn gecategoriseerd."},
            archive: {".": "{0} e-mails zijn gearchiveerd."},
            move: {".": "{0} e-mails zijn verplaatst."},
            GenericWithSender: {".": "E-mails van {0} worden verwerkt."}
        },
        MoveAllFromSender: {
            Flyout: {
                FolderPicker: {NoneSelected: {".": "<map selecteren>"}},
                FolderPickerLabel: {".": "Verplaatsen naar"},
                Button: {MoveAll: {".": "Alles verplaatsen"}},
                Option: {
                    MoveAllExceptMostRecent: {".": "Altijd de meest recente bewaren, de rest verplaatsen"},
                    MoveAll: {".": "Uit {0} verplaatsen"},
                    MoveAllCreateRule: {".": "Uit {0} verplaatsen en alle toekomstige e-mailberichten verplaatsen"},
                    MoveAllOlderThan10Days: {".": "E-mail ouder dan 10 dagen altijd verplaatsen"}
                }
            }
        },
        GenericError: {".": "We kunnen deze actie momenteel niet uitvoeren. Probeer het later opnieuw."}
    },
    EmojiPicker: {
        Category: {
            Travel: {".": "Reizen"},
            Nature: {".": "Natuur"},
            PeopleAndFaces: {".": "Personen en gezichten"},
            Recent: {".": "Recent"},
            Symbols: {".": "Symbolen"},
            FoodAndThings: {".": "Voedsel en voorwerpen"},
            Activities: {".": "Activiteiten"}
        }
    },
    Options: {
        Prefetch: {
            Disable: {".": "Berichten niet vooraf laden"},
            GroupLabel: {".": "Instellingen voor vooraf laden van berichten"},
            Enable: {".": "Berichten vooraf laden om sneller te kunnen lezen"},
            ParagraphOne: {".": "Berichten vooraf laden maakt het makkelijker om snel tussen berichten te schakelen door ze te downloaden voordat ze geopend worden."}
        },
        NextMessagePreferences: {
            Title: {".": "Standaardweergave na het verplaatsen of verwijderen van e-mail"},
            Paragraph: {".": "Het volgende weergeven na het verwijderen of verplaatsen van een bericht:"},
            Disable: {".": "Lijst met mijn berichten"},
            Enable: {".": "Het volgende bericht"}
        },
        MainOptions: {
            ManageAccount: {".": "Account beheren"},
            YourAccounts: {".": "Your accounts"},
            ReadingEmail: {".": "E-mail lezen"},
            JunkMail2: {".": "Junk email"},
            EmailSettings: {".": "E-mailinstellingen"},
            WritingEmail: {".": "E-mail schrijven"},
            JunkMail: {".": "Ongewenste e-mail voorkomen"},
            AccountDetails: {".": "Account details"}
        },
        Forwarding: {
            AddressInfo: {
                Remove: {".": "Verwijderen"},
                Pending: {".": "Wachten op bevestiging"},
                Resend: {".": "Bevestigingsmail opnieuw verzenden"},
                Verified: {".": "Bevestigd"}
            },
            Description: {".": "E-mail doorsturen"},
            BoxLabel: {
                Single: {".": "Naar welk adres moeten berichten worden doorgestuurd?"},
                Multiple: {".": "Typ de e-mailaccounts waarnaar de e-mail moet worden doorgestuurd."}
            },
            Instructions: {
                AddMore: {".": "E-mail kan worden doorgestuurd naar {0} andere e-mailaccounts."},
                FreeUser: {
                    Note: {".": "je moet je minstens eens in de {0} dagen aanmelden. Anders wordt het account als inactief beschouwd en wordt het verwijderd."},
                    NoteLabel: {".": "Belangrijk: "}
                },
                MultipleAddresses: {".": "E-mail kan worden doorgestuurd naar {0} verschillende e-mailaccounts."},
                Semicolon: {".": "Gebruik ';' om de adressen te scheiden:"},
                SingleAddress: {DomainUnrestricted: {".": "E-mail kan worden doorgestuurd naar een ander e-mailaccount."}}
            },
            Toggle: {
                OnWithAddresses: {".": "E-mail doorsturen naar de volgende e-mailaccounts"},
                Off: {".": "Niet doorsturen"},
                On: {".": "E-mail doorsturen naar een ander e-mailaccount"}
            },
            ExampleAddress: {".": "Voorbeeld: naam@example.com"},
            AddMore: {".": "E-mailadressen toevoegen"},
            Title: {".": "Doorsturen"},
            Button: {AddMore: {".": "Meer e-mailadressen toevoegen"}, Done: {".": "Gereed"}}
        },
        FAD: {
            Mode: {
                EnableFAD: {".": "Berichten controleren op vergeten bijlagen alvorens deze te verzenden"},
                DisableFAD: {".": "E-mail verzenden zonder deze te controleren op vergeten bijlagen"}
            }, Title: {".": "Herinneringen voor vergeten bijlage"}
        },
        Pinning: {
            Mode: {
                On: {".": "Met een vlag gemarkeerde berichten boven in Postvak IN weergeven"},
                Off: {".": "Met een vlag gemarkeerde berichten niet boven in Postvak IN weergeven"}
            },
            Title: {
                ShortDescription: {".": "Geef aan of je gemarkeerde berichten in je Postvak IN wilt bekijken (je kunt ze altijd in de categorie met gemarkeerde berichten bekijken)."},
                ".": "Markeren"
            }
        },
        SendReceive: {Title: {".": "Je e-mailaccounts"}},
        Privacy: {
            AutoComplete: {
                Title: {
                    ShortDescription: {".": "Bij het toevoegen van personen aan een bericht worden automatisch suggesties weergegeven. Kies welke suggesties worden weergegeven."},
                    ".": "Suggesties automatisch aanvullen"
                }
            },
            Title: {".": "Geavanceerde privacyinstellingen"},
            MessageRecovery: {
                Title: {".": "Verwijderde berichten"},
                Disable: {".": "Ik wil verwijderde berichten niet terugzetten. Zodra ze uit de map Verwijderd zijn gehaald, heb ik geen koppeling nodig om ze terug te halen."},
                Enable: {".": "Ik wil verwijderde berichten terugzetten."}
            }
        },
        InstantActions: {
            FlagInstantActionName: {".": "Markeren met vlag"},
            ShowHoverLabel: {".": "Weergeven bij aanwijzen"},
            DeleteInstantActionDescription: {".": "Een bericht verwijderen."},
            AddMenu: {".": "Acties toevoegen"},
            AddCategorize: {".": "Categoriseren"},
            Delete: {".": "Verwijderen uit lijst"},
            PreviewTitle: {".": "Voorbeeld"},
            ArchiveInstantActionName: {".": "Archiveren"},
            JunkInstantActionDescription: {".": "Een bericht als ongewenst markeren en verwijderen (tenzij de map met ongewenste e-mail — geopend is; in dat geval wordt het bericht gemarkeerd als niet-ongewenst en naar Postvak IN verplaatst)."},
            MoveInstantActionName: {".": "Verplaatsen naar"},
            FixedActionsHeader: {".": "Altijd weergeven"},
            SenderNameLabel: {".": "Naam van afzender"},
            CategorizeInstantActionDescription: {".": "Een bericht aan een geselecteerde categorie toevoegen."},
            AddJunk: {".": "Ongewenste e-mail"},
            DeleteInstantActionName: {".": "Verwijderen"},
            CategorizeInstantActionName: {".": "Categoriseren"},
            SweepInstantActionDescription: {".": "Alle berichten van de afzender verplaatsen of verwijderen."},
            AddFlag: {".": "Markeren met vlag"},
            JunkInstantActionName: {".": "Ongewenste e-mail"},
            SubjectLabel: {".": "Onderwerp van het bericht"},
            AlwaysAsk: {".": "Altijd vragen"},
            SweepInstantActionName: {".": "Opruimen"},
            MarkInstantActionDescription: {".": "Een ongelezen bericht markeren als gelezen en een gelezen bericht markeren als ongelezen."},
            OtherSweepActionAlreadyExists: {".": "Kan de waarde niet wijzigen. Er bestaat al een opruimactie van dit type."},
            MoveDown: {".": "Omlaag"},
            AddArchive: {".": "Archiveren"},
            MaxActionsMessage: {".": "Er is te weinig ruimte voor nog een directe actie. Als je een nieuwe directe actie wilt toevoegen, moet je er eerst een verwijderen."},
            MessageHoverLabel: {".": "Bericht met de muis erboven"},
            ShowFixedLabel: {".": "Altijd weergeven"},
            AddSweep: {".": "Opruimen"},
            ArchiveInstantActionDescription: {".": "Archiveert een bericht."},
            SweepDeleteRadioButton: {".": "Alles van een afzender verwijderen"},
            MarkInstantActionName: {".": "Markeren als gelezen/ongelezen"},
            AddMarkRead: {".": "Markeren als gelezen/ongelezen"},
            MoveInstantActionDescription: {".": "Een bericht naar een geselecteerde map verplaatsen."},
            SweepMoveRadioButton: {".": "Alles van een afzender verplaatsen"},
            AlsoMarkAsRead: {".": "Ook markeren als gelezen"},
            MoveUp: {".": "Omhoog"},
            ShowInlineActions: {".": "Directe acties weergeven"},
            AddMove: {".": "Verplaatsen naar"},
            AddDelete: {".": "Verwijderen"},
            AssignIcon: {".": "Pictogram kiezen voor deze directe actie"},
            HoverActionsHeader: {".": "Weergeven bij aanwijzen met de muis"},
            Title: {
                ShortDescription: {".": "Snelle acties worden naast de naam van de afzender en de onderwerpregel in je berichtenlijst weergegeven. Je hoeft een bericht daardoor niet te openen om actie te kunnen ondernemen."},
                ".": "Snelle acties"
            },
            FlagInstantActionDescription: {".": "Een bericht boven aan je Postvak IN laten staan."}
        },
        MyAccount: {Language: {Title: {".": "Taal"}}},
        KeyboardShortcutsSetting: {
            Mode: {
                Gmail: {".": "Gmail"},
                Off: {".": "Sneltoetsen uitschakelen"},
                Yahoo: {".": "Yahoo! Mail"}
            }, Title: {ShortDescription: {".": "Selecteer de gewenste instellingen:"}, ".": "Sneltoetsen"}
        },
        Button: {Save: {".": "Opslaan"}, Cancel: {".": "Annuleren"}, Ok: {".": "OK"}},
        MailAndJunk: {
            ReadingPaneSetting: {Title: {".": "Leesvenster"}},
            Filters: {
                ChooseFilter: {
                    Description: {".": "Selecteer het filterniveau voor binnenkomende berichten."},
                    Title: {".": "Een filter voor ongewenste e-mail kiezen"},
                    Standard: {".": "Standaard - de meeste ongewenste e-mail wordt in de map Ongewenst geplaatst."}
                },
                Confirm: {
                    Confirmation: {".": "Afzender vragen je te verwijderen van mailinglijsten en spammers automatisch blokkeren."},
                    Title: {".": "Ongewenste e-mail rapporteren aan de afzender"},
                    NoConfirmation: {".": "Afzenders niet vertellen dat je hun e-mail niet wilt ontvangen."}
                }
            },
            FontAndSignature: {Title: {".": "Font and signature"}},
            Signature: {
                Signature: {
                    Instruction: {".": "Een afsluiting toevoegen aan ieder verzonden bericht? Geef de tekst hieronder op."},
                    Title: {".": "Persoonlijke handtekening"}
                },
                Title: {".": "Opmaak, lettertype en handtekening"},
                Font: {
                    Title: {".": "Lettertype voor berichten"},
                    Instruction: {".": "Geef op in welk lettertype nieuwe berichten gestart moeten worden."},
                    RteText: {".": "Zo zien de geselecteerde lettertypen eruit."}
                }
            },
            ReplyTo: {
                Example: {".": "Voorbeeld: naam@example.com"},
                ReplyToAddress: {".": "Antwoordadres: "},
                CurrentEmail: {".": "<b>Je Van-adres<\/b>"},
                Instructions: {".": " Als personen naar een ander adres moeten antwoorden dan je Van-adres, typ je hieronder het gewenste adres en klik je op <b>Opslaan<\/b>."},
                OtherAddress: {".": "<b>Ander e-mailadres<\/b>"},
                Title: {".": "Antwoordadres"}
            },
            AutoSelectMessage: {
                NotAutoSelect: {".": "Een bericht pas weergeven nadat het geselecteerd is"},
                AutoSelect: {".": "Automatisch het eerste bericht weergeven"},
                GroupLabel: {".": "Bij de eerstse keer dat het Postvak IN of een andere map wordt geopend, kan het leesvenster worden ingesteld op:"}
            },
            ReadingPaneConfiguration: {
                GroupLabel: {".": "Waar moet het leesvenster worden weergegeven?"},
                Bottom: {".": "Onder"},
                Off: {".": "Uit"},
                Right: {".": "Rechts"}
            },
            AllowedSenders: {
                Instructions1: {".": "Berichten van veilige afzenders worden niet in de map Ongewenst geplaatst. Zowel aparte e-mailadressen als hele domeinen (het deel van een e-mailadres na het @-teken) kunnen als veilig worden gemarkeerd."},
                Instructions2: {".": "Typ het e-mailadres of domein dat moet worden toegestaan in het vak hieronder en klik op <b>Aan lijst toevoegen<\/b>. Klik op <b>Uit lijst verwijderen<\/b> om een adres of domein te verwijderen."},
                AllowedSenders: {".": "Veilige afzenders en domeinen:"}
            },
            Senders: {
                BlockedSenders: {
                    Example: {".": "Voorbeeld: naam@example.com"},
                    Instruction1: {".": "Berichten van geblokkeerde afzenders worden automatisch verwijderd. Zowel afzonderlijke e-mailadressen als hele domeinen (het deel van een e-mailadres na het @-teken) kunnen worden geblokkeerd."},
                    Instruction2: {".": "Typ het te blokkeren e-mailadres of domein in het vak hieronder en klik op <b>Aan lijst toevoegen<\/b>. Selecteer een geblokkeerd adres of domein in de onderstaande lijst en klik op <b>Uit lijst verwijderen<\/b> om dit uit die lijst te verwijderen."},
                    BlockedEmail: {".": "Geblokkeerd e-mailadres of domein: "},
                    Title: {".": "Geblokkeerde afzenders"},
                    BlockedSenders: {".": "Geblokkeerde afzenders: "}
                },
                Title: {".": "Veilige en geblokkeerde afzenders"},
                GoodReputationSenders: {
                    Title: {".": "Inhoud van onbekende afzenders blokkeren"},
                    ShowContent: {".": "Bijlagen, afbeeldingen en links weergeven voor afzenders met een goede reputatie"},
                    BlockContent: {".": "Bijlagen, afbeeldingen en links blokkeren voor iedereen die niet in de lijst met veilige afzenders staat"}
                },
                AllowedLists: {
                    Description: {".": "Beheren welke mailinglijsten naar het account worden verstuurd. Berichten naar veilige mailinglijsten worden niet in de map Ongewenst geplaatst."},
                    Title: {".": "Veilige mailinglijsten"}
                },
                AllowedSenders: {Title: {".": "Veilige afzenders"}}
            },
            LiveViews: {
                Hotmail: {
                    EnableSelect: {".": "Altijd voorbeelden weergeven (bijvoorbeeld van foto's en video's)"},
                    GroupLabel: {".": "Voorbeelden laten zien?"},
                    DisableSelect: {".": "Voorbeelden verbergen"}
                }
            },
            SaveSentMessages: {
                Description: {".": "Alle verzonden berichten kunnen automatisch worden opgeslagen"},
                Title: {".": "Verzonden berichten opslaan"},
                Yes: {".": "Alle verzonden berichten opslaan in de map Verzonden berichten"},
                Prompt: {".": "Kiezen of verzonden berichten automatisch worden opgeslagen."}
            },
            AllowedLists: {
                AllowedEmail: {".": "Ontvangstadres voor de mailinglijst: "},
                AllowedLists: {".": "Veilige mailinglijsten: "},
                Instructions2: {".": "Om berichten toe te staan die naar een ander adres dan {0} zijn verstuurd, moet het ontvangstadres van de mailinglijst in het vak hieronder worden getypt. Klik vervolgens op <b>Aan lijst toevoegen<\/b>. Selecteer om een lijst te verwijderen de gewenste mailinglijst in de onderstaande lijst en klik op <b>Uit lijst verwijderen<\/b>."},
                Example: {".": "Voorbeeld: mailinglijst@example.com"}
            }
        },
        AutoComplete: {
            Mode: {
                UsePicw: {".": "Suggesties voor iedereen met wie ik eerder heb gecommuniceerd (contactpersonen en anderen)"},
                HidePicw: {".": "Alleen suggesties voor contactpersonen"}
            }
        },
        Conversations: {
            Disable: {".": "Berichten apart weergeven"},
            Title: {".": "Groeperen op gesprek en berichten vooraf laden"},
            GroupLabel: {".": "Discussie-instellingen"},
            Enable: {".": "Berichten groeperen op discussie"},
            Paragraph1: {".": "De meeste e-mailservices geven ieder bericht weer als een aparte regel in het Postvak IN."}
        },
        Title: {".": "Opties"},
        ReplyAllPreferences: {
            Title: {".": "'Allen beantwoorden' als standaard instellen voor beantwoorden"},
            Paragraph: {".": "Wil je 'Allen beantwoorden' als standaard instellen voor het beantwoorden van berichten?"},
            Disable: {".": "Nee"},
            Enable: {".": "Ja"},
            Note: {".": "Opmerking: je kunt in het vervolgkeuzemenu nog steeds 'Beantwoorden' en 'Doorsturen' selecteren."}
        },
        SocialInboxPreferences: {
            ShowingPrivateContent: {Title: {".": "Aanvullende inhoud weergeven"}},
            Disabled: {".": "Geen openbare inhoud van netwerken van derden weergeven"},
            Enabled: {".": "Openbare inhoud van netwerken van derden weergeven"},
            Title: {".": "Inhoud van derden"},
            ShowingPublicContent: {Title: {".": "Openbare inhoud weergeven"}}
        },
        ImportAccount: {
            ImportAccountTop: {
                OauthImport: {
                    Description: {".": "Gmail en contactpersonen"},
                    Title: {".": "Google"},
                    Connected: {Title: {".": "Google ({0})"}}
                },
                YahooImport: {Description: {".": "E-mail"}, Title: {".": "Yahoo"}},
                ImapImport: {
                    Description: {".": "E-mail"},
                    Title: {".": "Andere e-mailprovider"},
                    NoOAuth: {Title: {".": "E-mailprovider"}}
                },
                Description: {".": "Kies waaruit je je account wilt importeren:"},
                ClientToServer: {Description: {".": "E-mail en contactpersonen"}, Title: {".": "E-mailapps op je pc"}},
                Title: {".": "E-mailaccounts importeren"}
            }
        }
    },
    Rules: {
        Error: {
            CantEditLegacyRule: {".": "Regels die zijn gemaakt in het oude systeem voor regels kunnen niet worden bewerkt. Als je deze regel wilt bewerken, verwijder je de regel en maak je een nieuwe."},
            DeferredConditionsRequireAdditionalCondition: {".": "Kan deze regel niet opslaan omdat deze alleen 'ouder dan x dagen' of 'niet de nieuwste y' bevat."},
            CantReorder: {".": "Kan regelvolgorde niet wijzigen omdat sommige regels zijn bijgewerkt."},
            ConditionDuplicateCategoryIds: {".": "Kan deze regel niet opslaan omdat dezelfde categorie is ingesteld voor 'bevat' en 'bevat niet'."},
            OldRulesExist: {".": "Kan sommige oude regels niet weergeven op deze pagina."},
            EmptyInputs: {".": "Kan deze regel niet opslaan omdat voorwaarden en acties niet leeg mogen zijn."},
            Generic: {".": "Er is iets misgegaan. Probeer de regel opnieuw te maken."},
            ActionDuplicateCategoryIds: {".": "Kan deze regel niet opslaan omdat dezelfde categorie moet worden toegevoegd en verwijderd."},
            ForwardActionCannotBeDeferred: {".": "Kan deze regel niet opslaan omdat er geen e-mail kan worden doorgestuurd die valt onder 'ouder dan x dagen' en 'niet de nieuwste y'."},
            ParentConditionWithExistingChildren: {".": "Voorwaarde 'ouder dan X dagen' kan niet worden verwijderd; deze is vereist voor regels die de voorwaarde 'map', 'gemarkeerd', 'gelezen' of 'beantwoord' bevatten."},
            MaxRules: {".": "Kan geen regel toevoegen omdat het maximaal toegestane aantal is bereikt."}
        },
        CreateRule: {Title: {".": "Regel maken"}},
        Predicate: {
            DoesNotContain: {".": "bevat niet"},
            StartsWith: {".": "begint met"},
            Contains: {".": "bevat"},
            ContainsWord: {".": "bevat woord"},
            IsEqualTo: {".": "is gelijk aan"},
            EndsWith: {".": "eindigt op"}
        },
        ManageRules: {
            BreadCrumb: {".": "Regels beheren"},
            MoveRuleUp: {
                Label: {
                    ".": "Regel omhoog verplaatsen",
                    Disabled: {".": "Regel omhoog verplaatsen uitgeschakeld"}
                }
            },
            Title: {".": "Regels voor het sorteren van nieuwe berichten"},
            MoveRuleDown: {
                Label: {
                    ".": "Regel omlaag verplaatsen",
                    Disabled: {".": "Regel omlaag verplaatsen uitgeschakeld"}
                }
            },
            Button: {New: {".": "Nieuw"}},
            DeleteRule: {Label: {".": "Regel verwijderen"}},
            RuleCount: {
                Single: {".": "Er is 1 actieve regel "},
                Multiple: {".": "Er zijn {0} actieve regels"},
                None: {".": "Er zijn geen regels"}
            },
            Message1: {".": "Je kunt regels instellen om binnenkomende e-mail automatisch te verwerken. Je kunt onder andere berichten markeren of verplaatsen naar mappen."}
        },
        Condition: {
            RepliedTo: {".": "e-mail is beantwoord"},
            RecipientContains: {".": "ontvanger bevat: {0}"},
            NotFlagged: {".": "e-mail is niet gemarkeerd"},
            SenderContains: {".": "afzender bevat: {0}"},
            HasAttachment: {".": "heeft bijlage"},
            HeaderSearch: {
                Subject: {".": "onderwerp"},
                SenderEmail: {".": "e-mailadres afzender"},
                ToCcLines: {".": "Regel Aan of CC"},
                SenderName: {".": "naam afzender"},
                FormatString: {".": "{0} {1} {2}"}
            },
            Unread: {".": "e-mail is ongelezen"},
            Separator: {".": " en "},
            SubjectContains: {".": "onderwerp bevat: {0}"},
            NotRepliedTo: {".": "e-mail is niet beantwoord"},
            NotTheLatest: {".": "is niet de nieuwste {0}"},
            OlderThan: {".": "is ouder dan {0} dagen"},
            DoesNotHaveCategory: {".": "categorie is niet: {0}"},
            KeywordsContain: {".": "trefwoord bevat: {0}"},
            Read: {".": "e-mail is gelezen"},
            HasCategory: {".": "categorie is: {0}"},
            SenderAddress: {".": "adres van afzender is {0}"},
            Folder: {".": "is in map {0}"},
            Flagged: {".": "e-mail is gemarkeerd"},
            DoesNotHaveAttachment: {".": "heeft geen bijlage"}
        },
        EditRule: {
            Action: {
                AddAction: {Label: {".": "Een actie toevoegen"}, ".": "Handeling"},
                MarkAsJunk: {".": "Als ongewenst markeren"},
                RemoveCategory: {".": "Categorie verwijderen"},
                SetFlagState: {
                    ".": "Status van markering instellen",
                    Unflagged: {".": "Niet gemarkeerd"},
                    Flagged: {".": "Gemarkeerd"}
                },
                MoveTo: {".": "Verplaatsen naar"},
                ForwardTo: {".": "Doorsturen naar"},
                Archive: {".": "Archiveren"},
                MarkAs: {".": "Markeren als", Unread: {".": "Ongelezen"}, Read: {".": "Gelezen"}},
                CategorizeAs: {".": "Categoriseren als"},
                Delete: {".": "Verwijderen"},
                Title: {".": "Volgende handelingen uitvoeren"},
                DeleteAction: {Label: {".": "Deze actie verwijderen"}}
            },
            Placeholder: {
                BoolExpr: {".": "term1 AND term2"},
                EmailBoolExpr: {".": '"gebruiker@example.com" OR naam'},
                Email: {".": '"gebruiker@example.com"'}
            },
            Title: {".": "Regel bewerken"},
            Category: {
                DefaultNoSelection: {".": "<Categorieën selecteren>"},
                MultipleCategories: {".": "{0} categorieën"}
            },
            Button: {CreateRule: {".": "Regel maken"}, SaveRule: {".": "Regel opslaan"}, Cancel: {".": "Annuleren"}},
            Condition: {
                DeleteCondition: {Label: {".": "Deze voorwaarde verwijderen"}},
                ReadStateIs: {".": "Leesstatus is", Unread: {".": "Ongelezen"}, Read: {".": "Gelezen"}},
                OlderThanTemplate: {".": "{0} dagen"},
                RecipientContains: {".": "Ontvanger bevat"},
                NotRepliedTo: {".": "Niet beantwoord aan"},
                SenderAddressContains: {".": "Adres van afzender bevat"},
                NotTheLatest: {".": "Niet de nieuwste"},
                Attachment: {
                    HasAttachment: {".": "Met bijlage"},
                    DoesNotHaveAttachment: {".": "Heeft geen bijlage"},
                    ".": "Bijlage"
                },
                FlaggedState: {
                    ".": "Markeringsstatus",
                    Unflagged: {".": "Niet gemarkeerd"},
                    Flagged: {".": "Gemarkeerd"}
                },
                SubjectContains: {".": "Onderwerp bevat"},
                CategoryIs: {".": "Categorie is"},
                FolderIs: {".": "Map is"},
                OlderThan: {".": "Ouder dan"},
                AddCondition: {Label: {".": "Een voorwaarde toevoegen"}, ".": "Voorwaarde"},
                Title: {".": "Wanneer een e-mail voldoet aan"},
                CategoryIsNot: {".": "Categorie is niet"},
                SenderContains: {".": "Afzender bevat"},
                KeywordsContains: {".": "Trefwoorden bevatten"}
            }
        },
        FullDescription: {".": "Als {0} dan {1}"},
        Action: {
            Move: {".": "verplaatsen naar {0}"},
            MarkAsJunk: {".": "markeren als ongewenst"},
            RemoveCategory: {".": "categorie {0} verwijderen"},
            Separator: {".": " en "},
            MarkUnread: {".": "markeren als ongelezen"},
            Forward: {".": "doorsturen naar {0}"},
            Flag: {".": "bericht markeren"},
            Categorize: {".": "categoriseren als {0}"},
            Unflag: {".": "markering van bericht opheffen"},
            MarkRead: {".": "markeren als gelezen"}
        },
        CantEditDialog: {Title: {".": "Kan deze regel niet bewerken"}},
        List: {Postfix: {".": ""}, Template: {".": "{0}{1}{2}"}, Prefix: {".": ""}},
        CategorySeparator: {".": ", "}
    },
    FilterSelector: {CurrentView: {".": "Weergeven:"}},
    EditMessage: {
        To: {".": "Aan"},
        GoToFullCompose: {".": "Op volledig scherm opstellen"},
        Subject: {".": "Voeg een onderwerp toe"},
        TooManyRecipients: {".": "Er zijn te veel ontvangers. Verwijder een aantal ontvangers en probeer het opnieuw."},
        SendDuringUploadConfirmation: {".": "Je bijlagen zijn nog niet volledig geüpload. Bestanden die worden geüpload, worden niet verzonden."},
        CannotForwardMessage: {".": "Dit bericht niet kan niet worden doorgestuurd."},
        UploadInProgress: {".": "Uploaden..."},
        Attachment: {".": "{0} ({1}){2}"},
        CannotReplyToAddress: {".": "Het is niet mogelijk te antwoorden naar dit adres."},
        SpellCheck: {IgnoreAll: {".": "Alles negeren"}, AddToDictionary: {".": "Toevoegen aan woordenlijst"}},
        UploadInProgressCancel: {".": "Annuleren"},
        AcceptAction: {".": "Accepteren"},
        AddCc: {".": "Cc"},
        TentativeAction: {".": "Voorlopig accepteren"},
        AttachmentForgotten: {
            Title: {".": "Zonder bijlagen verzenden?"},
            ActionButton: {".": "Nu verzenden"},
            Message: {".": "Voor alle zekerheid: het lijkt erop dat je bijlagen vermeldt, maar die zijn er niet."},
            Cancel: {".": "Ga terug"},
            Checkbox: {".": "Dit bericht niet opnieuw weergeven."}
        },
        AddBcc: {".": "Bcc"},
        ContinueComposeDraft: {".": "Doorgaan met schrijven"},
        DeclineAction: {".": "Weigeren"},
        Sending: {".": "Verzenden..."},
        Attach: {".": "Bestanden als bijlagen"},
        NotSavedAction: {".": "Doorgaan"},
        InlineReply: {Fre: {".": "Inlineantwoord is de snelle methode waarmee je je gesprekken op gang houdt. Mis je de klassieke antwoordmodus? Die kun je hier vinden."}},
        EditSubject: {".": "Klik om het onderwerp te bewerken"},
        ShowQuotedText: {".": "Berichtgeschiedenis weergeven"},
        RecipientsFiltered: {".": "Je kunt alleen e-mails versturen naar contactpersonen in je adresboek. Geadresseerden zijn verwijderd uit het bericht."},
        DraftSavedFormat: {".": "Concept opgeslagen om {0}"},
        LowImportance: {".": "Lage prioriteit"},
        Cc: {".": "CC"},
        Bcc: {".": "BCC"},
        Cancel: {
            DirtyWarning: {".": "Je staat op het punt je bericht te verwijderen."},
            DirtyAction: {".": "Concept opslaan"},
            DirtyAlternate: {".": "Verwijderen"}
        },
        ContactPicker: {
            Bcc: {AriaLabel: {".": "Veld BCC: voer een e-mailadres of een naam uit je lijst met contactpersonen in"}},
            To: {AriaLabel: {".": "Veld To: voer een e-mailadres of een naam uit je lijst met contactpersonen in"}},
            Cc: {AriaLabel: {".": "Veld CC: voer een e-mailadres of een naam uit je lijst met contactpersonen in"}},
            Tooltip: {".": "Contactpersonen selecteren"}
        },
        NotSavedWarning: {".": "Dit bericht wordt verwijderd zonder het te verzenden."},
        PasswordSafetyQuestion: {".": "Controleer of deze onbekende afzender betrouwbaar is. Geef nooit een wachtwoord, creditcardnummer of andere persoonlijke gegevens door via e-mail."},
        SubjectMissing: {
            Action: {".": "Toch verzenden"},
            Warning: {".": "Je staat op het punt deze e-mail te versturen zonder onderwerpregel."}
        },
        HighImportance: {".": "Hoge prioriteit"},
        ReplyForwardWithPicturesQuestion: {
            ".": "Als het bericht wordt doorgestuurd of beantwoord, worden eventuele afbeeldingen en koppelingen in de e-mail weergegeven. De afzender kan dan mogelijk nog meer ongewenste e-mail sturen.",
            ContinueButton: {".": "Doorgaan"}
        },
        NotSavedCancel: {".": "Annuleren"},
        NormalImportance: {".": "Normale prioriteit"},
        SendDuringUploadConfirmationAction: {".": "Toch verzenden"},
        RemoveAttachment: {".": "Verwijderen"},
        SavedAtFormat: {".": "Opgeslagen op {0}"},
        DraftSaveInProgress: {".": "Opslaan..."}
    },
    ScheduleCleanup: {Generic: {".": "Opruiming plannen"}},
    DragNDrop: {
        DragFolder: {".": "Verplaatsen onder {0}"},
        DragMessage: {All: {".": "Geselecteerde items"}, Single: {".": "{0} item"}, Multiple: {".": "{0} items"}},
        DragFolderToBase: {".": "Verplaatsen naar basis"}
    },
    SkyApi: {
        Permissions: {
            ItemContainsCoOwnedItem: {".": "\tKan deze items van OneDrive niet delen omdat ze een gedeelde groepsmap bevatten."},
            OnlyUserOwnedItemsAllowedOnBundlesError: {".": "Items waarvan je mede-eigenaar bent, kunnen niet worden gedeeld als onderdeel van een selectie. Maak een andere selectie."}
        }
    },
    ReadingPane: {
        NotAutoSelectMessage: {
            Hint: {
                Paragraph3: {".": "het eerste bericht hier automatisch weergeven."},
                Title: {".": "Klik op een bericht om het hier weer te geven"},
                Paragraph2: {".": "Dit lege bericht helpt je privacy te beschermen, Je kunt ook"},
                Paragraph1: {".": ""}
            }
        }
    },
    Multibrowser: {
        FolderMoved: {".": "De map die je zoekt, is eerder verplaatst of verwijderd."},
        MessageMoved: {".": "Het bericht dat je zoekt, is eerder verplaatst of verwijderd."},
        AnchorMoved: {".": "De map of categorie waarin je bladert, is bijgewerkt."}
    },
    InboxFlow: {
        ImportAccountThrottling: {
            Title: {".": "Importeren is momenteel niet beschikbaar."},
            Button: {OK: {".": "OK"}},
            Content: {".": "We zitten momenteel aan onze limiet. Probeer het later opnieuw."}
        }
    },
    OwaOptin: {
        Line2: {".": "Vernieuw de pagina, zodat u de nieuwe functies kunt bekijken."},
        Line1: {".": "De nieuwe gebruikerservaring is nu klaar."}
    },
    PaginationControl: {
        BlindPagingFormat: {".": "Pagina {0}"},
        FullPagingFormat: {".": "Pagina {0} van {1}"},
        GoToLinkText: {".": "Ga naar"}
    },
    List: {Item: {Add: {".": "Nieuw item toevoegen"}}},
    MessageList: {
        Categorize: {".": "Categorieën"},
        OneSelected: {".": "1 bericht geselecteerd"},
        FirstPage: {".": "Eerste pagina"},
        SortInboxBySize: {".": "Grootte"},
        SortInboxByFrom: {".": "Van"},
        Sort: {BigInboxError: {Para1: {".": "Deze map bevat te veel berichten en kan daardoor niet worden gesorteerd. Gebruik de zoekfunctie als je wilt zoeken naar berichten van een bepaalde afzender of met specifieke tekst."}}},
        SortInboxByTo: {".": "Aan"},
        MultipleSelected: {".": "{0} items geselecteerd"},
        SortInboxByConversation: {".": "Discussie"},
        Deleted: {
            BottomText: {
                FailureConfirmation: {".": "Bedankt voor de feedback. Jammer dat het niet is gelukt om terug te halen wat je zocht."},
                ExecuteMessageRecoveryFormat: {".": "Een bericht kwijt? Het is mogelijk om {0}, dan halen we zo veel mogelijk berichten terug. {1}"},
                SuccessLinkText: {".": "Het is gelukt"},
                SuccessConfirmation: {".": "Fijn dat het is gelukt. Bedankt dat je dit aan ons hebt doorgegeven."},
                FailureLinkText: {".": "Het is niet gelukt"},
                SomeMessagesRecoveredFormat: {".": "We hebben zo veel mogelijk berichten hersteld. De map Verwijderd wordt regelmatig leeggemaakt, dus verplaats berichten die je wilt bewaren. Is het ons gelukt? {0} | {1}"},
                LearnMoreLink: {".": "Meer informatie"},
                NoMessagesRecoveredFormat: {".": "Sorry, we hebben ons best gedaan, maar het is helaas niet gelukt om verwijderde berichten te herstellen. {0}"},
                RecoverLink: {".": "verwijderde berichten te herstellen"}
            }
        },
        BulkSelectAllPrompt: {
            ".": "{0} in {1}",
            Inbox: {".": "{0}"},
            LinkText: {".": "Alles selecteren"},
            Folder: {".": "de map {0}"}
        },
        PreviousPage: {".": "Vorige pagina"},
        SortByLabel: {".": "Rangschikken op {0}"},
        AllSelected: {".": "Alles in {0} is geselecteerd"},
        SortBy: {".": "Schikken op"},
        SortInboxBySubject: {".": "Onderwerp"},
        Selection: {GenericView: {".": "deze weergave"}, GenericFolder: {".": "deze map"}},
        PermanentlyDeleteConfirmation: {
            Scope: {
                View: {".": "Je staat op het punt om alle berichten in deze weergave te verwijderen."},
                Folder: {".": "Je staat op het punt om alle berichten in deze map te verwijderen."}
            }
        },
        NoMessages: {
            Folder: {
                Default: {Text: {".": null}},
                Sent: {
                    LinkText: {".": "Instellingen controleren"},
                    Text: {".": "Verzonden berichten worden automatisch opgeslagen, tenzij anders ingesteld. {0}"}
                },
                Trash: {Text: {".": "Zowel met de knop Verwijderen als met de knop Ongewenst kunnen overbodige berichten worden verwijderd. Bij gebruik van de knop Ongewenst wordt de afzender van het bericht echter ook geblokkeerd (en kunnen wij onze filters tegen ongewenste e-mail verbeteren)."}},
                Inbox: {LinkText: {".": null}, Text: {".": "Dit Postvak IN is erg opgeruimd!"}},
                Junk: {Text: {".": "Geen ongewenste e-mail hier. Microsoft SmartScreen zorgt ervoor dat dit ook voor het Postvak IN geldt."}},
                Custom: {
                    LinkText: {".": "regels te maken"},
                    Text: {".": "Het is mogelijk om berichten naar mappen te slepen, en om {0} om berichten automatisch hiernaartoe te verplaatsen."}
                },
                Drafts: {Text: {".": "Klik op 'Concept opslaan' als een bericht nog niet verzonden moet worden. Het kan dan later worden afgemaakt."}},
                InboxWithPopUpsell: {LinkText: {".": "voeg je je andere account toe als account voor alleen verzenden"}}
            },
            QuickView: {
                ShippingNotification: {Text: {".": "Met deze categorie kun je zoeken naar alle berichten die pakketnummers bevatten. (Zo te zien zijn deze nu niet beschikbaar.)"}},
                FlaggedMessages: {Text: {".": "Er zijn nog geen berichten gemarkeerd, maar zodra dat gebeurt, worden ze hier weergegeven."}},
                Photos: {Text: {".": "Eventuele ontvangen foto's of links naar foto's worden hier weergegeven."}},
                Default: {Text: {".": "Je hebt nog geen berichten aan {0} toegevoegd, maar zodra je dit hebt gedaan, worden ze hier weergegeven. Je kunt de berichten dan volgen."}},
                File: {Text: {".": "Met deze categorie kun je zoeken naar ontvangen documenten, spreadsheets of presentaties. (Zo te zien zijn deze nu niet beschikbaar.)"}}
            },
            Filter: {
                SocialUpdates: {Text: {".": "Met dit filter kun je je richten op berichten die afkomstig zijn van sociale netwerken, zoals Facebook en MySpace."}},
                MailingList: {Text: {".": "Met dit filter kunnen groepsdiscussies worden bekeken. (Zo te zien zijn die er op dit moment niet.)"}},
                UnreadMessages: {Text: {".": "Met dit filter worden alle ongelezen berichten in deze map weergegeven. (Zo te zien zijn alle berichten gelezen.)"}},
                Other: {Text: {".": "Met dit filter worden alle berichten in deze map weergegeven die niet afkomstig zijn van contactpersonen, groepen of sociale netwerken. "}},
                Default: {Text: {".": "Met dit filter geef je berichten weer die zijn toegevoegd aan {0}. (Blijkbaar heb je dit filter nog niet.)"}},
                Newsletters: {Text: {".": "Dit filter is bestemd voor het weergeven van nieuwsbrieven. (Het lijkt erop dat je er nu geen hebt.)"}},
                FromContacts: {
                    LinkText: {".": "contactpersonen importeren"},
                    Text: {".": "Met dit filter is het mogelijk om alle berichten van contactpersonen te bekijken. Nu {0}?"}
                }
            }
        },
        BackTo: {".": "Terug naar {0}", Messages: {".": "Berichten"}},
        LastPage: {".": "Laatste pagina"},
        Checkbox: {DefaultSender: {".": "Onbekende afzender"}},
        ContextMenu: {
            ForThisConversation: {".": "Voor deze discussie"},
            ForThisSender: {".": "Voor deze afzender"},
            ForThisMessage: {".": "Voor dit bericht"},
            ForTheseSenders: {".": "Voor deze afzenders"},
            ForTheseMessages: {".": "Voor deze berichten"}
        },
        Icons: {
            Voicemail: {AltText: {".": "Voicemail"}},
            LowPri: {AltText: {".": "Prioriteit Laag"}},
            Calllog: {AltText: {".": "Gesprekgeschiedenis"}},
            VoicemailOnPhone: {AltText: {".": "Aan de telefoon"}},
            Attachment: {AltText: {".": "Bijlage"}},
            Replied: {AltText: {".": "Beantwoord"}},
            Forwarded: {AltText: {".": "Doorgestuurd"}},
            TrustedSender: {AltText: {".": "Betrouwbare afzender"}},
            HighPri: {AltText: {".": "Prioriteit Hoog"}}
        },
        NoMatches: {
            AdvancedSearch: {".": "Geavanceerd zoeken"},
            Format: {".": "Er zijn geen zoekresultaten. Probeer {0}."}
        },
        SortInboxByDate: {".": "Datum"},
        ClearSelection: {".": "Alle selectievakjes uitschakelen"},
        NextPage: {".": "Volgende pagina"},
        Junk: {
            BottomText: {
                Link: {".": "lijst met geblokkeerde afzenders"},
                ".": "Op zoek naar een e-mail? Controleer de {0} als de e-mail hier niet bij staat."
            }
        }
    },
    MeetingRequest: {
        Tentative: {".": "Voorlopig"},
        Location: {".": "Locatie:"},
        MeetingRequest: {AltText: {".": "Dit is een verzoek voor vergadering"}},
        Decline: {".": "Weigeren"},
        Action: {".": "Actie:"},
        ResponseTentative: {AltText: {".": "Voorlopig geaccepteerd"}},
        MeetingCancelled: {AltText: {".": "Vergadering geannuleerd"}},
        ResponseAccept: {AltText: {".": "Geaccepteerd"}},
        When: {".": "Wanneer:"},
        RemoveFromCalendar: {".": "Uit agenda verwijderen"},
        Accept: {".": "Accepteren"},
        ResponseDecline: {AltText: {".": "Geweigerd"}},
        Calendar: {".": "Agenda"}
    },
    ProcessWindow: {
        CompletedWithErrors: {".": "Voltooid met fouten"},
        State: {
            Error: {".": "Fout"},
            Pending: {".": "In behandeling"},
            Completed: {".": "Voltooid"},
            Active: {".": "Actief"},
            Canceling: {".": null}
        },
        ActionsInProgressSingular: {".": "1 actie wordt uitgevoerd..."},
        Error: {".": "Er is een fout opgetreden"},
        ActionsInProgressMultiple: {".": "{0} acties worden uitgevoerd..."}
    },
    DhtmlAlerts: {
        CloseBox: {AltText: {".": "Sluiten"}},
        LowImportance: {AltText: {".": "Informatie"}},
        MediumImportance: {AltText: {".": "Waarschuwing"}},
        HighImportance: {AltText: {".": "Fout"}}
    },
    Inbox: {
        Safety: {
            Notifications: {
                NotJunk: {
                    Single: {ButtonText: {".": "Ga naar bericht"}},
                    Multiple: {ButtonText: {".": "Naar Postvak IN"}},
                    Message: {
                        Single: {".": "We hebben dit bericht gemarkeerd als veilig en teruggeplaatst in het Postvak In."},
                        Multiple: {".": "We hebben de berichten gemarkeerd als veilig en teruggeplaatst in het Postvak In."}
                    }
                },
                LearnMoreButton: {Text: {".": "Meer informatie"}},
                Compromised: {Message: {".": "Fijn dat je dit rapporteert. Dat is echte vriendschap."}},
                Phishing: {Message: {".": "Bedankt. Door phishing te melden blijven jouw persoonlijke gegevens en de gegevens van anderen veilig."}}
            }
        }
    },
    SaveToOneNote: {
        SectionNameExists: {".": "Er bestaat al een sectie met deze naam in dit notitieblok."},
        Attachments: {".": "Bijlagen"},
        Saving: {".": "Opslaan..."},
        SelectLocation: {".": "Locatie selecteren in OneNote"},
        Error: {".": "De aanvraag kan momenteel niet worden voltooid. Probeer het later opnieuw."},
        NewSectionButton: {".": "Nieuwe sectie"},
        CalloutText: {".": "U kunt kopieën van uw belangrijkste e-mailberichten overzichtelijk in OneNote bewaren en er offline mee werken."},
        PickSectionOrPage: {".": "Een sectie kiezen"},
        Loading: {".": "Laden..."},
        CalloutTitle: {".": "Opslaan in OneNote"},
        NewNotebook: {".": "Nieuw notitieblok"},
        SeeInOneNoteButton: {".": "Weergeven in OneNote"},
        ClickToSeeOriginalMessage: {".": "Klik hier om het oorspronkelijke bericht weer te geven in {$Cobrand.Mail.ProductName}"},
        EmailSaved: {".": "Uw e-mailadres is opgeslagen in OneNote"},
        EmailNotePlaceHolderText: {".": "Een notitie toevoegen"},
        DoneSaving: {".": "Gereed"},
        DefaultEmailSubject: {".": "Opgeslagen vanuit {$Cobrand.Mail.ProductName}"},
        InvalidCharactersInSectionName: {".": 'De naam van een sectie mag geen van de volgende tekens bevatten: ~  #  %  &  *  {  }  |  \\  /  :  "  <  >  ?  ^'},
        CalloutDismissButtonText: {".": "Ik snap het"},
        AllNotebooks: {".": "Alle notitieblokken"},
        CalloutButtonText: {".": "Probeer het"},
        SaveButton: {".": "Opslaan"}
    },
    Error: {
        Server: {
            UnsatisfiedQuery: {
                RefreshButton: {".": "Vernieuwen"},
                ErrorMessage: {".": "Er is iets fout gegaan. Vernieuw je browser of sluit de browser en meld je opnieuw aan."}
            }
        },
        Folder: {
            FolderNameInUse: {".": "Deze map bestaat al. Kies een andere naam."},
            CannotMoveParentFolderUnderChildFolder: {".": "Je kunt een map niet naar een van de eigen submappen verplaatsen."},
            FolderNameIsSystemFolderName: {".": "Deze mapnaam is niet beschikbaar. Probeer een andere naam."},
            FolderNameTooLong: {".": "De mapnaam is te lang."},
            CannotMoveFolderNameCollision: {".": "De map kan niet worden verplaatst. Er bestaat al een map met dezelfde naam."},
            FolderNameEmpty: {".": "Een mapnaam is verplicht. Typ een naam in voor de nieuwe map."},
            FolderNameContainsInvalidChars: {".": "De mapnaam bevat tekens die niet zijn toegestaan. Kies een naam waarin geen van de volgende tekens voorkomen: & < > \" ' : ; \\ / ( ) +"}
        },
        TaskNotCompleted: {".": "Deze taak kon niet worden uitgevoerd. Probeer het opnieuw."},
        EditMessage: {
            AttachmentHasVirus: {".": "Het bestand bevat een virus en kan niet worden bijgevoegd."},
            CurrentUploadLimitFull: {".": "Totale grootte: {0} van {1}"},
            BlockedAttachmentFileExtension: {".": "Dit bestandstype kan niet worden bijgevoegd"},
            UploadFileFailure: {".": "Met dit bestand overschrijd je de limiet voor bijlagen."},
            MessageAddressesMissing: {".": "Typ een adres in het vak Aan."},
            UpgradeTierLinkText: {".": "Bevestigen"},
            AttachmentInvalidFilename: {".": "De bestandsnaam bevat ongeldige tekens en kan niet worden toegevoegd."},
            CloseHipError: {".": "Sluiten"}
        },
        Global: {
            PassportSessionTimedOut: {".": "Voer de accountgegevens opnieuw in. Om persoonlijke gegevens te beschermen, wordt het account soms afgemeld (bijvoorbeeld na 24 uur of als een ander account wordt aangemeld)."},
            LoadingIndicatorMessage: {".": "Laden..."}
        },
        Button: {LearnMore: {".": "Meer informatie"}, Login: {".": "Opnieuw aanmelden"}, Cancel: {".": "Annuleren"}},
        Options: {Error: {CannotBlockSelf: {".": "Het is niet mogelijk het eigen e-mailadres te blokkeren."}}}
    },
    SearchFilters: {
        FilterByParam: {".": "Filteren op: {0}"},
        FilterBy: {".": "Filteren op"},
        MultipleFilters: {".": "{0} filters"},
        ClearFilters: {".": "Filters wissen"},
        Apply: {".": "Toepassen"}
    },
    Mandrake: {Privacy: {Warning: {".": "Door dit traceerbestand voor fouten opsporen te leveren, geef je Microsoft toestemming de persoonlijke gegevens in het traceerbestand die je hebt geüpload naar, opgeslagen op of verzonden via de interne services en clients, te verzamelen en te gebruiken. Dit omvat onder andere automatische en handmatige controle van persoonlijke gegevens en materiaal, inclusief alle informatie die op het scherm is weergegeven terwijl tracering voor fouten opsporen actief was, zoals de e-mails die je hebt bekeken, de inhoud van deze e-mails, de namen van contactpersonen, de namen van mappen, de inhoud van mappen, berichten die je hebt opgesteld, de inhoud van de berichten die je hebt opgesteld, inhoud die je hebt ingevoerd in of ontvangen via het Messenger-deelvenster, sociale inhoud of updates die zijn weergegeven in het rechterdeelvenster en alle andere informatie die zichtbaar is op het browsertabblad wanneer tracering voor fouten opsporen is ingeschakeld. Microsoft gebruikt deze gegevens uitsluitend voor foutrapportage en fouten opsporen om Microsoft-producten en -services te leveren en te verbeteren."}}},
    Easi: {AliasInterrupt: {ButtonSkip: {".": "Nu overslaan"}, ButtonStart: {".": "Maak een alias"}}},
    UnifiedInbox: {UnresolvedSenderEmail: {FriendlyEmailDisplayNameFormat: {".": "{0} ({1})"}}},
    ShareSkyDrive: {
        SkyTemplateError: {".": null},
        TemplateHeaderPlural: {".": "{0} wil bestanden met je delen op OneDrive. Klik op de links hieronder om de bestanden te bekijken."},
        Folder: {".": "map"},
        SkyAPIError: {".": "Je e-mail kan nu niet worden verzonden omdat OneDrive niet toegankelijk is. Sla een concept op en verzend het bericht later."},
        Remove: {".": "Verwijderen"},
        View: {".": "Weergeven in OneDrive"},
        SkyPickerError: {".": "OneDrive is op dit moment niet toegankelijk. Probeer het later opnieuw."},
        TemplateHeaderSingular: {".": "{0} wil een bestand met je delen op OneDrive. Klik op de link hieronder om het bestand te bekijken."}
    },
    RTE: {
        Editor: {ConversionError: {".": "Er is een probleem gevonden in onderstaande HTML-code. Corrigeer de code en schakel dan pas over naar opgemaakte of niet-opgemaakte tekst."}},
        Toolbar: {
            JustifyLeft: {".": "Tekst links uitlijnen"},
            Indent: {".": "Inspringing vergroten"},
            ShowFontNames: {".": "Lettertype wijzigen"},
            InsertNumberedList: {".": "Genummerde lijst maken"},
            ItalicsCharacter: {".": "I"},
            ShowHighlightColors: {".": "Markeringskleur wijzigen"},
            Underline: {".": "Onderstrepen"},
            Bold: {".": "Vet"},
            EmojiPicker: {".": "Emoticon invoegen"},
            LoseFormattingAction: {".": "Doorgaan"},
            JustifyRight: {".": "Tekst rechts uitlijnen"},
            InsertLink: {".": "Hyperlink invoegen"},
            ShowFontSizes: {".": "Tekengrootte wijzigen"},
            BoldCharacter: {".": "B"},
            Outdent: {".": "Inspringing verkleinen"},
            LoseFormattingWarning: {".": "De opmaak gaat verloren."},
            ShowFontColors: {".": "Tekstkleur wijzigen"},
            JustifyCenter: {".": "Tekst centreren"},
            UnderlineCharacter: {".": "U"},
            Italic: {".": "Cursief"},
            InsertBulletedList: {".": "Lijst met opsommingstekens maken"}
        },
        InsertHyperlink: {OK: {".": "OK"}, Caption: {".": "Adres:"}},
        Mode: {
            RichText: {".": "RTF", Tooltip: {".": "Lettertypen, tekstkleuren en andere opmaakfuncties gebruiken"}},
            HTML: {
                ".": "Bewerken in HTML",
                Tooltip: {".": "HTML gebruiken om het bericht op te maken (en daarna in RTF-modus bekijken)"}
            },
            PlainText: {
                ".": "Niet-opgemaakte tekst",
                Tooltip: {".": "Geen opmaakfuncties (lettertypen en tekstkleuren) gebruiken"}
            }
        }
    },
    DeleteAllFromSender: {Generic: {".": "Alles verwijderen van..."}},
    MessageHeader: {
        Bcc: {".": "BCC: "},
        Cc: {".": "CC: "},
        To: {".": "Aan: "},
        Sent: {".": "Verzonden:"},
        From: {".": "Van:"},
        Saved: {".": "Opgeslagen:"}
    }
});
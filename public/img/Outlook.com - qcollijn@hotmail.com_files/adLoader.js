(function () {
    "use strict";
    window.microsoft || (window.microsoft = {});
    var n = window.microsoft;
    n.createNS = function (n, t) {
        var u, r, i;
        if (typeof n != "string" || n.length === 0)throw new Error("Namespace has to be not empty string.");
        for (t = t || window, u = n.split("."), r = 0; r < u.length; r++) {
            i = u[r], typeof t[i] == "undefined" && (t[i] = {});
            if (typeof t[i] != "object")throw new Error("Namespace cannot be create, part of it already exists and it is not an object.");
            t = t[i]
        }
        return t
    }, n.getNS = function (n, t) {
        var u, i, r;
        if (typeof n != "string" || n.length === 0)throw new Error("Namespace has to be not empty string.");
        for (t = t || window, u = n.split("."), i = 0; i < u.length; i++) {
            r = u[i];
            if (typeof t[r] != "object")return null;
            t = t[r]
        }
        return t
    }, n.doesNSExist = function (t, i) {
        return n.getNS(t, i) !== null
    }, n.getCtor = function (t, i) {
        if (typeof t != "string" || t.length === 0)throw new Error("Constructor has to be not empty string.");
        i = i || window;
        if (t.indexOf(".") > -1) {
            var r = t.split(".");
            t = r[r.length - 1], i = n.getNS(r.slice(0, r.length - 1).join("."), i)
        }
        if (i === null)throw new Error("Constructor cannot be fetched, namespace does not exist.");
        if (typeof i[t] != "function")throw new Error("Constructor cannot be fetched, it is not a function.");
        return i[t]
    }
})();
window.microsoft.createNS("microsoft.advertising.sdk.sf").ext = function (n) {
    "use strict";
    function o(n, t) {
        typeof e == "function" && e(n, t)
    }

    function h(n) {
        var f, r;
        switch (n.action) {
            case"collapse":
                f = "collapsed", u = i.DEFAULT, t = n.geom, r = "";
                break;
            case"resize":
                f = "geom-update", r = t = n.geom;
                break;
            case"notify_collapsed":
                f = "collapsed", u = i.DEFAULT;
                break;
            case"notify_expanded":
                f = "expanded", r = t = n.geom, u = i.EXPANDED;
                break;
            case"failed":
                f = "failed", r = t = n.geom, u = i.DEFAULT, r = {reason: n.reason}
        }
        o(f, r)
    }

    var f = n.hostOrigin || "", s = n.adReqTime, r = {}, e, t = n.geom, i = {
        DEFAULT: 0,
        EXPANDING: 1,
        EXPANDED: 2
    }, u = i.DEFAULT;
    r.expand = function (n, t) {
        if (!n || !n.l && !n.r && !n.b) {
            o("failed", {reason: "Bad dimensions passed in"});
            return
        }
        if (u !== i.DEFAULT)return;
        u = i.EXPANDING;
        var r = {action: "expand", dim: n, meta: t, reqTime: s};
        window.parent.postMessage(JSON.stringify(r), f)
    }, r.register = function (n, t, i) {
        e = typeof i == "function" ? i : function () {
        }
    }, r.geom = function () {
        return t || {}
    }, r.supports = function () {
        return {"exp-ovr": !0, "exp-push": !1, "read-cookie": !1, "write-cookie": !1}
    }, r.collapse = function () {
        if (u === i.DEFAULT)return;
        var n = {action: "collapse", reqTime: s};
        window.parent.postMessage(JSON.stringify(n), f)
    }, r.status = function () {
    }, r.inViewPercentage = function () {
        return t && t.self && typeof t.self.iv == "number" ? t.self.iv * 100 : 1
    }, window.$sf = window.sf = {ext: r};
    jQuery(window).on("message", function (n) {
        var r = n.originalEvent, t, i = "";
        if (f.lastIndexOf(r.origin, 0) === 0) {
            i = r.data;
            if (typeof i == "string") {
                try {
                    t = JSON.parse(i)
                } catch (u) {
                }
                t.cpp ? window.GETADR(t.cpp) : h(t)
            }
        }
    })
};
(function () {
    "use strict";
    function t(n) {
        var t = {success: !1, reqTime: u};
        t.error = n !== null && typeof n == "object" ? n : {errorText: n || "Exception"}, window.parent.postMessage(JSON.stringify(t), r)
    }

    function s(t) {
        var e, i, f;
        t = t || {}, i = window.$sf;
        if (!i || !i.ext || typeof i.ext.inViewPercentage != "function")return;
        f = i.ext.inViewPercentage(), typeof f != "number" || f < 50 || setTimeout(function () {
            var t, r;
            f = i.ext.inViewPercentage();
            if (typeof f == "number" && f >= 50) {
                if (!n)return;
                n.Viewability === "true" && (t = n.GSERV, typeof t == "string" && (t.lastIndexOf("https://", 0) === 0 || t.lastIndexOf("//", 0) === 0) && (r = p.replace("{GSERV}", t).replace("{CID}", n.CID).replace("{PID}", n.PID).replace("{TID}", n.TargetID).replace("{PG}", n.PG).replace("{ASID}", n.ASID).replace("{ANID}", n.ANID), r += "&_=" + +new Date, window.ORMMA.request(r)))
            }
        }, 1e3), e = {success: !0, payload: t, reqTime: u}, window.parent.postMessage(JSON.stringify(e), r)
    }

    function l(n) {
        for (var u = {}, r = n.split(";"), i, t = 0; t < r.length; t++)i = r[t].split(":="), i.length === 2 && (u[i[0]] = decodeURIComponent(i[1]));
        return u
    }

    function a(n) {
        return w.test(n) ? !0 : !1
    }

    function v() {
        var n, w = !1, o = l(window.location.hash.slice(1)), h = o.pu, b = o.t, p, y, v;
        r = o.o, u = o.rt, f = o.m;
        try {
            p = JSON.parse(o.g)
        } catch (k) {
            p = {}
        }
        y = window.microsoft.getNS("microsoft.advertising.sdk.sf") || {}, typeof y.ext == "function" && y.ext({
            hostOrigin: r,
            adReqTime: u,
            geom: p
        });
        if (o.cpp === "1") {
            window.parent.postMessage(JSON.stringify({action: "get_payload", reqTime: u}), r);
            return
        }
        if (!a(h)) {
            t();
            return
        }
        i = window.setTimeout(function () {
            window.GETADR = function () {
            }, n && n.parentNode && n.parentNode.removeChild(n), w = !0, clearTimeout(i), t("timeout")
        }, parseInt(b, 10)), e = new Date, h += h.lastIndexOf("?") > -1 ? "&" : "?", h += "_=" + e.getTime(), document.write("<script type='text/javascript' id='payloadScript' src='" + h + "'><\/script>"), n = document.getElementById("payloadScript"), v = function () {
            if (c)return;
            if (w)return;
            clearTimeout(i), s({tpc: !0})
        }, window.addEventListener ? n.addEventListener("load", v, !1) : window.attachEvent ? n.attachEvent("onreadystatechange", function () {
            /loaded|complete/.test(n.readyState) && (v(), n.detachEvent("onreadystatechange"))
        }) : window.onload && (n.onload = v), n.onerror = function () {
            if (w)return;
            clearTimeout(i), t()
        }
    }

    var r, u = "", w = /^https:(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i, p = "{GSERV}/view/{CID}?EVT=EV&PID={PID}&UIT=G&TargetID={TID}&PG={PG}&ASID={ASID}&ANID={ANID}", n = null, c = !1, i, e, h, y = null, o, f;
    window.ORMMA = {
        request: function (n) {
            var t = jQuery("#pixelContainerId");
            t.length === 0 && (t = jQuery("<div id='pixelContainerId' style='display:none;'></div>").appendTo("body")), typeof n == "string" && t.append('<img style="display:none;" src=\'' + n + "' width='1' height='1' />")
        }
    }, window.setCreativeData = function (t, i) {
        var f = {action: "setcreativedata", cData: i, reqTime: u};
        window.parent.postMessage(JSON.stringify(f), r), n = i
    }, window.GETADR = function (n) {
        var u, v, l, r, a;
        c = !0, u = "", i && clearTimeout(i), h = new Date, y = n, o = h - e;
        if (!n || n.err) {
            t("Invalid Payload");
            return
        }
        if (n.error) {
            n.error.fbu = n.fbu, t(n.error);
            return
        }
        for ((f === "zh-tw" || f === "ja-jp") && (v = "content/adBar-" + f + ".css", jQuery("<link rel='stylesheet' type='text/css' media='all' href='" + v + "'/>").appendTo("head")), l = n.rdr || [], r = 0; r < l.length; r++)if (l[r].c === "wl") {
            u = l[r].u;
            break
        }
        a = u.match(/adbar\/products\/v(\d+(.\d+)?)\//), (!a || a.length < 2 || parseFloat(a[1]) < 3.1) && (u = "https://ads1.msads.net/adbar/products/v3.1/rafStitcher.js"), jQuery.ajax({
            url: u,
            dataType: "script",
            cache: !0,
            success: function () {
                var i = window.microsoft.getNS("microsoft.advertising.renderers.stitcher") || {};
                typeof i.render == "function" ? i.render(n, o, function () {
                    s({raf: !0})
                }) : t("Error calling rendering on stitcher")
            },
            error: function () {
                t("Error loading RAF stitcher")
            }
        })
    }, v()
})()
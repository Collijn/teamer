// Copyright (c) ADRTA.COM 2011-2013 - ALL RIGHTS RESERVED
if (!window.__adrta__aait) {
    __adrta__aait = ""
}
if (!window.__adrta__aasi) {
    __adrta__aasi = ""
}
if (!window.__adrta__aast) {
    __adrta__aast = ""
}
if (!window.__adrta__aavi) {
    __adrta__aavi = ""
}
if (!window.__adrta__aavt) {
    __adrta__aavt = ""
}
if (!window.__adrta__aadb) {
    __adrta__aadb = ""
}
if (!window.__adrta__aacb) {
    __adrta__aacb = ""
}
if (!window.__adrta__aasm) {
    __adrta__aasm = 0
}
var Pixalate = (function () {
    var U = "9.0";
    var o = /http(s)?:\/\/(cdn|js).adrta.com\/aa.js#.*/;
    var s = /(?:\?([^#]*))/;
    var J = /https?:\/\/\w.*[\w]/;
    var D = /(\w+):\/{2}([^\/:]+)(?:\:(\d+))?(\/(?:[^?]+\/)?)?([^\?#]+)?(?:\?([^#]*))?(\#.*)?$/;
    var am = ",970x66,728x90,300x250,180x150,160x600,468x60,234x60,120x90,120x60,125x125,88x31,120x240,336x280,300x100,720x300,250x250,240x400,300x600,120x600,970x90,970x250,300x1050,550x480,700x500,640x480,400x300,950x90,300x150,320x50,200x200,168x42,168X42,216x54,300x75,300x50,216x36,168x28,192x53,";
    var q = [15000, 15000, 30000, 30000, 60000, 60000];
    var z = [60000, 180000];
    var aT = 0;
    var aC = 1000;
    var R = true;
    var au = 0.5;
    var u = 511;
    var aH = 2047;
    var aK = 9000000000000000;
    var ao = false;
    var d = [];
    var aG = new Date().getTimezoneOffset();
    var I = (navigator.userAgent.indexOf("Opera Mini") != -1);
    var aS = (navigator.userAgent.indexOf("MSIE") != -1);
    var ap = window.location.protocol;
    if (ap.indexOf("http") != 0) {
        ap = "http:"
    }
    var ai = Math.floor(Math.random() * aK);
    d.push(ai);
    var an = {Unknown: 0, FireFox: 1, Chrome: 2, Safari: 3, IE: 4, IE7: 5, IE8: 6, IE9: 7, IE10: 8, IE11: 8, IE12: 8};
    var av = function () {
        try {
            var j;
            var e;
            var n;
            switch (navigator.appName.toLowerCase()) {
                case"microsoft internet explorer":
                    j = an.IE;
                    n = navigator.appVersion.substring(navigator.appVersion.indexOf("MSIE") + 5, navigator.appVersion.length);
                    e = parseFloat(n.split(";")[0]);
                    switch (parseInt(e)) {
                        case 7:
                            j = an.IE7;
                            break;
                        case 8:
                            j = an.IE8;
                            break;
                        case 9:
                            j = an.IE9;
                            break;
                        case 10:
                            j = an.IE10;
                            break;
                        case 11:
                            j = an.IE11;
                            break;
                        case 12:
                            j = an.IE12;
                            break
                    }
                    break;
                case"netscape":
                    if (navigator.userAgent.toLowerCase().indexOf("chrome") > -1) {
                        j = an.Chrome
                    } else {
                        if (navigator.userAgent.toLowerCase().indexOf("firefox") > -1) {
                            j = an.FireFox
                        }
                    }
                    break;
                default:
                    j = an.Unknown;
                    break
            }
            return j
        } catch (i) {
        }
    };
    var ah = 0;
    var E = null;
    try {
        E = document.currentScript
    } catch (ae) {
    }
    if (!E) {
        ah = 1;
        var al = document.getElementsByTagName("script");
        var G = [];
        for (var ab = 0; ab < al.length; ++ab) {
            G[ab] = al[ab]
        }
        E = G[G.length - 1];
        for (var X = G.length - 1; X >= 0; --X) {
            var W = G[X];
            if (W && W.src && o.test(W.src)) {
                E = W;
                ah = 0;
                break
            }
        }
    }
    var aQ = E.src.substring(E.src.indexOf("#") + 1).replace(/%%/g, "%25%25").split(";");
    var Q = aQ.shift();
    if (Q.indexOf("=") != -1 || Q.length == 0 || E.src.indexOf("#") == -1) {
        Q = "px"
    }
    var l = [];
    for (var aa = 0; aa < aQ.length; ++aa) {
        if (aQ[aa].indexOf("blocked") != -1) {
            ao = true
        }
        if (J.test(aQ[aa])) {
            aQ[aa] = aQ[aa].replace(D, "$1://$2$4$5")
        }
        if (aQ[aa].indexOf("kv2=") != -1) {
            l.push(aQ.splice(aa, 1))
        }
    }
    var aI = "__aaci=" + Q;
    if (aQ.length > 0) {
        aI += "&" + aQ.join("&")
    }
    var aJ = aI.indexOf("&__aa__=");
    if (aJ != -1) {
        aI = aI.substring(0, aJ)
    }
    __adrta__aadb = __adrta__aadb || (("&" + aI + "&").indexOf("&debug=true&") != -1);
    var t = true;
    var k = 0;
    var aA = "";
    var ad = "";
    var az = window;
    try {
        var O = window;
        for (var ab = 0; ab < 16; ++ab) {
            t = true;
            try {
                aA = O.location.href.replace(s, "").substring(0, u);
                if (aA == "undefined") {
                    aA = O.location.href.substring(0, u)
                }
                ad = O.document.referrer.replace(s, "").substring(0, u);
                if (ad == "undefined") {
                    ad = O.document.referrer.substring(0, u)
                }
                az = O
            } catch (ae) {
                t = false
            }
            if (O == window.top) {
                break
            }
            O = O.parent;
            ++k
        }
    } catch (ae) {
        t = false
    }
    var h = false;
    var af = R;
    if (t) {
        af = true;
        var aD = null;
        if (aS) {
            aD = az.document.onfocusin;
            az.document.onfocusin = function () {
                af = true;
                if (aD) {
                    try {
                        aD()
                    } catch (i) {
                    }
                }
            }
        } else {
            aD = az.onfocus;
            az.onfocus = function () {
                af = true;
                if (aD) {
                    aD()
                }
            }
        }
        var T = null;
        var aw = null;
        if (aS) {
            T = az.document.onfocusout;
            az.document.onfocusout = function () {
                if (aw != az.document.activeElement) {
                    aw = az.document.activeElement
                } else {
                    af = false;
                    if (T) {
                        try {
                            T()
                        } catch (i) {
                        }
                    }
                }
            }
        } else {
            T = az.onblur;
            az.onblur = function () {
                af = false;
                if (T) {
                    T()
                }
            }
        }
    }
    var aq = 0;
    var aM = 0;
    var C = function () {
        if (aM != 0) {
            var i = new Date().getTime();
            aq += i - aM;
            aM = i
        }
        var e = aq;
        aq = 0;
        return e
    };
    var f = function () {
        var i = function () {
            aM = new Date().getTime()
        };
        var e = function () {
            if (aM != 0) {
                aq += new Date().getTime() - aM;
                aM = 0
            }
        };
        if (aL.addEventListener) {
            aL.addEventListener("mouseover", i, false);
            aL.addEventListener("mouseout", e, false)
        } else {
            if (aL.attachEvent) {
                aL.attachEvent("mouseover", i);
                aL.attachEvent("mouseout", e)
            }
        }
    };
    var aL = null;
    var aX = false;
    var r = 0;
    var ar = 0;
    var aU = 0;
    var ax = new Date().getTime();
    var aP = function (e, i) {
        return (am.indexOf("," + e + "x" + i + ",") != -1)
    };
    var at = function (e) {
        if (e.getAttribute("height")) {
            return parseInt(e.getAttribute("height"))
        }
        if ((e.style) && (e.style.height) && (e.style.height.indexOf("%") == -1)) {
            return parseInt(e.style.height)
        }
        return e.offsetHeight
    };
    var ac = function (e) {
        if (e.getAttribute("width")) {
            return parseInt(e.getAttribute("width"))
        }
        if ((e.style) && (e.style.width) && (e.style.height.indexOf("%") == -1)) {
            return parseInt(e.style.width)
        }
        return e.offsetWidth
    };
    var ay = function (j) {
        if (!j) {
            return j
        }
        if (j.nodeType != 1) {
            return ay(j.nextSibling)
        }
        if (j.tagName == "SCRIPT") {
            return ay(j.nextSibling)
        }
        var e = ac(j);
        var n = at(j);
        if (aP(e, n)) {
            r = e;
            ar = n;
            return j
        }
        if (j.tagName == "IMG") {
            return ay(j.nextSibling)
        }
        if (!j.firstChild) {
            return ay(j.nextSibling)
        }
        var i = ay(j.firstChild);
        if (i) {
            return i
        }
        return ay(j.nextSibling)
    };
    var aE = function () {
        aL = ay(E.parentNode.firstChild);
        if (!aL && (window != top)) {
            aL = ay(document.body)
        }
        if (aL) {
            aX = true;
            aU = new Date().getTime() - ax;
            if (aU < 1) {
                aU = 1
            }
            f()
        }
    };
    aE();
    var a = false;
    var p = 0;
    var m = 0;
    var K = 0;
    var Y = 0;
    var c = false;
    var aF = new Date().getTime();
    var aW = false;
    var aN = 0;
    var H = 0;
    var V = [0, 0, 0, 0, 0, 0, 0];
    var ak = function () {
        var i = new Date().getTime();
        var e = i - aF;
        aF = i;
        return e
    };
    var aV = function (e) {
        var j = 0;
        var i = 0;
        if (e.offsetParent) {
            do {
                if (e.style.position == "fixed") {
                    j = e.getBoundingClientRect().left;
                    i = e.getBoundingClientRect().top;
                    break
                }
                j += e.offsetLeft;
                j -= e.scrollLeft;
                i += e.offsetTop;
                i -= e.scrollTop
            } while ((e = e.offsetParent) && (e.tagName != "BODY"))
        }
        return [j, i]
    };
    var A = function (j) {
        try {
            var aZ = j.parent.document.getElementsByTagName("IFRAME");
            for (var n = 0; n < aZ.length; n++) {
                if (aZ[n].contentWindow == j) {
                    return aZ[n]
                }
            }
        } catch (w) {
        }
        return null
    };
    var N = function () {
        var i = 0;
        var e = 0;
        if (typeof(az.innerWidth) == "number") {
            i = az.innerWidth;
            e = az.innerHeight
        } else {
            if (az.document.documentElement && (az.document.documentElement.clientWidth || az.document.documentElement.clientHeight)) {
                i = az.document.documentElement.clientWidth;
                e = az.document.documentElement.clientHeight
            }
        }
        return [i, e]
    };
    var P = function () {
        var i = 0;
        var e = 0;
        if (typeof(az.pageYOffset) == "number") {
            e = az.pageYOffset;
            i = az.pageXOffset
        } else {
            if (az.document.body && (az.document.body.scrollLeft || az.document.body.scrollTop)) {
                e = az.document.body.scrollTop;
                i = az.document.body.scrollLeft
            } else {
                if (az.document.documentElement && (az.document.documentElement.scrollLeft || az.document.documentElement.scrollTop)) {
                    e = az.document.documentElement.scrollTop;
                    i = az.document.documentElement.scrollLeft
                }
            }
        }
        return [i, e]
    };
    var L = false;
    var v = function () {
        aW = true;
        ++H;
        if (af) {
            h = true;
            if (aX && t) {
                var bi = 0;
                var bg = 0;
                var bc = window;
                for (var bl = 0; bl < 16; ++bl) {
                    try {
                        var a7 = A(bc);
                        if (a7) {
                            var a3 = aV(a7);
                            bi += a3[0];
                            bg += a3[1]
                        }
                    } catch (bn) {
                    }
                    if (bc == window.top) {
                        break
                    }
                    bc = bc.parent
                }
                var aZ = P();
                var be = aZ[1];
                var bp = aZ[0];
                var bf = N();
                var n = be + bf[1];
                var bm = bp + bf[0];
                var a0 = Math.max(az.document.documentElement.clientHeight, az.document.body.scrollHeight, az.document.documentElement.scrollHeight, az.document.body.offsetHeight, az.document.documentElement.offsetHeight);
                var a2 = Math.max(az.document.documentElement.clientWidth, az.document.body.scrollWidth, az.document.documentElement.scrollWidth, az.document.body.offsetWidth, az.document.documentElement.offsetWidth);
                V[0] += a0;
                V[1] += a2;
                var a9 = a0 * 0.2;
                var j = 0;
                for (var bl = 2; bl < 7; ++bl) {
                    var ba = j + a9;
                    var a8 = ba - j;
                    if (a8 < 0) {
                        j = ba;
                        continue
                    }
                    if (j < be) {
                        j = be
                    }
                    if (ba > n) {
                        ba = n
                    }
                    var br = ba - j;
                    j = ba;
                    if (br <= 0) {
                        continue
                    }
                    V[bl] += Math.floor((br / a8) * 1000) / 1000
                }
                ++aN;
                var a3 = aV(aL);
                var bq = a3[1] + bg;
                var bk = a3[0] + bi;
                var a4 = bq + aL.offsetHeight;
                var bd = bk + aL.offsetWidth;
                var bb = (bq < be) ? be : bq;
                var a1 = (bk < bp) ? bp : bk;
                var bj = (a4 > n) ? n : a4;
                var bh = (bd > bm) ? bm : bd;
                var a6 = (a4 - bq) * (bd - bk);
                if (a6 < 0) {
                    a6 = 0
                }
                var bo = (bj - bb) * (bh - a1);
                if (bo < 0) {
                    bo = 0
                }
                p = bk;
                m = bq;
                var a5 = 0;
                if (I) {
                    a5 = 1
                } else {
                    if (a6 > 0) {
                        a5 = Math.floor((bo / a6) * 1000) / 1000
                    }
                    if (!L) {
                        if ((bq + ((a4 - bq) / 2) <= bf[1]) && (bk + ((bd - bk) / 2) <= bf[0])) {
                            a = true
                        }
                    }
                }
                if (a5 >= au) {
                    K += a5;
                    ++Y;
                    c = true
                }
            } else {
                if (R) {
                    ++K;
                    ++Y;
                    c = true
                }
            }
        }
        L = true
    };
    var g = function (a3, n, a2) {
        var j = "";
        var aZ = "";
        var i = "";
        if (a3) {
            for (var a1 in a3) {
                j += "&" + a1 + "=" + encodeURIComponent(a3[a1])
            }
        }
        if (n) {
            for (var a0 in n) {
                if (a0 != "__aaam") {
                    aZ += "&" + a0 + "=" + encodeURIComponent(n[a0])
                }
            }
            if (n.__aaam) {
                i += "&__aaam=" + encodeURIComponent(n.__aaam)
            }
        }
        var w = document.createElement("script");
        w.setAttribute("type", "text/javascript");
        var e = (ap + "//adrta.com/i?");
        e += j;
        e += "&" + aI;
        if (l.length > 0) {
            e += "&" + l.join("")
        }
        e += aZ;
        e += i;
        e = e.substring(0, aH);
        w.setAttribute("src", e);
        if (w.readyState) {
            w.onreadystatechange = function () {
                if (w.readyState == "loaded" || w.readyState == "complete") {
                    w.onreadystatechange = null;
                    w.parentNode.removeChild(w);
                    if (a2) {
                        a2()
                    }
                }
            }
        } else {
            w.onload = function () {
                w.onload = null;
                w.parentNode.removeChild(w);
                if (a2) {
                    a2()
                }
            }
        }
        document.getElementsByTagName("head")[0].appendChild(w)
    };
    var Z = function (a1) {
        var a0 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var j = "";
        var a9, a6, a4, a8, a5, a3, a2;
        var w = 0;
        a1 = a1.replace(/\r\n/g, "\n");
        var a7 = "";
        for (var e = 0; e < a1.length; e++) {
            var aZ = a1.charCodeAt(e);
            if (aZ < 128) {
                a7 += String.fromCharCode(aZ)
            } else {
                if ((aZ > 127) && (aZ < 2048)) {
                    a7 += String.fromCharCode((aZ >> 6) | 192);
                    a7 += String.fromCharCode((aZ & 63) | 128)
                } else {
                    a7 += String.fromCharCode((aZ >> 12) | 224);
                    a7 += String.fromCharCode(((aZ >> 6) & 63) | 128);
                    a7 += String.fromCharCode((aZ & 63) | 128)
                }
            }
        }
        a1 = a7;
        while (w < a1.length) {
            a9 = a1.charCodeAt(w++);
            a6 = a1.charCodeAt(w++);
            a4 = a1.charCodeAt(w++);
            a8 = a9 >> 2;
            a5 = ((a9 & 3) << 4) | (a6 >> 4);
            a3 = ((a6 & 15) << 2) | (a4 >> 6);
            a2 = a4 & 63;
            if (isNaN(a6)) {
                a3 = a2 = 64
            } else {
                if (isNaN(a4)) {
                    a2 = 64
                }
            }
            j = j + a0.charAt(a8) + a0.charAt(a5) + a0.charAt(a3) + a0.charAt(a2)
        }
        return j
    };
    var aR = false;
    var M = function (w) {
        var i = {};
        var e = {};
        i.__aasv = U;
        i.__aaii = ai;
        if (__adrta__aait != 0) {
            i.__aait = __adrta__aait
        }
        if (__adrta__aasi.length > 0) {
            i.__aasi = __adrta__aasi;
            i.__aast = __adrta__aast
        }
        if (__adrta__aavi.length > 0) {
            i.__aavi = __adrta__aavi;
            i.__aavt = __adrta__aavt
        }
        i.__aavz = aG;
        i.__aaib = (((k > 0) && t) ? 1 : 0);
        i.__aaai = ((k > 0) ? 1 : 0);
        i.__aaaa = (a ? 1 : 0);
        i.__aafl = (t) ? az.innerHeight || az.document.documentElement.clientHeight : 0;
        i.__aaaf = (aX ? 1 : 0);
        i.__aaag = aU;
        i.__aaax = Math.floor(p);
        i.__aaay = Math.floor(m);
        if ((r != 0) || (ar != 0)) {
            i.__aasz = r + "x" + ar
        }
        i.__aapf = (h ? 1 : 0);
        if (ah != 0) {
            i.__aaec = ah
        }
        if (!aW) {
            i.__aaae = 0;
            i.__aaat = 0;
            i.__aaav = 0;
            i.__aaas = 0;
            i.__aaah = 0;
            i.__aaph = 0;
            i.__aapw = 0;
            i.__aapc = 0;
            i.__aap1 = 0;
            i.__aap2 = 0;
            i.__aap3 = 0;
            i.__aap4 = 0;
            i.__aap5 = 0
        } else {
            i.__aaup = 1;
            h = false;
            if (Y == 0) {
                i.__aaat = 0;
                i.__aaae = 0
            } else {
                i.__aaat = Y * aC;
                i.__aaae = Math.round((K / Y) * 1000) / 1000;
                if (i.__aaat <= 0 || i.__aaae <= 0) {
                    i.__aaat = 0;
                    i.__aaae = 0
                }
            }
            i.__aaav = (c ? 1 : 0);
            K = 0;
            Y = 0;
            i.__aaas = ak();
            i.__aaah = C();
            if (i.__aaas < 0) {
                i.__aaas = 0
            }
            if (i.__aaah < 0) {
                i.__aaah = 0
            }
            if (i.__aaat > i.__aaas) {
                i.__aaat = i.__aaas
            }
            if (i.__aaah > i.__aaas) {
                i.__aaah = i.__aaas
            }
            if (H > 0) {
                i.__aapc = H * aC;
                H = 0
            }
            if (aN > 0) {
                i.__aaph = Math.ceil(V[0] / aN);
                i.__aapw = Math.ceil(V[1] / aN);
                i.__aap1 = Math.round((V[2] / aN) * 1000) / 1000;
                i.__aap2 = Math.round((V[3] / aN) * 1000) / 1000;
                i.__aap3 = Math.round((V[4] / aN) * 1000) / 1000;
                i.__aap4 = Math.round((V[5] / aN) * 1000) / 1000;
                i.__aap5 = Math.round((V[6] / aN) * 1000) / 1000;
                for (var j = 0; j < 7; ++j) {
                    V[j] = 0
                }
                aN = 0
            } else {
                i.__aaph = 0;
                i.__aapw = 0;
                i.__aapc = 0;
                i.__aap1 = 0;
                i.__aap2 = 0;
                i.__aap3 = 0;
                i.__aap4 = 0;
                i.__aap5 = 0
            }
        }
        if (ao) {
            i.__aaab = 1
        }
        if (screen && screen.width && screen.height) {
            i.__aass = screen.width + "x" + screen.height
        }
        e.__aapu = aA;
        e.__aapr = ad;
        if (__adrta__aadb) {
            i.__aadb = 1;
            if (!aR) {
                e.__aaam = Z(E.parentNode.innerHTML);
                aR = true
            }
        }
        g(i, e, w)
    };
    var F = q[aT++];
    var b = new Date().getTime() + F;
    var ag = false;
    var B = 0;
    var aY = false;

    function S(e) {
        ao = e;
        M()
    }

    function aB() {
        var j = ("https:" == document.location.protocol ? "https://" : "http://") + "testforblockedad.com";
        var i = function () {
            var w = "ad-image-id";
            var n = document.createElement("IMG");
            n.id = w;
            n.style.width = "1px";
            n.style.height = "1px";
            n.style.top = "-1000px";
            n.style.left = "-1000px";
            document.body.appendChild(n);
            n.src = j + "/ads/ad.gif";
            setTimeout(function (aZ) {
                if (!aZ) {
                    S(true);
                    aZ.parentNode.removeChild(aZ);
                    return
                }
                if (aZ.complete && aZ.style.visibility == "hidden") {
                    S(true);
                    aZ.parentNode.removeChild(aZ);
                    return
                }
                if (aZ) {
                    aZ.parentNode.removeChild(aZ)
                }
                e()
            }, 1000, n)
        };
        var e = function () {
            var aZ = "ad-script-id";
            var n = document.createElement("SCRIPT");
            n.id = aZ;
            n.type = "text/javascript";
            n.src = "http://testforblockedad.com/pagead/show_ads.js";
            if (n.readyState) {
                n.onreadystatechange = function () {
                    if (n.readyState == "loaded" || n.readyState == "complete") {
                        n.onreadystatechange = null;
                        n.parentNode.removeChild(n);
                        S(false);
                        aO()
                    } else {
                        n.parentNode.removeChild(n);
                        S(true)
                    }
                }
            } else {
                var w = setTimeout(function () {
                    S(true)
                }, 1000);
                n.onload = function () {
                    n.onload = null;
                    n.parentNode.removeChild(n);
                    clearTimeout(w);
                    S(false);
                    aO()
                }
            }
            document.getElementsByTagName("head")[0].appendChild(n)
        };
        i()
    }

    var aO = function () {
        aY = true;
        B = 0;
        if (F) {
            window.setTimeout(x, aC)
        }
    };
    var x = function () {
        try {
            var i = new Date().getTime();
            if (!aX) {
                aE();
                B++
            }
            if (aX && (__adrta__aait > 0) && (__adrta__aasi.length > 0) && (__adrta__aast > 0) && (__adrta__aavi.length > 0) && (__adrta__aavt > 0)) {
                v();
                if (!ag && c) {
                    M();
                    ag = true;
                    b = i + F
                } else {
                    if ((h || (K > 0)) && (b <= i)) {
                        M();
                        if ((k > 0) && !((k > 0) && t)) {
                            F = z[aT++]
                        } else {
                            F = q[aT++]
                        }
                        b = i + F
                    }
                }
            }
        } catch (j) {
        }
        if (F) {
            window.setTimeout(x, aC)
        }
    };
    var y = function (i) {
        document.removeEventListener("webkitvisibilitychange", y);
        if (window.__adrta__aasm == 1) {
            window.setTimeout(M, 1000);
            window.setTimeout(x, 100 + aC)
        } else {
            window.__adrta__aasm = 1;
            M();
            window.setTimeout(x, aC)
        }
    };
    if ((typeof document.webkitVisibilityState == "undefined") || (typeof document.webkitVisibilityState != "undefined" && document.webkitVisibilityState != "prerender")) {
        if (window.__adrta__aasm == 1) {
            window.setTimeout(M, 1000);
            window.setTimeout(x, 100 + aC)
        } else {
            window.__adrta__aasm = 1;
            M();
            window.setTimeout(x, aC)
        }
    } else {
        if (typeof document.webkitVisibilityState != "undefined" && document.webkitVisibilityState == "prerender") {
            document.addEventListener("webkitvisibilitychange", y, false)
        }
    }
    function aj() {
    }

    aj.tag = function (i) {
        var e = /\${PXL8_CLICK}/g;
        return i.replace(e, aj.getClickURL())
    };
    aj.getClickURL = function () {
        var e = aQ.join("&");
        if (l.length > 0) {
            e += "&" + l.join("")
        }
        return "http://adrta.com/c?clid=" + Q + "&" + e + "&__aaii=" + ai + "&redirect="
    };
    return aj
})();
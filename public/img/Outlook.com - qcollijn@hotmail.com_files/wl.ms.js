(function () {
    if (!window.WL && !window.OneDrive) {
        window.OneDrive = {};
        OneDrive.Constants = {WebViewLink: W, DownloadLink: sb};
        OneDrive.open = function (c) {
            var b = h(c), a = new f(b, ce);
            try {
                a.initialize();
                a.validateOpenParameters();
                a.executeOpenOperation()
            } catch (d) {
                a.processError(d, T)
            }
        };
        OneDrive.save = function (c) {
            var b = h(c), a = new f(b, de);
            try {
                a.initialize();
                a.validateSaveParameters();
                a.executeSaveOperation()
            } catch (d) {
                a.processErrorCallback(d, T)
            }
        };
        OneDrive.createOpenButton = function (d) {
            var c = h(d), a = new f(c, ac);
            try {
                a.initialize();
                a.validateOpenParameters();
                a.validateButtonParameters();
                var b = a.createButtonElement();
                e(b, L, function () {
                    a.executeOpenOperation()
                });
                return b
            } catch (g) {
                a.processError(g, T);
                return null
            }
        };
        OneDrive.createSaveButton = function (d) {
            var c = h(d), a = new f(c, pd);
            try {
                a.initialize();
                a.validateSaveParameters();
                a.validateButtonParameters();
                var b = a.createButtonElement();
                e(b, L, function () {
                    a.executeSaveOperation()
                });
                return b
            } catch (g) {
                a.processErrorCallback(g, T);
                return null
            }
        };
        function f(d, e) {
            var b = this, c = d[Qd];
            b._internalApp = WL.UnitTests && typeof c === E ? c : a;
            b._options = d;
            b._method = e
        }

        f.onloadInit = function () {
            fb(function () {
                var c = document.querySelectorAll(sd);
                for (var b = 0; b < c.length; b++) {
                    var a = c[b];
                    f.createButtonElement(a, X, db);
                    var g = a.href, d = new f({file: g}, id);
                    a.href = "#";
                    e(a, L, function () {
                        d.initialize();
                        d.executeSaveOperation();
                        return false
                    })
                }
            })
        };
        f.createButtonElement = function (a, e, d) {
            var b = {};
            b[j] = e;
            b[C] = d;
            var c = Yb(b, true);
            a.innerHTML = c.innerHTML;
            a.id = c.buttonId;
            a.title = c.buttonTitle;
            cc(b, a);
            return a
        };
        f.prototype = {
            initialize: function () {
                var a = this;
                if (a._internalApp._status !== yb) {
                    var b = ve();
                    if (!b)throw new Error(ud.replace(i, a._method));
                    a._internalApp.appInit({client_id: b, interface_method: a._method});
                    of("Picker SDK initialized with client_id: " + b, ONEDRIVE_PREFIX)
                }
            }, executeOpenOperation: function () {
                var b = this, e = b._internalApp, h = b._method, a = b._options, c = a[rc], f = a[ab], g = a[bb], d = {
                    mode: G,
                    resourceType: V,
                    select: a[xc] ? ub : y,
                    linkType: c,
                    interface_method: h
                };
                e.fileDialog(d).then(function (l) {
                    var b = l.apiResponse, e = c === sb, k = {
                        link: e ? null : b.webUrl,
                        values: []
                    }, j = e ? b.data : b.children && b.children.length > 0 ? b.children : [b];
                    for (var i = 0; i < j.length; i++) {
                        var a = j[i], h = [], g = e ? a.images : a.thumbnails && a.thumbnails[0];
                        if (g)if (e)for (var d = 0; d < g.length; d++)h.push(g[d].source); else for (var d = 0; d < VROOM_THUMBNAIL_SIZES.length; d++)h.push(g[VROOM_THUMBNAIL_SIZES[d]].url);
                        k.values.push({
                            fileName: a.name,
                            link: e ? a.source : a.webUrl,
                            linkType: c,
                            size: a.size,
                            thumbnails: h
                        })
                    }
                    f(k)
                }, function (a) {
                    a.error === Cc ? k(g) : b.processError(a, ec)
                })
            }, executeSaveOperation: function () {
                var a = this, c = a._internalApp, b = a._options, e = a._method, f = b[Ec], d = b[qc], g = Tc;
                if (Qb(f)) {
                    g = Xc;
                    d = d || we(f)
                }
                var j = b[ab], h = b[sc], m = b[bb], l = {mode: F, resourceType: U, select: y, interface_method: e};
                c.fileDialog(l).then(function (b) {
                    var r = b.pickerResponse, n = b.apiResponse, o = n.data && n.data[0].id;
                    switch (g) {
                        case Xc:
                            var l = o.split("."), s = l.length > 2 ? l[2] : "root", m = c.getAccessTokenForApi(), q = {
                                path: "drives/" + r.owner_cid + "/items/" + s + "/children",
                                method: Oc,
                                use_vroom_api: true,
                                request_headers: [{name: Fe, value: Id}, {name: Oe, value: "bearer " + m}],
                                response_headers: [Eb],
                                json_body: {"@content.sourceUrl": f, "name": d, "file": {}},
                                interface_method: e
                            };
                            c.api(q).then(function (b) {
                                var c = b[Eb];
                                b[zb] === jc && !Db(c) ? a.beginPolling(j, h, c, m) : a.processErrorCallback(b, vb)
                            }, function (b) {
                                a.processErrorCallback(b, vb)
                            });
                            break;
                        case Tc:
                            var p = {path: o, element: f, overwrite: xd, file_name: d, interface_method: e};
                            c.upload(p).then(function () {
                                k(j)
                            }, function (b) {
                                a.processErrorCallback(b, vb)
                            }, function (a) {
                                k(h, a.progressPercentage)
                            });
                            break;
                        default:
                            throw new Error(ld.replace(i, e))
                    }
                }, function (b) {
                    b.error === Cc ? k(m) : a.processError(b, ec)
                })
            }, beginPolling: function (i, e, h, g) {
                var a = this, b = Je, d = Sc, f = {
                    path: Ab(h, {access_token: g}),
                    method: Lb,
                    use_vroom_api: true,
                    response_headers: [Eb],
                    interface_method: a._method
                }, c = function () {
                    a._internalApp.api(f).then(function (f) {
                        switch (f[zb]) {
                            case jc:
                                k(e, f[yd]);
                                if (!d--) {
                                    b *= 2;
                                    d = Sc
                                }
                                D(c, b);
                                break;
                            case pe:
                                k(e, 100);
                                k(i);
                                break;
                            default:
                                a.processErrorCallback(f, Zb)
                        }
                    }, function (b) {
                        a.processErrorCallback(b, Zb)
                    })
                };
                D(c, b)
            }, validateOpenParameters: function () {
                m(this._options, [{name: rc, type: b, optional: true, defaultValue: W, allowedValues: [sb, W]}, {
                    name: xc,
                    type: Sb,
                    optional: true,
                    defaultValue: false
                }, {name: ab, type: c, optional: false}, {name: bb, type: c, optional: true}], this._method)
            }, validateSaveParameters: function () {
                m(this._options, [{name: qc, type: b, optional: true}, {name: Ec, type: b, optional: false}, {
                    name: ab,
                    type: c,
                    optional: true
                }, {name: sc, type: c, optional: true}, {name: bb, type: c, optional: true}, {
                    name: zc,
                    type: c,
                    optional: true
                }], this._method)
            }, validateButtonParameters: function () {
                m(this._options, [{
                    name: Ac,
                    type: b,
                    optional: true,
                    defaultValue: wb,
                    allowedValues: [wb, zd]
                }], this._method)
            }, createButtonElement: function () {
                var a = document.createElement("button"), c = this._method === ac ? z : X, b = this._options[Ac] === wb ? db : cb;
                return f.createButtonElement(a, c, b)
            }, processError: function (c, b) {
                var a = Rb(se, this._method, b, JSON.stringify(c));
                p(a, ONEDRIVE_PREFIX);
                return a
            }, processErrorCallback: function (b, a) {
                k(this._options[zc], this.processError(b, a))
            }
        };
        var qh = "download", d = "interface_method", nd = "WL.Internal.jsonp.", le = 2000, Oe = "Authorization", Uc = "body", kg = "json_body", B = "callback", kb = "code", Vf = "Content-Type", Hg = "element", Pc = "error", cg = "error_description", vg = "file_name", lg = "file_input", dg = "file_output", Ff = "request_headers", Df = "response_headers", Eb = "Location", Ic = "logging", hb = "message", Hb = "method", mg = "overwrite", xd = "rename", Mb = "path", yd = "percentageComplete", Fe = "Prefer", Ge = "pretty", Id = "respond-async", Qg = "result", Rg = "status", zb = "http_status", Yd = "return_ssl_resources", Wf = "stream_input", Ig = "tracing", Fc = "use_vroom_api", Sg = "error", jc = 202, pe = 200, zf = 500, wg = "success", Rd = "suppress_redirects", Af = "suppress_response_codes", H = "x_http_live_library", Qc = 0, yb = 1, n = "access_token", uh = "appstate", df = "authentication_key";
        AK_AUTH_TOKEN = "authentication_token", (AK_CLIENT_ID = "client_id", (AK_DISPLAY = "display", (AK_CODE = "code", (AK_ERROR = "error", (AK_ERROR_DESC = "error_description", (AK_EXPIRES = "expires", (AK_EXPIRES_IN = "expires_in", (AK_ITEMID = "item_id", (AK_LOCALE = "locale", (AK_OWNER_CID = "owner_cid", (AK_REDIRECT_URI = "redirect_uri", (AK_RESPONSE = "response", (AK_RESPONSE_TYPE = "response_type", (AK_REQUEST_TS = "request_ts", (AK_RESOURCEID = "resource_id", (AK_SCOPE = "scope", (AK_SESSION = "session", (AK_SECURE_COOKIE = "secure_cookie", (AK_STATE = "state", AK_STATUS = "status")))))))))))))))))));
        var gh = [n, AK_AUTH_TOKEN, AK_SCOPE, AK_EXPIRES_IN, AK_EXPIRES], Ye = "connected", ah = "notConnected", rh = "unchecked", yh = "unknown", ef = "expiring", xh = "expired", bh = "live-sdk-upload", Kg = "live-sdk-download", kf = "appId", kh = "channelUrl", vh = "wl_auth", lh = "wl_upload", th = "page", mh = "touch", sh = "none", Mc = "none", Tg = "auth.login", Lg = "auth.logout", mc = "auth.sessionChange", Nf = "auth.statusChange", jf = "wl.log", Cc = "access_denied", Md = "connection_failed", zg = "invalid_cookie", ae = "invalid_request", Nc = "request_canceled", I = "request_failed", Qe = "timed_out", Bg = "unknown_user", ng = "user_canceled", Jf = "METHOD: Failed to get the required user permission to perform this operation.", Fd = "The request could not be completed due to browser issues.", kc = "The request could not be completed due to browser limitations.", Kc = "METHOD: The operation has been canceled.", Gf = "The 'wl_auth' cookie is not valid.", Bf = "The 'wl_auth' cookie has been modified incorrectly. Ensure that the redirect URI only modifies sub-keys for values received from the OAuth endpoint.", uf = "The 'wl_auth' cookie has multiple values. Ensure that the redirect URI specifies a cookie domain and path when setting cookies.", Sd = "METHOD: The input property 'PARAM' does not reference a valid DOM element.", he = "METHOD: An exception was received for EVENT. Detail: MESSAGE", Td = "METHOD: The WL object must be initialized with WL.init() prior to invoking this method.", pc = "A connection to the server could not be established.", yf = "The user could not be identified.", Rf = "METHOD: Failed to get upload_location of the resource.", Mf = "The pending login request has been canceled.", wf = "Logging out the user is not supported in current session because the user is logged in with a Microsoft account on this computer. To logout, the user may quit the app or log out from the computer.", lc = "METHOD: The input value for parameter/property 'PARAM' is not valid.", Gd = "METHOD: The input parameter/property 'PARAM' must be included.", td = "METHOD: The type of the provided value for the input parameter/property 'PARAM' is not valid.", rb = "METHOD: There is a pending METHOD request, the current call will be ignored.", sf = rb.replace(/METHOD/g, Ie), jd = rb.replace(/METHOD/g, J), tf = rb.replace(/METHOD/g, De), xf = "METHOD: The input property 'redirect_uri' is required if the value of the 'response_type' property is 'code'.", rf = "WL.init: The redirect_uri value should be the same as the value of 'Redirect Domain' of your registered app. It must begin with 'http://' or 'https://'.", vf = "METHOD: The api call is not supported on this platform.", qf = "WL.init: The response_type value 'code' is not supported on this platform.", Ag = "METHOD: The input property 'redirect_uri' must use https: to match the scheme of the current page.", Lf = "The auth request is timed out.", Sf = "The popup is closed without receiving consent.", Gb = 0, Dd = 1, nc = 2, ue = 3, Lb = "GET", Oc = "POST", dh = "PUT", Dg = "DELETE", Ug = "COPY", Vg = "MOVE", J = "WL.fileDialog", Wc = "WL.api", pg = "WL.download", Rc = "WL.init", Ie = "WL.login", mb = "WL.ui", De = "WL.upload", Of = 30000, i = "METHOD", S = "PARAM", s = "onSuccess", t = "onError", A = "onProgress", nh = "redirect_type", Eg = "auth", eg = "upload", Fg = "code", rg = "token", nb = "https:", bd = "http:", af = "wl.signin", Te = "wl.skydrive", ee = "wl.skydrive_update", Og = /\s|,/, Sb = "boolean", gd = "dom", c = "function", cd = "number", E = "object", ib = "properties", b = "string", xe = "string_or_array", lb = "undefined", lf = "url", Xe = "name", Ib = "element", ih = "brand", ph = "type", Zf = "sign_in_text", Tf = "sign_out_text", C = "theme", sg = "onloggedin", hg = "onloggedout", Jb = "onerror", Gg = "messenger", Xg = "hotmail", Pg = "skydrive", Yg = "windows", gg = "windowslive", oh = "none", fd = "signin", bg = fd, ig = "login", Uf = "connect", ag = "custom", db = "blue", cb = "white", fh = "id", jb = "onedrive_api", Ve = "auth_server", K = "apiservice_uri", M = "skydrive_uri", x = "sdk_root", mf = "wl_trace", g = {
            subscribe: function (a, b) {
                pb("Subscribe " + a);
                var c = g.getHandlers(a);
                c.push(b)
            }, unsubscribe: function (c, e) {
                pb("Unsubscribe " + c);
                var b = g.getHandlers(c), d = [];
                if (e != null) {
                    var f = false;
                    for (var a = 0; a < b.length; a++)if (f || b[a] != e)d.push(b[a]); else f = true
                }
                g._eHandlers[c] = d
            }, getHandlers: function (b) {
                if (!g._eHandlers)g._eHandlers = {};
                var a = g._eHandlers[b];
                if (a == null)g._eHandlers[b] = a = [];
                return a
            }, notify: function (c, d) {
                pb("Notify " + c);
                var b = g.getHandlers(c);
                for (var a = 0; a < b.length; a++)b[a](d)
            }
        }, a = {_status: Qc, _statusRequests: [], _rpsAuth: false};
        a.appInit = function (b) {
            var f = b[d] || Rc;
            if (a._status == yb) {
                var g = a._session.getNormalStatus();
                return vc(f, true, b.callback, g)
            }
            var c = WL[x];
            if (c) {
                if (c.charAt(c.length - 1) !== "/")c += "/";
                a[x] = c
            }
            var e = b[Ic];
            if (e === false)a._logEnabled = e;
            if (a.testInit)a.testInit(b);
            a._status = yb;
            return Jd(b)
        };
        a.onloadInit = function () {
            Pe();
            Se()
        };
        function Kb(b) {
            if (a._status === Qc)throw new Error(Td.replace(i, b))
        }

        function hf() {
            return WL.Internal.tApp || a
        }

        a.api = function (a) {
            Kb(Wc);
            var c = a[Uc];
            if (c) {
                a = h(Ob(c), a);
                delete a[Uc]
            }
            var b = a[Hb];
            a[Hb] = (b != null ? Vb(b) : Lb).toUpperCase();
            return (new dd(a)).execute()
        };
        a.getAccessTokenForApi = function () {
            var c = null;
            if (!a._rpsAuth) {
                var b = hf()._session.getStatus();
                if (b.status === ef || b.status === Ye)c = b.session[n]
            }
            return c
        };
        var je = function () {
            var b = a.api.lastId, c;
            b = b === undefined ? 1 : b + 1;
            c = "WLAPI_REQ_" + b + "_" + (new Date).getTime();
            a.api.lastId = b;
            return c
        }, dd = function (b) {
            var c = this;
            c._properties = b;
            c._completed = false;
            c._id = je();
            b[Ge] = false;
            b[Yd] = a._isHttps;
            b[H] = a[H];
            var d = b[Mb], e = b[Fc];
            c._url = Qb(d) ? d : He(e) + (d.charAt(0) === "/" ? d.substring(1) : d);
            c._promise = new q(Wc, null, null)
        };
        dd.prototype = {
            execute: function () {
                Ce(this);
                return this._promise
            }, onCompleted: function (a) {
                if (this._completed)return;
                this._completed = true;
                O(this._properties.callback, a, true);
                if (a[AK_ERROR])this._promise[t](a); else this._promise[s](a)
            }
        };
        function Hc(f, c, b, e, d) {
            b = b ? Vb(b) : "";
            var a = b !== "" ? Ke(b) : null;
            if (a === null) {
                a = {};
                if (Math.floor(c / 100) !== 2)a[Pc] = ze(c, e)
            }
            if (d)d.forEach(function (b) {
                a[b.name] = b.value
            });
            a[zb] = c;
            f.onCompleted(a)
        }

        function ze(c, b) {
            var a = {};
            a[kb] = I;
            a[hb] = b || pc;
            return a
        }

        function Ob(i) {
            var c = {};
            for (var b in i) {
                var a = i[b], j = typeof a;
                if (a instanceof Array)for (var d = 0; d < a.length; d++) {
                    var f = a[d], l = typeof f;
                    if (j == E && !(a instanceof Date)) {
                        var h = Ob(f);
                        for (var e in h)c[b + "." + d + "." + e] = h[e]
                    } else c[b + "." + d] = f
                } else if (j == E && !(a instanceof Date)) {
                    var k = Ob(a);
                    for (var g in k)c[b + "." + g] = k[g]
                } else c[b] = a
            }
            return c
        }

        function Ud(c, b) {
            p(b.message);
            var a = c[Jb];
            if (a)D(function () {
                error = xb(mb, mb, b), a(error)
            })
        }

        function Vc() {
            return a[x]
        }

        function Ze() {
            return Vc() + "images"
        }

        var fg = new RegExp("^\\w+\\.\\w+:\\w+[\\|\\w+]+:([\\w]+\\!\\d+)(?:\\!(.+))*");

        function Vb(a) {
            return a.replace(/^\s+|\s+$/g, "")
        }

        function ic(a, b) {
            return a && b ? a.toLowerCase() === b.toLowerCase() : a === b
        }

        function Db(a) {
            return a == null || a === ""
        }

        function Rb() {
            var a = arguments, c = a[0];

            function b(b) {
                return a[parseInt(b.replace(/[\{\}]/g, "")) + 1] || ""
            }

            return c.replace(/\{\d+\}/g, b)
        }

        function Ub(b) {
            var a = {c: 0, s: -1};
            return b.replace(/[^\w .,-]/g, function (d, b, c) {
                if (te(c, b, a))return ["&#", a.c, ";"].join("");
                return ""
            })
        }

        function te(f, b, a) {
            var d = a.s === b;
            if (!d) {
                b = b || 0;
                var c = f.charCodeAt(b), g, e;
                a.s = -1;
                if (c < 55296 || c > 57343)a.c = c; else if (c <= 56319) {
                    g = c;
                    e = f.charCodeAt(b + 1);
                    a.c = (g - 55296) * 1024 + (e - 56320) + 65536;
                    a.s = b + 1
                } else {
                    a.c = -1;
                    d = true
                }
            }
            return !d
        }

        function h(b, d) {
            var c = d || {};
            if (b != null)for (var a in b)c[a] = b[a];
            return c
        }

        function ye(e, d, b) {
            var c = h(e, d);
            for (var a = 0; a < b.length; a++)delete c[b[a]];
            return c
        }

        function ed(a) {
            return Array.prototype.slice.call(a)
        }

        function w(b, a) {
            return function () {
                if (typeof a === c)return a.apply(b, arguments)
            }
        }

        function hd(a, g, d) {
            d = d || "[WL]";
            a = d + a;
            var c = window;
            if (c.debugLogger)c.debugLogger.push(a);
            var b = c.console;
            if (b && b.log)switch (g) {
                case "warning":
                    b.warn(a);
                    break;
                case "error":
                    b.error(a);
                    break;
                default:
                    b.log(a)
            }
            var f = c.opera;
            if (f)f.postError(a);
            var e = c.debugService;
            if (e)e.trace(a)
        }

        function Qb(a) {
            return a.indexOf("https://") === 0 || a.indexOf("http://") === 0
        }

        function pb(b) {
            if (a._traceEnabled)hd(b)
        }

        function qb(b, d, c) {
            if (a._logEnabled || a._traceEnabled)hd(b, d, c);
            g.notify(jf, b)
        }

        if (window.WL && WL.Internal) {
            WL.Internal.trace = pb;
            WL.Internal.log = qb
        }
        function of(b, a) {
            qb(b, "text", a)
        }

        function p(b, a) {
            qb(b, "error", a)
        }

        function Bc(b) {
            var a = document.createElement(b);
            a.style.position = "absolute";
            a.style.top = "-1000px";
            a.style.width = "300px";
            a.style.height = "300px";
            a.style.visibility = "hidden";
            return a
        }

        function Zd() {
            var a = null;
            while (a == null) {
                a = "wl_" + Math.floor(Math.random() * 1024 * 1024);
                if (N(a) != null)a = null
            }
            return a
        }

        function N(a) {
            return document.getElementById(a)
        }

        function pf(a) {
            h(Wb(a), this)
        }

        pf.prototype = {
            toString: function () {
                var a = this, b = (a.scheme != "" ? a.scheme + "//" : "") + a.host + (a.port != "" ? ":" + a.port : "") + a.path;
                return b
            }, resolve: function () {
                var a = this;
                if (a.scheme == "") {
                    var b = window.location.port, c = window.location.host;
                    a.scheme = window.location.protocol;
                    a.host = c.split(":")[0];
                    a.port = b != null ? b : "";
                    if (a.path.charAt(0) != "/")a.path = me(a.host, window.location.href, a.path)
                }
            }
        };
        function Wb(c) {
            var e = c.indexOf(nb) == 0 ? nb : c.indexOf(bd) == 0 ? bd : "", l = "", m = "", f;
            if (e != "")var a = c.substring(e.length + 2), h = gb(a.indexOf("/")), j = gb(a.indexOf("\\")), i = gb(a.indexOf("?")), k = gb(a.indexOf("#")), b = re(Math.min(h, j, i, k)), g = b > 0 ? a.substring(0, b) : a, d = g.split(":"), l = d[0], m = d.length > 1 ? d[1] : "", f = b > 0 ? a.substring(b) : ""; else f = c;
            return {scheme: e, host: l, port: m, path: f}
        }

        function gb(a) {
            if (a == -1)return Number.POSITIVE_INFINITY;
            return a
        }

        function re(a) {
            if (a == Number.POSITIVE_INFINITY)return -1;
            return a
        }

        function We(a) {
            return Wb(a.toLowerCase()).host
        }

        function me(e, b, h) {
            var d = function (a, c) {
                charIdx = b.indexOf(c);
                a = charIdx > 0 ? a.substring(0, charIdx) : a;
                return a
            };
            b = d(d(b, "?"), "#");
            var f = b.indexOf(e), a = b.substring(f + e.length), g = a.indexOf("/"), c = a.lastIndexOf("/");
            a = c >= 0 ? a.substring(g, c) : a;
            return a + "/" + h
        }

        function cf(a) {
            var b = a.indexOf("?");
            if (b > 0)a = a.substring(0, b);
            b = a.indexOf("#");
            if (b > 0)a = a.substring(0, b);
            return a
        }

        function we(c) {
            var a = cf(c), b = a.lastIndexOf("/") + 1;
            return a.substr(b)
        }

        function k(a, b) {
            if (typeof a == c)b !== undefined ? a(b) : a()
        }

        function O(a, b, e, d) {
            if (typeof a == c) {
                if (d)b[AK_STATE] = d;
                if (e)a(b); else D(function () {
                    a(b)
                })
            }
        }

        function Ke(a) {
            return JSON.parse(a)
        }

        function nf(b, c) {
            var d = b.length;
            for (var a = 0; a < d; a++)c(b[a])
        }

        function v(d, c) {
            var a = {}, b = {};
            a[kb] = d, a[hb] = c;
            b[Pc] = a;
            return b
        }

        function xb(a, b, c) {
            return v(I, he.replace("METHOD", a).replace("EVENT", b).replace("MESSAGE", c.message))
        }

        function Xd(b) {
            var a = b.split(".");
            return a[0] + "." + a[1]
        }

        function D(a, b) {
            window.wlUnitTests ? wlUnitTests.delayInvoke(a) : window.setTimeout(a, b || 1)
        }

        function Pe() {
            var b = Re(navigator.userAgent, document.documentMode), c = a[H];
            a._browser = b;
            a[H] = c.replace("DEVICE", b.device)
        }

        function Re(a, e) {
            a = a.toLowerCase();
            var c = "other", b = {
                "firefox": /firefox/.test(a),
                "firefox1.5": /firefox\/1\.5/.test(a),
                "firefox2": /firefox\/2/.test(a),
                "firefox3": /firefox\/3/.test(a),
                "firefox4": /firefox\/4/.test(a),
                "ie": (/msie/.test(a) || /trident/.test(a)) && !/opera/.test(a),
                "ie6": false,
                "ie7": false,
                "ie8": false,
                "ie9": false,
                "ie10": false,
                "ie11": false,
                "opera": /opera/.test(a),
                "webkit": /webkit/.test(a),
                "chrome": /chrome/.test(a),
                "mobile": /mobile/.test(a) || /phone/.test(a)
            };
            if (b["ie"]) {
                var d = 0;
                if (e)d = e; else if (/msie 7/.test(a))d = 7;
                d = Math.min(11, Math.max(d, 6));
                c = "ie" + d;
                b[c] = true
            } else if (b.firefox)c = "firefox"; else if (b.chrome)c = "chrome"; else if (b.webkit)c = "webkit"; else if (b.opera)c = "opera";
            if (b.mobile)c += "mobile";
            b.device = c;
            return b
        }

        function wc(e, c) {
            var f = c != null ? c : {};
            if (e != null) {
                var d = e.split("&");
                for (var b = 0; b < d.length; b++) {
                    var a = d[b].split("=");
                    if (a.length == 2)f[decodeURIComponent(a[0])] = decodeURIComponent(a[1])
                }
            }
            return f
        }

        function ne(a) {
            var c = "";
            if (a != null)for (var b in a)if (a.hasOwnProperty(b)) {
                var d = c.length ? "&" : "", e = a[b];
                c += d + encodeURIComponent(b) + "=" + encodeURIComponent(oe(e))
            }
            return c
        }

        function oe(a) {
            if (a instanceof Date) {
                var b = function (a, b) {
                    switch (b) {
                        case 2:
                            return a < 10 ? "0" + a : a;
                        case 3:
                            return (a < 10 ? "00" : a < 100 ? "0" : "") + a
                    }
                };
                return a.getUTCFullYear() + "-" + b(a.getUTCMonth() + 1, 2) + "-" + b(a.getUTCDate(), 2) + "T" + b(a.getUTCHours(), 2) + ":" + b(a.getUTCMinutes(), 2) + ":" + b(a.getUTCSeconds(), 2) + "." + b(a.getUTCMilliseconds(), 3) + "Z"
            }
            return "" + a
        }

        function Ee(b) {
            var d = b.indexOf("?") + 1, c = b.indexOf("#") + 1, a = {};
            if (d > 0) {
                var e = c > d ? c - 1 : b.length;
                a = wc(b.substring(d, e), a)
            }
            if (c > 0)a = wc(b.substring(c), a);
            return a
        }

        function Ab(a, b) {
            return a + (a.indexOf("?") < 0 ? "?" : "&") + ne(b)
        }

        var hc = {name: B, type: c, optional: true}, Hf = {name: B, type: c, optional: false};

        function Ue(a, c, d) {
            if (a instanceof Array)for (var b = 0; b < a.length; b++)Yc(a[b], c[b], d); else Yc(a, c, d)
        }

        function Yc(c, a, b) {
            Lc(c, a, b);
            if (a.type === ib)m(c, a.properties, b)
        }

        function Lc(g, f, d, h) {
            var e = f.name, a = typeof g, j = f.type;
            if (a === lb || g == null)if (f.optional) {
                h && h();
                return
            } else throw oc(e, d);
            switch (j) {
                case b:
                    if (a != b)throw u(e, d);
                    if (!f.optional && Vb(g) === "")throw oc(e, d);
                    break;
                case lf:
                    if (a != b || !Qb(g))throw u(a, d);
                    break;
                case ib:
                    if (a != E)throw u(e, d);
                    break;
                case c:
                    if (a != c)throw u(a, d);
                    break;
                case gd:
                    if (a == b) {
                        if (N(g) == null)throw new Error(Sd.replace(i, d).replace(S, e))
                    } else if (a != E)throw u(e, d);
                    break;
                case xe:
                    if (a != b && !(g instanceof Array))throw u(a, d);
                    break;
                default:
                    if (a != j)throw u(e, d)
            }
            if (f.allowedValues != null)ke(g, f.allowedValues, f.caseSensitive, e, d)
        }

        function m(g, d, f) {
            var b = g || {};
            for (var c = 0; c < d.length; c++) {
                var a = d[c], e = b[a.name] || b[a.altName];
                Lc(e, a, f, function () {
                    if (typeof a.defaultValue != lb)b[a.name] = a.defaultValue
                })
            }
        }

        function ke(d, a, e, f, h) {
            var g = typeof a[0] === b;
            for (var c = 0; c < a.length; c++)if (g && !e) {
                if (d.toLowerCase() === a[c].toLowerCase())return
            } else if (d === a[c])return;
            throw Ld(f, h)
        }

        function u(a, b) {
            return new Error(td.replace(i, b).replace(S, a))
        }

        function oc(a, b) {
            return new Error(Gd.replace(i, b).replace(S, a))
        }

        function Ld(c, d, a) {
            var b = lc.replace(i, d).replace(S, c);
            if (typeof a !== lb)b += " " + a;
            return new Error(b)
        }

        function Fb(k, j) {
            var g = ed(k), a = null, b = null;
            for (var f = 0; f < g.length; f++) {
                var e = g[f], i = typeof e;
                if (i === E && a === null)a = h(e); else if (i === c && b === null)b = e
            }
            a = a || {};
            if (b)a.callback = b;
            a[d] = j;
            return a
        }

        function Pd(a, b) {
            var c = xb(a, a, b);
            p(b.message);
            return vc(a, false, null, c)
        }

        var q = function (b, c, a) {
            this._name = b;
            this._op = c;
            this._uplinkPromise = a;
            this._isCompleted = false;
            this._listeners = []
        };
        q.prototype = {
            then: function (d, e, c) {
                var b = new q(null, null, this), a = {};
                a[s] = d;
                a[t] = e;
                a[A] = c;
                a.chainedPromise = b;
                this._listeners.push(a);
                return b
            }, cancel: function () {
                if (this._isCompleted)return;
                if (this._uplinkPromise && !this._uplinkPromise._isCompleted)this._uplinkPromise.cancel(); else {
                    var a = this._op ? this._op.cancel : null;
                    if (typeof a === c)this._op.cancel(); else this.onError(v(Nc, Kc.replace("METHOD", this._getName())))
                }
            }, _getName: function () {
                if (this._name)return this._name;
                if (this._op && typeof this._op._getName === c)return this._op._getName();
                if (this._uplinkPromise)return this._uplinkPromise._getName();
                return ""
            }, _onEvent: function (b, a) {
                if (this._isCompleted)return;
                this._isCompleted = a !== A;
                this._notify(b, a)
            }, _notify: function (b, a) {
                var c = this;
                nf(this._listeners, function (g) {
                    var h = g[a], d = g.chainedPromise, f = a !== A;
                    if (h)try {
                        var e = h.apply(g, b);
                        if (f && e && e.then) {
                            d._op = e;
                            e.then(function (a) {
                                d[s](a)
                            }, function (a) {
                                d[t](a)
                            }, function (a) {
                                d[A](a)
                            })
                        }
                    } catch (i) {
                        if (f)d.onError(xb(c._getName(), a, i))
                    } else if (f)d[a].apply(d, b)
                })
            }
        };
        q.prototype[s] = function () {
            this._onEvent(arguments, s)
        };
        q.prototype[t] = function () {
            this._onEvent(arguments, t)
        };
        q.prototype[A] = function () {
            this._onEvent(arguments, A)
        };
        function vc(f, e, b, g) {
            var a = new q(f, null, null), d = e ? s : t;
            if (typeof b === c)a.then(function (a) {
                b(a)
            });
            D(function () {
                a[d](g)
            });
            return a
        }

        var gh = [n, AK_AUTH_TOKEN, AK_SCOPE, AK_EXPIRES_IN, AK_EXPIRES, AK_REQUEST_TS, AK_ERROR, AK_ERROR_DESC], Zg = "refresh_type", jg = "app", tg = "ms", ug = "response_method", Pf = "url", Ef = "cookie", eb = "onanalytics", xg = "login", If = "loginstatus", Kd = "file_dialog", og = "auth.response", Jc = "client-id", sd = ".OneDriveSaveButton", Ah = "file", L = "click", Tb = "onedrive-js", tc = "2px", gc = "0px", Dc = "4px", ud = "METHOD: Failed to initialize due to missing 'client-id'.", Ad = "Loading OneDrive picker is timed out.", j = "mode", z = "open", X = "save", G = "read", F = "readwrite", Z = "linkType", dc = "resourceType", V = "file", U = "folder", r = "select", y = "single", ub = "multi", wd = "permission", od = "onetime", Y = "lightbox", rd = "grey", Xb = "transparent", tb = "white", bc = "loading_timeout", fc = "onselected", bb = "cancel", zc = "error", Ec = "file", qc = "fileName", Qd = "internal_app", rc = Z, sb = "downloadLink", W = "webViewLink", sc = "progress", xc = "multiSelect", ab = "success", Ac = "theme", wb = "blue", zd = "white", Xc = "from_url", Tc = "form", ld = "METHOD: This upload method is not implemented.", se = "{0}: Operation: '{1}' Error message: {2}", Kf = "API call", ec = "invoke picker", T = "unhandled exception", vb = "upload", Zb = "upload: poll for completion", ce = "OneDrive.open", de = "OneDrive.save", ac = "OneDrive.createOpenButton", pd = "OneDrive.createSaveButton", id = "OneDriveApp.onloadInit", Cf = "OneDriveApp.initialize", be = "auth", Bd = "rps", vd = "oauth", Cd = "v", kd = "1", md = "2", Nd = "domain", Hd = "livesdk", ie = "mkt";
        FILEDIALOG_PARAM_PICKER_SCRIPT = "pickerscript";
        FILEDIALOG_CHCMD_ONCOMPLETE = "onPickerComplete", FILEDIALOG_CHCMD_UPDATETOKEN = "updateToken";
        var gf = 27, Je = 1000, Sc = 5;
        ONEDRIVE_PREFIX = "[OneDrive]", (UI_SKYDRIVEPICKER = "skydrivepicker", VROOM_THUMBNAIL_SIZES = ["large", "medium", "small"]);
        window.WL = {};
        WL.Internal = {};
        WL.init = function (d) {
            try {
                var c = h(d);
                Ue(c, {
                    name: ib,
                    type: ib,
                    optional: false,
                    properties: [{name: AK_CLIENT_ID, altName: kf, type: b, optional: false}, {
                        name: Ic,
                        type: Sb,
                        optional: true
                    }]
                }, Rc);
                a.appInit(c)
            } catch (e) {
                p(e.message)
            }
        };
        WL.ui = function () {
            try {
                var c = Fb(arguments);
                m(c, [{name: Xe, type: b, allowedValues: [UI_SKYDRIVEPICKER], optional: false}, hc], mb);
                a.ui(c)
            } catch (f) {
                Ud(c, f)
            }
        };
        WL.fileDialog = function () {
            try {
                var b = J, c = Fb(arguments, b);
                uc(c, b);
                return a.fileDialog(c)
            } catch (f) {
                return Pd(b, f)
            }
        };
        function uc(e, d) {
            m(e, [{name: j, type: b, allowedValues: [z, X, G, F], optional: false}, {
                name: dc,
                type: b,
                allowedValues: [V, U],
                optional: true
            }, {name: r, type: b, allowedValues: [y, ub], optional: true}, {
                name: Y,
                type: b,
                allowedValues: [rd, Xb, tb],
                optional: true
            }, {name: bc, type: cd, optional: true}, {name: wd, type: b, allowedValues: [od], optional: true}, hc], d);
            if (d !== J)m(e, [{name: C, allowedValues: [db, cb], type: b, optional: true}, {
                name: fc,
                type: c,
                optional: false
            }, {name: Jb, type: c, optional: true}], d);
            if (!Nb.isSupported() || !window.JSON || a._browser.mobile)throw new Error(kc)
        }

        function Jd(b) {
            a._rpsAuth = true;
            a._appId = b[AK_CLIENT_ID]
        }

        function Se() {
            a._logEnabled = true;
            a._urlParams = Ee(window.location.href);
            a._traceEnabled = a._urlParams[mf];
            var b = window.wlAsyncInit;
            if (b && typeof b === c)b.call()
        }

        function Ce(a) {
            if (!a._properties[Fc] && Wd(a))return;
            if (sendAPIRequestViaXHR(a))return;
            if (Vd(a))return;
            var b = {};
            b[kb] = I;
            b[hb] = kc;
            a.onCompleted(b)
        }

        function He(b) {
            return b ? a[jb] : a[K]
        }

        a.jsonp = {};
        WL.Internal.jsonp = a.jsonp;
        function Wd(c) {
            var h = document.getElementsByTagName("HEAD")[0], b = document.createElement("SCRIPT"), d = ye(c._properties, d, [B, Mb]), e = c._id, f = a.getAccessTokenForApi();
            if (f != null)d[n] = f;
            d[B] = nd + e;
            d[Rd] = "true";
            var g = Ab(c._url, d);
            if (g.length > le)return false;
            c.scriptTag = b;
            a.jsonp[e] = function (a) {
                Bb(e, b);
                c.onCompleted(a)
            };
            qe(b, c);
            b.setAttribute("async", "async");
            b.type = "text/javascript";
            b.src = g;
            h.appendChild(b);
            window.setTimeout(function () {
                if (c._completed)return;
                Bb(e, b)
            }, 30000);
            return true
        }

        function qe(b, c) {
            if (a._browser.ie && b.attachEvent)b.attachEvent("onreadystatechange", function (a) {
                Pb(a, c)
            }); else {
                b.readyState = "complete";
                b.addEventListener("load", function (a) {
                    Pb(a, c)
                }, false);
                b.addEventListener("error", function (a) {
                    Pb(a, c)
                }, false)
            }
        }

        function Pb(d, c) {
            if (c._completed)return;
            var b = d.srcElement || d.currentTarget;
            if (!b.readyState)b = d.currentTarget;
            if (b.readyState != "complete" && b.readyState != "loaded")return;
            var f = c._id;
            failure = d.type == "error" || a.jsonp[f] != null;
            if (failure) {
                Bb(f, c.scriptTag);
                var e = {};
                e[kb] = Md;
                e[hb] = pc;
                c.onCompleted({error: e})
            }
        }

        function Bb(b, c) {
            delete a.jsonp[b];
            document.getElementsByTagName("HEAD")[0].removeChild(c)
        }

        function Vd(b) {
            ff();
            if (a._browser.flashVersion < 9)return false;
            a.xdrFlash.send(b);
            return true
        }

        a.xdrFlash = {
            _id: "", _status: Gb, _flashObject: null, _requests: {}, _pending: [], init: function () {
                if (this._status != Gb)return;
                this._status = Dd;
                var c = Bc("div");
                c.id = "wl_xdr_container";
                document.body.appendChild(c);
                this._id = Zd();
                var b = Od, d = a[x] + "XDR.swf";
                b = b.replace(/{url}/g, d);
                b = b.replace(/{id}/g, this._id);
                b = b.replace(/{variables}/g, "domain=" + document.domain);
                c.innerHTML = b
            }, onReady: function (b) {
                if (b) {
                    if (a._browser.firefox)this._flashObject = document.embeds[this._id]; else this._flashObject = N(this._id);
                    this._status = nc
                } else this._status = ue;
                while (this._pending.length > 0)this.send(this._pending.shift())
            }, onRequestCompleted: function (b, e, c, f) {
                var d = a.xdrFlash._requests[b];
                delete a.xdrFlash._requests[b];
                Hc(d, e, Le(c), f)
            }, send: function (a) {
                if (this._status < nc) {
                    this._pending.push(a);
                    if (this._status == Gb)fb(w(this, this.init));
                    return
                }
                if (this._flashObject != null) {
                    this._requests[a._id] = a;
                    var b = prepareXDRRequest(a);
                    this.invoke("send", [a._id, b.url, b.method, b.body])
                } else Hc(a, 0, null, Fd)
            }, invoke: function (d, a) {
                a = a || [];
                var c = window.__flash__argumentsToXML(a, 0), e = '<invoke name="' + d + '" returntype="javascript">' + c + "</invoke>", b = this._flashObject.CallFunction(e);
                if (b == null)return null;
                return eval(b)
            }
        };
        WL.Internal.xdrFlash = a.xdrFlash;
        function Le(a) {
            return a ? a.replace(/\r/g, " ").replace(/\n/g, " ") : a
        }

        var Od = "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0' width='300' height='300' id='{id}' name='{id}' type='application/x-shockwave-flash' data='{url}'>" + "<param name='movie' value='{url}'></param>" + "<param name='allowScriptAccess' value='always'></param>" + "<param name='FlashVars' value='{variables}'></param>" + "<embed play='true' menu='false' swLiveConnect='true' allowScriptAccess='always' type='application/x-shockwave-flash' FlashVars='{variables}' src='{url}' width='300' height='300' name='{id}'></embed>" + "</object>", Cb = null;
        (function () {
            a.fileDialog = function (b) {
                Kb(b[d]);
                if (a._pendingPickerOp != null)throw new Error(jd);
                return (new Cb(b)).execute()
            };
            var h = 0, b = 1, f = 2, c = 3;
            Cb = function (c) {
                var b = this;
                b._props = c;
                b._startTs = (new Date).getTime();
                c[Y] = c[Y] || tb;
                c[r] = c[r] || y;
                b._state = h;
                b._authDelegate = w(b, b._onAuthResp);
                a._pendingPickerOp = b
            };
            Cb.prototype = {
                execute: function () {
                    var a = this, b = new q(a._props[d], a, null);
                    a._promise = b;
                    a._process();
                    return b
                }, cancel: function (a) {
                    var b = this;
                    if (b._state === c)return;
                    if (!a)a = v(Nc, Kc.replace(i, b._props[d]));
                    b._onComplete(a)
                }, _process: function () {
                    var a = this;
                    switch (a._state) {
                        case h:
                            a._validateAuth();
                            break;
                        case b:
                            a._initPicker();
                            break;
                        case f:
                            a._complete()
                    }
                }, _changeState: function (d, b) {
                    var a = this;
                    if (a._state !== c && a._state !== d) {
                        a._state = d;
                        if (b)a._result = b;
                        a._process()
                    }
                }, _onComplete: function (a) {
                    this._changeState(f, a)
                }, _validateAuth: function () {
                    var c = this, f = false;
                    if (a._rpsAuth)c._changeState(b); else {
                        var e;
                        switch (c._props[j]) {
                            case z:
                                e = Te;
                                break;
                            case X:
                                e = ee;
                                break;
                            case G:
                            case F:
                                e = c._buildExternalScope();
                                f = true;
                                break;
                            default:
                                var k = lc.replace(i, J).replace(S, j);
                                c._onComplete(v(ae, k));
                                return
                        }
                        var h = c._props[d], g = 650;
                        if (f)a.login({scope: e, external_consent: true}).then(function (a) {
                            c._onExternalConsentComplete(a)
                        }, function (a) {
                            c._onComplete(a)
                        }); else {
                            e += " " + af;
                            a.ensurePermission(e, g, h, c._authDelegate)
                        }
                    }
                }, _buildExternalScope: function () {
                    var b = this, a = "onedrive_onetime.access:";
                    switch (b._props[j]) {
                        case G:
                            a += G;
                            break;
                        case F:
                            a += F
                    }
                    switch (b._props[dc]) {
                        case V:
                            a += "file|";
                            break;
                        case U:
                            a += "folder|";
                            break;
                        default:
                            a += "file|"
                    }
                    a += b._props[r] === y ? y : ub;
                    if (b._props[Z])a += "|" + b._props[Z];
                    return a
                }, _onAuthResp: function (c) {
                    var a = this;
                    if (c.error) {
                        if (!a._channel)a._onComplete(c)
                    } else {
                        var d = c.session[n];
                        a._props[n] = d;
                        if (a._channel)a._channel.send(FILEDIALOG_CHCMD_UPDATETOKEN, d); else a._changeState(b)
                    }
                }, _initPicker: function () {
                    var b = this, c = b._props;
                    m(c);
                    b._channel = a.channel.registerOuterChannel(Kd, Wb(c.url).host, c.frame.contentWindow, c.url, w(b, b._onMessage));
                    g.subscribe(mc, b._authDelegate);
                    var d = function (a) {
                        if (a.keyCode === gf)b.cancel()
                    };
                    c.keydownHandler = d;
                    e(window, "keydown", d);
                    b._initTimeout()
                }, _initTimeout: function () {
                    var a = this;
                    timeoutSeconds = a._props[bc];
                    if (timeoutSeconds && timeoutSeconds > 0)a._timeout = window.setTimeout(w(a, a._onTimeout), timeoutSeconds * 1000)
                }, _onTimeout: function () {
                    var a = this, b = a._channel._connected;
                    if (!b)a.cancel(v(Qe, Ad));
                    a._clearTimeout()
                }, _clearTimeout: function () {
                    var a = this;
                    if (a._timeout) {
                        window.clearTimeout(a._timeout);
                        a._timeout = undefined
                    }
                }, _complete: function () {
                    var b = this, e = b._result, f = e.error ? t : s;
                    b._state = c;
                    b._cleanup();
                    b._normalizeResp();
                    if (b._props[d] === J)O(b._props[B], e, true); else if (e.data)O(b._props[fc], e, true); else O(b._props[Jb], e, true);
                    b._promise[f](e);
                    if (!a._rpsAuth)D(function () {
                        b._report()
                    })
                }, _report: function () {
                    var f = this._props, b = this._result, l = ((new Date).getTime() - this._startTs) / 1000, i = "none", c = 0, e = 0;
                    if (b.data) {
                        if (b.data.folders != null)c = b.data.folders.length;
                        if (b.data.files != null)e = b.data.files.length
                    }
                    i = c > 0 && e > 0 ? "both" : c > 0 ? U : e > 0 ? V : "none";
                    var k = c + e, h = {
                        client: UI_SKYDRIVEPICKER,
                        api: f[d],
                        mode: f[j],
                        select: f[r],
                        result: b.error ? b.error.code : "success",
                        duration: l,
                        object_selected: i,
                        selected_count: k
                    }, g = a[eb];
                    a.api({path: "web_analytics", method: Oc, body: h});
                    if (g)g(h)
                }, _onExternalConsentComplete: function (b) {
                    var c = this, e = c._props[Z] === W;
                    if (b.error) {
                        c._onComplete(b);
                        return
                    }
                    var i = b[AK_OWNER_CID], f = b[AK_RESOURCEID], j = b[AK_ITEMID], g = b[df];
                    if (!f) {
                        c._onComplete(v(I, "Did not get expected resource id."));
                        return
                    }
                    if (!g && e) {
                        c._onComplete(v(I, "Did not get expected auth key for the resource."));
                        return
                    }
                    var h = {
                        path: e ? "drives/" + i + "/items/" + j + "?$expand=thumbnails,children($expand=thumbnails)&authkey=" + g : f + "/files",
                        method: Lb,
                        use_vroom_api: e,
                        interface_method: c._props[d]
                    };
                    a.api(h).then(function (d) {
                        var a = {pickerResponse: b, apiResponse: d};
                        c._onComplete(a)
                    }, function (a) {
                        c._onComplete(a)
                    })
                }, _onMessage: function (a, b) {
                    qb(a);
                    switch (a) {
                        case FILEDIALOG_CHCMD_ONCOMPLETE:
                            this._onComplete(b)
                    }
                }, _normalizeResp: function () {
                    var f = this, b = f._result.data, e = f._result.error, g = function (c) {
                        var b = c.upload_location;
                        if (b)c.upload_location = b.replace("WL_APISERVICE_URL", a[K])
                    };
                    if (b) {
                        if (b.folders)for (var c = 0; c < b.folders.length; c++)g(b.folders[c]);
                        if (b.files)for (var c = 0; c < b.files.length; c++)g(b.files[c])
                    }
                    if (e && e.message)e.message = e.message.replace(i, f._props[d])
                }, _cleanup: function () {
                    var c = this, b = c._props, f = c._channel, e = b.resizeHandler, d = b.keydownHandler;
                    c._clearTimeout();
                    g.unsubscribe(mc, c._authDelegate);
                    if (b.lightBox) {
                        b.frame.style.display = Mc;
                        b.lightBox.style.display = Mc;
                        document.body.removeChild(b.frame);
                        document.body.removeChild(b.lightBox);
                        delete b.lightBox;
                        delete b.frame
                    }
                    if (f) {
                        f.dispose();
                        delete c._channel
                    }
                    if (e)o(window, "resize", e);
                    if (d)o(window, "keydown", d);
                    delete a._pendingPickerOp
                }
            };
            function m(c) {
                var g = c[j] === z, f = g ? 800 : 500, d = g ? 600 : 450, h = c[Y], p = h === Xb, m = p ? 0 : 60, q = h === tb ? "white" : "rgb(0,0,0)", n = m / 100, i = k(f, d), o = l(c), b = document.createElement("div");
                b.style.top = "0px";
                b.style.left = "0px";
                b.style.position = "fixed";
                b.style.width = "100%";
                b.style.height = "100%";
                b.style.display = "block";
                b.style.backgroundColor = q;
                b.style.opacity = n;
                b.style.MozOpacity = n;
                b.style.filter = "alpha(opacity=" + m + ")";
                b.style.zIndex = "2600000";
                var a = document.createElement("iframe");
                a.id = "picker" + (new Date).getTime();
                a.style.top = i.top;
                a.style.left = i.left;
                a.style.position = "fixed";
                a.style.width = f + "px";
                a.style.height = d + "px";
                a.style.display = "block";
                a.style.zIndex = "2600001";
                a.style.borderWidth = "1px";
                a.style.borderColor = "gray";
                a.style.maxHeight = "100%";
                a.style.maxWidth = "100%";
                a.src = o;
                a.setAttribute("sutra", "picker");
                document.body.appendChild(a);
                document.body.appendChild(b);
                c.resizeHandler = function () {
                    var b = k(f, d);
                    a.style.top = b.top;
                    a.style.left = b.left
                };
                e(window, "resize", c.resizeHandler);
                c.lightBox = b;
                c.frame = a;
                c.url = o
            }

            function l(d) {
                var e = d[j] === z, f = e ? md : kd, b = {}, c = Vc() + a[FILEDIALOG_PARAM_PICKER_SCRIPT];
                if (c.charAt(0) === "/")c = nb + c;
                b[Cd] = f;
                b[be] = a._rpsAuth ? Bd : vd;
                b[Nd] = window.location.hostname.toLowerCase();
                b[Hd] = c;
                b[AK_CLIENT_ID] = a._appId;
                b[AK_REQUEST_TS] = (new Date).getTime();
                b[ie] = a._locale;
                if (!a._rpsAuth)b[n] = d[n];
                if (e)b[r] = d[r];
                return Ab(a[M], b)
            }

            function k(f, e) {
                var b, c;
                if (a._browser.ie) {
                    var d = document.documentElement;
                    b = (d.clientWidth - f) / 2;
                    c = (d.clientHeight - e) / 2
                } else {
                    b = (window.innerWidth - f) / 2;
                    c = (window.innerHeight - e) / 2
                }
                b = b < 10 ? 10 : b;
                c = c < 10 ? 10 : c;
                return {left: b + "px", top: c + "px"}
            }
        })();
        a.ui = function (a) {
            Kb(mb);
            switch (a.name) {
                case fd:
                    new SignInControl(a);
                    break;
                case UI_SKYDRIVEPICKER:
                    new yc(a)
            }
        };
        var yc = function (b) {
            uc(b);
            var a = this;
            a._props = b;
            fb(w(a, a.init))
        };
        yc.prototype = {
            init: function () {
                var a = this;
                if (a._inited === true)return;
                a._inited = true;
                try {
                    var c = a._props, e = c[Ib], f = c[B];
                    c[d] = "WL.ui-" + UI_SKYDRIVEPICKER;
                    a.validate();
                    c[C] = c[C] || cb;
                    e = typeof e === b ? N(c[Ib]) : e;
                    a._element = e;
                    var g = qd(c);
                    bf(e, g);
                    Ed(e, w(a, a.onClick), w(a, a.onRender));
                    a.onRender(false, false);
                    O(f, c, false)
                } catch (h) {
                    p(h.message)
                }
            }, validate: function () {
                var a = this._props;
                m(a, [{name: Ib, type: gd, optional: false}], a[d])
            }, onClick: function () {
                try {
                    return a.fileDialog(this._props)
                } catch (b) {
                    p(b.message)
                }
                return false
            }, onRender: function () {
                cc(this._props, this._element.childNodes[0])
            }
        };
        function cc(k, d) {
            var h = k[C] === cb, j = a._locale, f = j.indexOf("ar") > -1 || j.indexOf("he") > -1, l = f ? "RTL" : "LTR", n = d.childNodes[0], m = d.childNodes[1], e = "#094AB2", i = "#ffffff", b = d.style, g = n.style, c = m.style;
            b.direction = l;
            b.backgroundColor = h ? i : e;
            b.border = "solid 1px";
            b.borderColor = e;
            b.height = "20px";
            b.paddingLeft = Dc;
            b.paddingRight = Dc;
            b.textAlign = "center";
            b.cursor = "pointer";
            if (d instanceof HTMLAnchorElement) {
                b.lineHeight = "20px";
                b.display = "inline-block";
                b.textDecoration = "none"
            }
            g.verticalAlign = "middle";
            g.height = "16px";
            c.fontFamily = "'Segoe UI', 'Segoe UI Web Regular', 'Helvetica Neue', 'BBAlpha Sans', 'S60 Sans', Arial, 'sans-serif'";
            c.fontSize = "12px";
            c.fontWeight = "bold";
            c.color = h ? e : i;
            c.textAlign = "center";
            c.verticalAlign = "middle";
            c.marginLeft = f ? gc : tc;
            c.marginRight = f ? tc : gc
        }

        function qd(b) {
            var a = Yb(b, false);
            return "<button id='" + Ub(a.buttonId) + "' title='" + Ub(a.buttonTitle) + "'>" + a.innerHTML + "</button>"
        }

        function Yb(b, c) {
            var n = b[j], m = b[C], a = n === z, e = a ? ob.skyDriveOpenPickerButtonText : ob.skyDriveSavePickerButtonText, i = m === db ? "SkyDriveIcon_white.png" : "SkyDriveIcon_blue.png", h = "<img alt='' src='" + Ze() + "/SkyDrivePicker/" + i + "'>", g = "<span>" + Ub(e) + "</span>", d = a ? ob.skyDriveOpenPickerButtonTooltip : ob.skyDriveSavePickerButtonTooltip, k = c ? "onedriveopenpickerbutton" : "skydriveopenpickerbutton", l = c ? "onedrivesavepickerbutton" : "skydrivesavepickerbutton", f = a ? k : l;
            return {innerHTML: h + g, buttonTitle: d, buttonId: f}
        }

        function Ed(c, m, l) {
            var a = c.childNodes[0];
            if (m) {
                var g = function (b) {
                    if (c.childNodes.length == 0) {
                        o(a, L, g);
                        return
                    }
                    m(b)
                };
                e(a, L, g)
            }
            if (l) {
                var d = false, f = false, h = function (a) {
                    f = true;
                    b(a)
                }, i = function (a) {
                    f = false;
                    b(a)
                }, j = function (a) {
                    d = true;
                    b(a)
                }, k = function (a) {
                    d = false;
                    b(a)
                }, b = function () {
                    if (c.childNodes.length == 0) {
                        o(a, "mouseenter", h);
                        o(a, "mouseleave", i);
                        o(document, "mousedown", j);
                        o(document, "mouseup", k);
                        return
                    }
                    l(d, f)
                };
                e(a, "mouseenter", h);
                e(a, "mouseleave", i);
                e(document, "mousedown", j);
                e(document, "mouseup", k)
            }
        }

        function ff() {
            if (a._browser.flash !== undefined)return;
            var b = 0;
            try {
                if (a._browser.ie) {
                    var k = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7"), f = k.GetVariable("$version"), i = f.split(" "), g = i[1], d = g.split(",");
                    b = d[0]
                } else if (navigator.plugins && navigator.plugins.length > 0)if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
                    var j = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "", e = navigator.plugins["Shockwave Flash" + j].description, h = e.split(" "), c = h[2].split(".");
                    b = c[0]
                }
            } catch (l) {
            }
            a._browser.flashVersion = b;
            a._browser.flash = b >= 8
        }

        function fb(b) {
            if (document.body)switch (document.readyState) {
                case "complete":
                    b();
                    return;
                case "loaded":
                    if (a._browser.webkit) {
                        b();
                        return
                    }
                    break;
                case "interactive":
                case undefined:
                    if (a._browser.firefox || a._browser.webkit) {
                        b();
                        return
                    }
            }
            if (document.addEventListener) {
                document.addEventListener("DOMContentLoaded", b, false);
                document.addEventListener("load", b, false)
            } else if (window.attachEvent)window.attachEvent("onload", b);
            if (a._browser.ie && document.attachEvent)document.attachEvent("onreadystatechange", function () {
                if (document.readyState === "complete") {
                    document.detachEvent("onreadystatechange", arguments.callee);
                    b()
                }
            })
        }

        function bf(b, a) {
            b.innerHTML = a
        }

        function e(a, b, c) {
            if (a.addEventListener)a.addEventListener(b, c, false); else if (a.attachEvent)a.attachEvent("on" + b, c)
        }

        function o(a, b, c) {
            if (a.removeEventListener)a.removeEventListener(b, c, false); else if (a.detachEvent)a.detachEvent("on" + b, c)
        }

        function ve() {
            var a = N(Tb);
            if (a) {
                var b = a.getAttribute(Jc);
                !b && p(Rb("Could not find attribute '{0}' on element with id '{1}'.", Jc, Tb), ONEDRIVE_PREFIX);
                return b
            } else {
                p(Rb("Could not find element with id '{0}'.", Tb), ONEDRIVE_PREFIX);
                return null
            }
        }

        var Nb = {
            registerOuterChannel: function (e, b, c, d, a) {
                return l.registerChannel(e, b, c, d, a)
            }, registerInnerChannel: function (c, b, a) {
                return l.registerChannel(c, b, null, null, a)
            }, isSupported: function () {
                return l.isSupported()
            }
        }, l = {
            _channels: [], isSupported: function () {
                return window.postMessage != null
            }, registerChannel: function (i, f, g, h, d) {
                var b = l, c = b._channels, a = null;
                if (b.isSupported()) {
                    a = new Gc(i, f, g, h, d);
                    if (c.length === 0)e(window, "message", b._onMessage);
                    c.push(a)
                }
                return a
            }, unregisterChannel: function (d) {
                var c = l, a = c._channels;
                for (var b = 0; b < a.length; b++)if (a[b] == d) {
                    a.splice(b, 1);
                    break
                }
                if (a.length === 0)o(window, "message", c._onMessage)
            }, _onMessage: function (a) {
                var d = l, a = a || window.event, c = Ne(a);
                if (c != null) {
                    var b = d._findChannel(a, c);
                    if (b != null)switch (c.text) {
                        case ad:
                            b._onConnectReq(a.source, a.origin);
                            break;
                        case Zc:
                            b._onConnectAck(a.source);
                            break;
                        default:
                            b._onMessage(c.text)
                    }
                }
            }, _findChannel: function (g, f) {
                var d = l, c = d._channels, e = We(g.origin);
                for (var b = 0; b < c.length; b++) {
                    var a = c[b];
                    if (ic(a._name, f.name) && ic(a._allowedDomain, e))return a
                }
                return null
            }
        }, ad = "@ConnectReq", Zc = "@ConnectAck";

        function Gc(f, d, b, e, c) {
            var a = this;
            a._name = f;
            a._allowedDomain = d;
            a._msgHandler = c;
            if (b) {
                a._targetWindow = b;
                a._targetUrl = Me(e);
                a._connect()
            }
        }

        Gc.prototype = {
            _disposed: false,
            _connected: false,
            _allowedDomain: null,
            _targetWindow: null,
            _targetUrl: null,
            _msgHandler: null,
            _connectSchedule: -1,
            _pendingQueue: [],
            _recvQueue: [],
            dispose: function () {
                var a = this;
                if (!a._disposed) {
                    a._disposed = true;
                    a._cancelConnect();
                    l.unregisterChannel(a)
                }
            },
            send: function (c, d) {
                var a = this;
                if (a._disposed)return;
                var b = ge(c, d);
                if (a._connected)a._post(b); else a._pendingQueue.push(b)
            },
            _connect: function () {
                var a = this;
                if (a._disposed || a._connected)return;
                var b = function () {
                    a._post(ad)
                };
                if (a._connectSchedule === -1) {
                    a._connectSchedule = window.setInterval(b, 500);
                    b()
                }
            },
            _post: function (b) {
                var a = this, c = Be(a._name, b);
                a._targetWindow.postMessage(c, a._targetUrl)
            },
            _onConnectReq: function (c, b) {
                var a = this;
                if (!a._connected) {
                    a._connected = true;
                    a._targetWindow = c;
                    a._targetUrl = b;
                    a._post(Zc);
                    a._onConnected()
                }
            },
            _onConnectAck: function (b) {
                var a = this;
                if (!a._connected) {
                    a._targetWindow = b;
                    a._onConnected()
                }
            },
            _onConnected: function () {
                var a = this, c = a._pendingQueue;
                a._connected = true;
                for (var b = 0; b < c.length; b++)a._post(c[b]);
                a._pendingQueue = [];
                a._cancelConnect()
            },
            _cancelConnect: function () {
                var a = this;
                if (a._connectSchedule != -1) {
                    window.clearInterval(a._connectSchedule);
                    a._connectSchedule = -1
                }
            },
            _onMessage: function (b) {
                if (this._msgHandler) {
                    var a = fe(b);
                    this._msgHandler(a.cmd, a.args)
                }
            }
        };
        function Me(b) {
            var a = b.indexOf("://");
            if (a >= 0) {
                a = b.indexOf("/", a + 3);
                if (a >= 0)b = b.substring(0, a)
            }
            return b
        }

        function Ne(a) {
            var b = null;
            if (!Db(a.origin) && !Db(a.data) && a.source != null)b = Ae(a.data);
            return b
        }

        function Be(a, b) {
            return "<" + a + ">" + b
        }

        function Ae(a) {
            var c = null;
            if (a.charAt(0) == "<") {
                var b = a.indexOf(">");
                if (b > 0) {
                    var d = a.substring(1, b), e = a.substr(b + 1);
                    c = {name: d, text: e}
                }
            }
            return c
        }

        function ge(a, c) {
            var b = {cmd: a, args: c};
            return JSON.stringify(b)
        }

        function fe(b) {
            var a = JSON.parse(b);
            return a
        }

        if (window.WL)a.channel = Nb; else window.WL = {channel: Nb};
        var ob = {
            connect: "Connect",
            signIn: "Sign in",
            signOut: "Sign out",
            login: "Log in",
            logout: "Log out",
            skyDriveOpenPickerButtonText: "Open from OneDrive",
            skyDriveOpenPickerButtonTooltip: "Open from OneDrive",
            skyDriveSavePickerButtonText: "Save to OneDrive",
            skyDriveSavePickerButtonTooltip: "Save to OneDrive"
        };
        a._locale = "en";
        a[H] = "Web/DEVICE_" + Xd("5.5.8821.4000");
        a.testInit = function (b) {
            if (b.env)a._settings.init(b.env);
            if (b.skydrive_uri)a._settings[M] = b.skydrive_uri;
            if (b[eb])a[eb] = b[eb]
        };
        var P = {};
        P[K] = "https://apis.live.net/v5.0/";
        P[M] = "https://onedrive.live.com/";
        P[x] = "//js.live.net/v5.0/";
        P[jb] = "https://api.onedrive.com/v1.0/";
        var R = {};
        R[K] = "https://apis.live.net/v5.0/";
        R[M] = "https://onedrive.live.com/";
        R[x] = "//df-js.live.net/v5.0/";
        R[jb] = "https://df.api.onedrive.com/v1.0/";
        var Q = {};
        Q[K] = "https://apis.live-int.net/v5.0/";
        Q[M] = "https://onedrive.live-int.com/";
        Q[x] = "//js.live-int.net/v5.0/";
        Q[jb] = "https://newapi.storage.live-int.com/v1.0/";
        a._settings = {
            PROD: P, DF: R, INT: Q, init: function (b) {
                b = b.toUpperCase();
                var c = this[b];
                if (c)h(c, a)
            }
        };
        a._settings.init("PROD");
        a[FILEDIALOG_PARAM_PICKER_SCRIPT] = "wl.skydrivepicker.js";
        a.onloadInit();
        f.onloadInit()
    }
})();